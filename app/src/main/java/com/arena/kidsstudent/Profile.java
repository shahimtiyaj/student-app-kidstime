package com.arena.kidsstudent;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.arena.kidsstudent.utils.URLs;
import com.bumptech.glide.Glide;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONException;
import org.json.JSONObject;


public class Profile extends Fonts implements NavigationView.OnNavigationItemSelectedListener{
    Toolbar toolbar;
    Integer textID[] = { R.id.txt1, R.id.txt2, R.id.txt3, R.id.txt4, R.id.txt5, R.id.txt6, R.id
            .txt7, R.id.txt8, R.id.txt9, R.id.txt10, R.id.txt11 , R.id.namet, R.id.classt, R.id
            .rollt };
    String response;
    Integer title [] = {R.id.tname,R.id.tsn,R.id.tve,R.id.tc,R.id.tcr,R.id.tg,R.id.tmn,R.id
            .tdate,R.id.fa,R.id.ma,R.id.garinfo,R.id.cf};
    ImageView imgPicture;
    ProgressDialog pDialog;
    TextView namet,classn,classroll;
    View navview;
    RequestQueue requestQueue;
    JSONObject jsonObject;
    Parent p;
    Bitmap image,output;
    Context context = Profile.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        TextView t =findViewById(R.id.garinfo);
        t.setTypeface(getSemiBold());
        initiateViews();

        setfont(title);
        Checkandget();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().getItem(1).setChecked(true);
        NavigationMenuView navMenuView = (NavigationMenuView) navigationView.getChildAt(0);
       /* navMenuView.addItemDecoration(new DividerItemDecoration(Profile.this,
                DividerItemDecoration
                        .VERTICAL));*/
        navview = navigationView.getHeaderView(0);
        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size()-1;i++) {
            MenuItem mi = m.getItem(i).setActionView(R.layout.menu_image);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    p.applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            p.applyFontToMenuItem(mi);
        }
        namet = navview.findViewById(R.id.name);
        classn = navview.findViewById(R.id.classn);
        classroll = navview.findViewById(R.id.classroll);

        namet.setText(p.getPreference("name"));
        classn.setText(p.getPreference("class"));
        classroll.setText(p.getPreference("class_roll"));
        String response = p.getPreference("image");
        String gender = p.getPreference("gender");
        if (!response.equalsIgnoreCase("http://bgb.bgbschoolraj.edu.bd/smsadmin/uploads/std_photo/")){



            Glide.with(Profile.this).load(response).bitmapTransform(new
                    RoundedCornersTransformation( Profile.this,15,
                    1)).into((ImageView)
                    navview.findViewById(R.id.imageView));

        }else {
            if (gender.equalsIgnoreCase("female")) {

                Glide.with(Profile.this).load(R.drawable.female).bitmapTransform(new
                        RoundedCornersTransformation( Profile.this,15,
                        1)).into((ImageView) navview.findViewById(R.id
                        .imageView));


            } else {

                Glide.with(Profile.this).load(R.drawable.download).bitmapTransform
                        (new
                                RoundedCornersTransformation( Profile.this,15,
                                1)).into((ImageView) navview.findViewById(R.id
                        .imageView));

            }
            Toast.makeText(Profile.this, "No Picture Found", Toast.LENGTH_LONG)
                    .show();

        }



    }

    private void Checkandget() {
        if (isInternetOn()){
            new AsynUserInformation().execute();
        }else{

            AlertDialog.Builder builder = new AlertDialog.Builder(Profile.this);
            builder.setTitle("NO Internet Connection")
//                builder.setMessage("")
                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            Checkandget();

                        }
                    })
                    .show();
        }
    }


    private void initiateViews() {

        TextView pt= findViewById(R.id.ptitle);
        pt.setTypeface(getSemiBold());
       // pt.setTextColor(Color.parseColor("#223B4B"));
        imgPicture = (ImageView)findViewById(R.id.img_picture);
        requestQueue = Volley.newRequestQueue(Profile.this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Student Profile");
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        jsonObject = new JSONObject();
        p = new Parent(Profile.this);
        jsonObject=  p.getjsonobj();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
               finish();
                return true;
            case R.id.Logout:
                Intent i = new Intent(Profile.this,Login.class);
                startActivity(i);
                finishAffinity();
                return true;
            case R.id.Changepass:
                Intent j = new Intent(Profile.this,ChangePass.class);
                startActivity(j);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setTextViewText(int id, String text)
    {
        TextView tv = (TextView) findViewById(id);
        tv.setTypeface(getSemiBold());
        tv.setText(text);

    }

    public void setfont(Integer id [])
    {   for (int i = 0 ; i<id.length;i++){


        TextView tv = (TextView) findViewById(id[i]);
        if (id[i]==R.id.garinfo){
            tv.setTypeface(getSemiBold());
        }
        tv.setTypeface(getMedium());

    }

    }

    class AsynUserInformation extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Profile.this);

            pDialog.setMessage("Loading Please wait.");
            pDialog .requestWindowFeature (Window.FEATURE_NO_TITLE);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args)
        {
            JsonObjectRequest objectRequest = new JsonObjectRequest (Request.Method
                    .POST, URLs.GET_USER_INFO,jsonObject,

                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonResponse) {
                            try {
                                setTextViewText(textID[0],jsonResponse.getString("name"));
                                setTextViewText(textID[1],p.getPreference(Constants.USERNAME));
                                setTextViewText(textID[2],jsonResponse.getString("version"));
                                setTextViewText(textID[3],jsonResponse.getString("class"));
                                setTextViewText(textID[4],jsonResponse.getString("section"));
                                setTextViewText(textID[5],jsonResponse.getString("class_roll"));
                                setTextViewText(textID[6],jsonResponse.getString("gender"));
                                setTextViewText(textID[7],jsonResponse.getString("mobile"));
                                setTextViewText(textID[8],jsonResponse.getString("dob"));
                                setTextViewText(textID[11],jsonResponse.getString("name"));
                                setTextViewText(textID[12],jsonResponse.getString("class"));
                                setTextViewText(textID[13],p.getPreference(Constants.USERNAME));
                                String gender = jsonResponse.getString("gender");
                                if(jsonResponse.getString("father_name").equalsIgnoreCase
                                        ("null")){
                                    setTextViewText(textID[9],"");
                                }else
                                setTextViewText(textID[9],jsonResponse.getString("father_name"));
                                if(jsonResponse.getString("mother_name").equalsIgnoreCase
                                        ("null")){
                                    setTextViewText(textID[9],"");
                                }else
                                setTextViewText(textID[10],jsonResponse.getString("mother_name"));
                                response= jsonResponse.getString("image");
                                Log.e("image",jsonResponse.getString("image"));
                                if (!response.equalsIgnoreCase("http://bgb.bgbschoolraj.edu.bd/smsadmin/uploads/std_photo/")){

                                    /*try {
                                        URL url = new URL("http://....");
                                         image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                                    } catch(IOException e) {
                                        System.out.println(e);
                                    }
                                    getRoundedCornerBitmap(image,12);*/

                                    Glide.with(Profile.this).load(response).bitmapTransform(new
                                            RoundedCornersTransformation( Profile.this,15,
                                            1)).into((ImageView)
                                            findViewById(R.id.img_picture));
                                    imgPicture.setVisibility(View.VISIBLE);
                                }else {
                                    if (gender.equalsIgnoreCase("female")) {

                                        Glide.with(Profile.this).load(R.drawable.female).bitmapTransform(new
                                                RoundedCornersTransformation( Profile.this,15,
                                                1)).into((ImageView) findViewById(R.id.img_picture));

                                        imgPicture.setVisibility(View.VISIBLE);
                                    } else {

                                        Glide.with(Profile.this).load(R.drawable.download).bitmapTransform(new
                                                RoundedCornersTransformation( Profile.this,15,
                                                1)).into((ImageView) findViewById(R.id.img_picture));
                                        imgPicture.setVisibility(View.VISIBLE);
                                    }
                                    Toast.makeText(Profile.this, "No Picture Found", Toast.LENGTH_LONG).show();

                                }







                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.e("Responsen", jsonResponse.toString());
                            pDialog.dismiss();

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Error", error.toString());
                    pDialog.dismiss();
                }
            }) ;
            requestQueue.add(objectRequest);
            requestQueue.getCache().clear();
            objectRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            return null;
        }
        @Override
        protected void onPostExecute(String file_url) {
            runOnUiThread(new Runnable() {
                              @Override
                              public void run() {

                              }
                          }
                );
        }



    }

    public final boolean isInternetOn() {
        ConnectivityManager connec =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED ) {

            return true;
        } else if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||  connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED  ) {

            return false;
        }
        return false;
    }

    public  Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

            imgPicture.setImageBitmap(output);
        return output;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dash) {
            Intent i = new Intent(context,Home.class);
            startActivity(i);
            finish();
        } else if (id == R.id.profile) {
            Intent i = new Intent(context,Profile.class);
            startActivity(i);
            finish();
        } else if (id == R.id.payment) {
            Intent i = new Intent(context,Payment_list.class);
            startActivity(i);
            finish();
        }   else if (id == R.id.attendance) {
            p.getAttendance();
            Intent i = new Intent(context,attendance.class);
            startActivity(i);
            finish();

        }else if (id == R.id.classroutine) {
            Intent i = new Intent(context,Routine.class);
            startActivity(i);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
