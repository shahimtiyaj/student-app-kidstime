package com.arena.kidsstudent.repository;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import com.arena.kidsstudent.LibraryBookSearch;
import com.arena.kidsstudent.utils.Helper;
import com.arena.kidsstudent.utils.RetrofitInstance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterLibraryBookListRepository {

    JSONObject reqJsonObj;
    Context context;
    public FilterLibraryBookListRepository(LibraryBookSearch application, JSONObject reqJsonObj) {
        context = application;
        this.reqJsonObj = reqJsonObj;
        Log.e("json req--->", "RegistrationRepository: "+reqJsonObj.length() );

    }

    MutableLiveData<HashMap<String,String>> success;

    public  LiveData<HashMap<String,String>> getBookListStatus(){

        Log.e("RESPO____>","BAL ");
        if (success == null) {
            success = new MutableLiveData<>();
        }

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"),reqJsonObj.toString());

        Call<ResponseBody> call = RetrofitInstance.getInstance().getApi().doLibraryBookPagination(requestBody);
        Helper.showLoader(context,"");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                Log.e("repsonsefilter",response.toString());
                Log.e("respofilter-->", "onResponse: "+response.body().toString());




                    String output = response.body().string();
                    JSONObject jsonObj = new JSONObject(output);

                    JSONArray booklist = jsonObj.getJSONArray("book_list");
                    JSONObject jObject;
                    //JSONObject jsonObject = new JSONObject(output);
                    String total_page = jsonObj.getString("total_page");
                    String current_page = jsonObj.getString("current_page");

                    Log.e("classlistsize", String.valueOf(booklist.length()));
                    for(int i=0; i<booklist.length(); i++){

                        HashMap<String,String> responseMap = new HashMap<>();
                        JSONObject c = booklist.getJSONObject(i);

                        jObject = booklist.getJSONObject(i);

                        String book_name = jObject.getString("book_title");
                        String writer_name = jObject.getString("writer_name");
                        String book_status = jObject.getString("book_status");
                        String create_at = jObject.getString("create_at");

                        responseMap.put("book_title",book_name);
                        responseMap.put("writer_name",writer_name);
                        responseMap.put("book_status",book_status);
                        responseMap.put("create_at",create_at);

                        responseMap.put("total_page",total_page);
                        responseMap.put("current_page",current_page);

                        Log.e("json_data", c.toString());
                        success.setValue(responseMap);
                        Helper.cancelLoader();
                    }

                    //success.setValue(dataList);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("RESPO____>","BAL "+e.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("RESPO____>","BAL "+e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("RESPO____>","BAL "+e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                 Helper.cancelLoader();
            }
        });

        return success;



    }

}
