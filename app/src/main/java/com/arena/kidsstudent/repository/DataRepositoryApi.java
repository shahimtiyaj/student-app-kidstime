package com.arena.kidsstudent.repository;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.arena.kidsstudent.utils.RetrofitInstance;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataRepositoryApi {

    JSONObject reqJsonObj;
    public DataRepositoryApi(Application application, JSONObject reqJsonObj) {
        this.reqJsonObj = reqJsonObj;
        Log.e("json req--->", "RegistrationRepository: "+reqJsonObj.length() );
    }

    MutableLiveData<HashMap<String,String>> success;

    public LiveData<HashMap<String,String>> getRegStatus(){

        Log.e("REPO____>","BAL ");
        if (success == null) {
            success = new MutableLiveData<>();
        }

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json;charset=utf-8"),reqJsonObj.toString());

        Call<ResponseBody> call = RetrofitInstance.getInstance().getApi().doLogin(requestBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    JSONObject jsonObject=new JSONObject(response.body().string());
                    Log.e("REPO____>",""+jsonObject.getString("status"));

                    HashMap<String,String> responseMap = new HashMap<>();
                    responseMap.put("status",jsonObject.getString("status"));
                    responseMap.put("message",jsonObject.getString("message"));

                    success.setValue(responseMap);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("REPO____>","VALUE "+e.getMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Fail>","VALUE "+t.getMessage());

            }
        });

        return success;


    }

}
