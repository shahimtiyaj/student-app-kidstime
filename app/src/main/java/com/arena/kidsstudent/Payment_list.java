package com.arena.kidsstudent;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.arena.kidsstudent.ADapter.CoustomeAdapterforPayment;
import com.arena.kidsstudent.Model.Payment;
import com.arena.kidsstudent.kotlin.view.OnlineClassUrl;
import com.arena.kidsstudent.kotlin.view.SendMessage;
import com.arena.kidsstudent.utils.URLs;
import com.bumptech.glide.Glide;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Payment_list extends Fonts implements NavigationView.OnNavigationItemSelectedListener{

    Toolbar toolbar;

    ArrayList<Payment> List;
    ListView listView;
    CoustomeAdapterforPayment PaymentCa;
    RequestQueue requestQueue;
    JSONObject jsonObject;
    Integer textv [] = {R.id.Sl,R.id.PaymentTitle,R.id.TransactionID,R.id.Amount,R.id.month,R.id
            .year,R.id.ExpireDate,R.id.Status,R.id.Action};
    String month;
    ProgressDialog pDialog;
    String expiredate;
    Date date;
    String  today;
    int d,de,m,me,ye,y;
    Float amount;
    TextView namet,classn,classroll;
    View navview;
    int incress=0;
    Payment payment;
    String Txid;
    Context context = Payment_list.this;
    Parent parent;
    long currentdate,sexpiredate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_list);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Payment List");
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        List = new ArrayList<Payment>();
        parent = new Parent(Payment_list.this);
        listView= findViewById(R.id.listviewpd);
        requestQueue = Volley.newRequestQueue(Payment_list.this);
            for (int i = 0 ; i<textv.length;i++){
                TextView tv = findViewById(textv[i]);
                tv.setTypeface(getSemiBold());
            }
        jsonObject = new JSONObject();
        Parent p = new Parent(Payment_list.this);
        jsonObject=  p.getjsonobj();
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        d = cal.get(Calendar.DATE);
        m = cal.get(Calendar.MONTH);
        y=cal.get(Calendar.YEAR);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        currentdate = Long.parseLong(System.currentTimeMillis()+"");
        today = sdf.format(date);
        Log.e("current",currentdate+"");

        checkandget();






        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().getItem(3).setChecked(true);
        NavigationMenuView navMenuView = (NavigationMenuView) navigationView.getChildAt(0);
       /* navMenuView.addItemDecoration(new DividerItemDecoration(Payment_list.this,
                DividerItemDecoration
                        .VERTICAL));*/
        navview = navigationView.getHeaderView(0);
        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size()-1;i++) {
            MenuItem mi = m.getItem(i).setActionView(R.layout.menu_image);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    parent.applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            parent.applyFontToMenuItem(mi);
        }
        namet = navview.findViewById(R.id.name);
        classn = navview.findViewById(R.id.classn);
        classroll = navview.findViewById(R.id.classroll);

        namet.setText(parent.getPreference("name"));
        classn.setText(parent.getPreference("class"));
        classroll.setText(parent.getPreference("class_roll"));
        String response = parent.getPreference("image");
        String gender = parent.getPreference("gender");
        if (!response.equalsIgnoreCase("http://bgb.bgbschoolraj.edu.bd/smsadmin/uploads/std_photo/")){



            Glide.with(Payment_list.this).load(response).bitmapTransform(new
                    RoundedCornersTransformation( Payment_list.this,15,
                    1)).into((ImageView)
                    navview.findViewById(R.id.imageView));

        }else {
            if (gender.equalsIgnoreCase("female")) {

                Glide.with(Payment_list.this).load(R.drawable.female).bitmapTransform(new
                        RoundedCornersTransformation( Payment_list.this,15,
                        1)).into((ImageView) navview.findViewById(R.id
                        .imageView));


            } else {

                Glide.with(Payment_list.this).load(R.drawable.download).bitmapTransform
                        (new
                                RoundedCornersTransformation( Payment_list.this,15,
                                1)).into((ImageView) navview.findViewById(R.id
                        .imageView));

            }
            Toast.makeText(Payment_list.this, "No Picture Found", Toast.LENGTH_LONG)
                    .show();

        }
    }



    private void checkandget() {

        if (isInternetOn()){

                new AsynPayment().execute();

        }else{

            AlertDialog.Builder builder = new AlertDialog.Builder(Payment_list.this);
            builder.setTitle("NO Internet Connection")
//                builder.setMessage("")
                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            checkandget();

                        }
                    })
                    .show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.Logout:
                Intent i = new Intent(Payment_list.this,Login.class);
                startActivity(i);
                finishAffinity();
                return true;
            case R.id.Changepass:
                Intent j = new Intent(Payment_list  .this,ChangePass.class);
                startActivity(j);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


 /*   @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(Payment_list.this,"clicked",Toast.LENGTH_SHORT).show();
    }*/


    class AsynPayment extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Payment_list.this);

            pDialog.setMessage("Loading Please wait.");
            pDialog .requestWindowFeature (Window.FEATURE_NO_TITLE);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args)
        {
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST, URLs.PAYEMENT_LIST, jsonObject, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    Log.e("re",response.toString());
                    for (int i = response.length()-1; i >=0 ; i--) {


                        try {
                            expiredate =  response.getJSONObject(i).getString( "expire_date");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        SimpleDateFormat inputFormatter = new SimpleDateFormat
                                ("yyyy-MM-dd");





                        try {
                            date = inputFormatter.parse(expiredate);
                            sexpiredate=Long.parseLong(date.getTime()+"");
                            Log.e("expire",sexpiredate+"");
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        de = date.getDate();
                        me = date.getMonth();
                        ye = Integer.parseInt(DateFormat.format("yyyy", date).toString());

                        Log.e("date",me+"");

                        SimpleDateFormat outputFormatter = new SimpleDateFormat
                                ("dd-MM-yyyy");
                        String output = outputFormatter.format(date);



                        try {
                            if (response.getJSONObject(i).getString("month").equals("1")){

                                month ="January";
                            }else if (response.getJSONObject(i).getString("month").equals("2")){
                                month ="February";
                            }else if (response.getJSONObject(i).getString("month").equals("3")){
                                month ="March";
                            }else if (response.getJSONObject(i).getString("month").equals("4")){
                                month ="April";
                            }else if (response.getJSONObject(i).getString("month").equals("5")){
                                month ="May";
                            }else if (response.getJSONObject(i).getString("month").equals("6")){
                                month ="June";
                            }else if (response.getJSONObject(i).getString("month").equals("7")){
                                month ="July";
                            }else if (response.getJSONObject(i).getString("month").equals("8")){
                                month ="August";
                            }else if (response.getJSONObject(i).getString("month").equals("9")){
                                month ="September";
                            }else if (response.getJSONObject(i).getString("month").equals("11")){
                                month ="November";
                            }else if (response.getJSONObject(i).getString("month").equals("10")){
                                month ="October";
                            }else if (response.getJSONObject(i).getString("month").equals("12")){
                                month ="December";
                            }else{
                                month ="Bad Month Formate";
                            }




                            if (response.getJSONObject(i).getString( "bank_transection_id")
                                    .equalsIgnoreCase("null")){
                               Txid="";

                            }else {
                              Txid = response.getJSONObject(i).getString( "bank_transection_id");
                            }

                            if ( !response.getJSONObject(i).getString( "pay_status")
                                    .equalsIgnoreCase("paid")){


                                String amounts = response.getJSONObject(i)
                                        .getString( "amount").trim();

                                String f1 = amounts.replace(".",".");
                                String f2 = f1.replace(",","");

                                amount = Float.parseFloat(f2);
                                Log.e("date0",m+","+me+","+d+","+de+","+y+","+ye);
                                if (currentdate>sexpiredate){
                                    amount=amount+100;
                                    incress = 100;
                                    Log.e("in",amount+"");
                                }

                                payment = new Payment(response.getJSONObject(i).getString
                                       ("payment_id"),"Monthly Fee",Txid,amount+"", response
                                       .getJSONObject
                                       (i).getString( "month"),month,response.getJSONObject(i)
                                       .getString( "year"),response.getJSONObject(i).getString(
                                               "pay_type"),response.getJSONObject(i).getString(
                                                       "pay_status"),response.getJSONObject(i)
                                        .getString( "expire_date"),incress+"",response
                                        .getJSONObject(i).getString("month"));



                            }else {
                                 payment =  new Payment(response.getJSONObject(i).getString
                                        ("payment_id"),"Monthly Fee",Txid,response.getJSONObject(i)
                                         .getString( "amount").trim(), response
                                        .getJSONObject
                                                (i).getString( "month"),month,response.getJSONObject(i)
                                        .getString( "year"),response.getJSONObject(i).getString(
                                        "pay_type"),response.getJSONObject(i).getString(
                                        "pay_status"),response.getJSONObject(i).getString(
                                                "expire_date"),response.getJSONObject(i).getString("month"));


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        List.add(payment);
                    }

                    PaymentCa = new CoustomeAdapterforPayment(Payment_list.this, List);

                    listView.setAdapter(PaymentCa);


                    pDialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                }
            });

            jsonArrayRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
            requestQueue.add(jsonArrayRequest);
            requestQueue.getCache().clear();

            return null;
        }



    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dash) {
            Intent i = new Intent(context,Home.class);
            startActivity(i);
            finish();
        } else if (id == R.id.profile) {
            Intent i = new Intent(context,Profile.class);
            startActivity(i);
            finish();
        } else if (id == R.id.payment) {
            Intent i = new Intent(context,Payment_list.class);
            startActivity(i);
            finish();
        }   else if (id == R.id.attendance) {
            parent.getAttendance();
            Intent i = new Intent(context,attendance.class);
            startActivity(i);
            finish();

        }else if (id == R.id.classroutine) {
            Intent i = new Intent(context,Routine.class);
            startActivity(i);
            finish();
        }

         else if (id == R.id.sendteachemessage) {
            Intent i = new Intent(context, SendMessage.class);
            startActivity(i);
        } else if (id == R.id.teachemessagelist) {
            Intent i = new Intent(context, com.arena.kidsstudent.kotlin.view.MessageList.class);
            startActivity(i);
        } else if (id == R.id.onlineclassurl) {
            Intent i = new Intent(context, OnlineClassUrl.class);
            startActivity(i);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public final boolean isInternetOn() {
        ConnectivityManager connec =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED ) {

            return true;
        } else if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||  connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED  ) {

            return false;
        }
        return false;
    }
}
