package com.arena.kidsstudent;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arena.kidsstudent.ADapter.ViewSearchLibraryBookListAdapter;
import com.arena.kidsstudent.Model.SearchBook;
import com.arena.kidsstudent.utils.Utils;
import com.arena.kidsstudent.viewmodel.FilterLibraryBookListViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LibraryBookSearch extends AppCompatActivity {
    EditText searchBook;
    String user_id;

    FilterLibraryBookListViewModel filterLibraryBookListViewModel;
    RecyclerView recyclerView;

    List<SearchBook> searchBooks = new ArrayList<>();
    ImageView backImg;
    Intent intent;
    Button nextBtn, previousBtn, search, all;
    int onlinepage;
    int currentpage = 1;
    int totpage;
    Parent parent;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library_boook_search);

        Utils.adjustFullScreen(this, R.color.textcolor,true);

        intent = getIntent();

        parent = new Parent(LibraryBookSearch.this);
        user_id=parent.getPreference(Constants.USER_ID);

        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Book Search");
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));


        toolbar.setNavigationOnClickListener(v -> {
            Intent intent = new Intent(LibraryBookSearch.this,Home.class);
            startActivity(intent);
            finish();
        });


        search = findViewById(R.id.searchBtn);
        searchBook = findViewById(R.id.searchbookEdt);

        recyclerView = findViewById(R.id.recycler_view);
        //backImg = findViewById(R.id.backImg);
        previousBtn = findViewById(R.id.previousBtn);
        previousBtn.setEnabled(false);
        previousBtn.getBackground().setAlpha(45);

        nextBtn = findViewById(R.id.nextBtn);
        all = findViewById(R.id.all);

        /*backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LibraryBookSearch.this,Home.class);
                startActivity(intent);
                finish();
            }
        });
*/
        filterList("", currentpage);



        previousBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("page", String.valueOf(currentpage));
                if(currentpage == 2){
                    previousBtn.getBackground().setAlpha(45);
                }
                if(currentpage>1){
                    currentpage = currentpage - 1;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (Utils.SearchString.equals(searchBook.getText().toString())) {
                                filterList(searchBook.getText().toString(), currentpage);
                            }                        }
                    }, 1000);

                    Log.e("curr_page", String.valueOf(currentpage));
                }

            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentpage = currentpage + 1;

                if(currentpage > 1){
                    previousBtn.setEnabled(true);
                    previousBtn.getBackground().setAlpha(255);
                }

                if(currentpage <= totpage){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (Utils.SearchString.equals(searchBook.getText().toString())) {
                                filterList(searchBook.getText().toString(), currentpage);
                            }                        }
                    }, 1000);

                    Log.e("curr_page", String.valueOf(currentpage));
                }
            }
        });

        all.setOnClickListener(v -> {
            searchBook.getText().clear();
            Utils.SearchString = "";
            filterList("", currentpage);
        });


        search.setOnClickListener(v -> {

            filterList(searchBook.getText().toString(), currentpage);
            Utils.SearchString = searchBook.getText().toString();
        });

    }

    private void filterList(String search,  int currentpage) {
        recyclerView.setAdapter(null);
        searchBooks.clear();

        JSONObject reqJsonObj = new JSONObject();

        try {
            reqJsonObj.put("book_name",search);
            reqJsonObj.put("current_page",currentpage);

            Log.e("reqjson", String.valueOf(reqJsonObj));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        filterLibraryBookListViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication()).create(FilterLibraryBookListViewModel.class);
        filterLibraryBookListViewModel.initialize(LibraryBookSearch.this,reqJsonObj);
        filterLibraryBookListViewModel.getProfileStatus().observe(LibraryBookSearch.this, new Observer<HashMap<String, String>>() {
            @Override
            public void onChanged(HashMap<String, String> stringStringHashMap) {
                try {
                    String book_title = stringStringHashMap.get("book_title");
                    String writer_name = stringStringHashMap.get("writer_name");
                    String book_status = stringStringHashMap.get("book_status");
                    String create_at = stringStringHashMap.get("create_at");

                    String totalpage = stringStringHashMap.get("total_page");
                    String current_page = stringStringHashMap.get("current_page");

                    totpage = Integer.parseInt(totalpage);

                    onlinepage = Integer.parseInt(current_page);

                    searchBooks.add(new SearchBook(book_title,writer_name,book_status,create_at));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                } finally {
                    Log.e("listpost",stringStringHashMap.get("book_title"));
                }

                ViewSearchLibraryBookListAdapter adapter = new ViewSearchLibraryBookListAdapter(searchBooks);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(LibraryBookSearch.this);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(adapter);

            }
        });

    }


}