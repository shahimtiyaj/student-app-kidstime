package com.arena.kidsstudent;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DividerItemDecoration;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.arena.kidsstudent.kotlin.view.MessageList;
import com.arena.kidsstudent.kotlin.view.OnlineClassUrl;
import com.arena.kidsstudent.kotlin.view.SendMessage;
import com.arena.kidsstudent.utils.URLs;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

public class Home extends Fonts implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    public static final String TAG = "@MY_APP";
    public static final String TOPIC = "all_user";

    Toolbar toolbar;
    Integer[] buttons = {R.id.dp, R.id.dpay, R.id.dno, R.id.dro, R.id.dc, R.id.call, R.id.dashattendance, R.id.dashpayment, R.id.dashnotice};

    Integer[] textViews = {R.id.well, R.id.named, R.id.classt, R.id.rollt, R.id.todaydash, R.id.attdash, R.id.date, R.id.paydash, R.id.paymentdash, R.id.noticedash, R.id.noticedashte};

    TextView dashname, classname, stdid, astatus, adate, paydash, paymdash, noticedash;
    JSONObject jsonObject;
    JSONObject jsonObject1;
    Parent parent;
    View navview;
    int[] data = new int[5];
    ImageView imageView;
    TextView name, classn, classroll;
    RequestQueue requestQueue;
    LinearLayout lbg1, lbg2, lbg3;
    ProgressDialog pDialog;
    String response;
    int working_days, total_attendance, absent, todayflag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Firebase init -------------------------------------
        FirebaseMessaging.getInstance().subscribeToTopic(TOPIC);

        toolbar = findViewById(R.id.toolbar);
        lbg1 = findViewById(R.id.bg1);
        lbg2 = findViewById(R.id.bg2);
        requestQueue = Volley.newRequestQueue(Home.this);
        parent = new Parent(Home.this);
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.homed);
        toolbar.setNavigationIcon(drawable);
        jsonObject1 = new JSONObject();
        jsonObject1 = parent.getjsonobj();
        Log.e("jsonobj", jsonObject1.toString());
        checkandget();
        setSupportActionBar(toolbar);
        setfont(buttons);
        setfonttext(textViews);
        astatus = findViewById(R.id.attdash);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        NavigationMenuView navMenuView = (NavigationMenuView) navigationView.getChildAt(0);
        navMenuView.addItemDecoration(new DividerItemDecoration(Home.this, DividerItemDecoration
                .HORIZONTAL));
        navview = navigationView.getHeaderView(0);
        name = navview.findViewById(R.id.name);
        classn = navview.findViewById(R.id.classn);
        classroll = navview.findViewById(R.id.classroll);
        Animation animation = AnimationUtils.loadAnimation(Home.this, R.anim.fade_in);
        dashname.setAnimation(animation);
        classname.setAnimation(animation);
        stdid.setAnimation(animation);
        lbg1.setAnimation(animation);
        lbg2.setAnimation(animation);
        dashname.setText(parent.getPreference("name"));
        classname.setText(parent.getPreference("class"));
        stdid.setText(parent.getPreference("class_roll"));

        navigationView.getMenu().getItem(0).setChecked(true);
        setTitle("DashBoard");
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size() - 1; i++) {
            MenuItem mi = m.getItem(i).setActionView(R.layout.menu_image);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    parent.applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            parent.applyFontToMenuItem(mi);
        }

        if (isInternetOn()) {
            Saveinformation();
        } else {
            Toast.makeText(Home.this, "No Internet Connection", Toast.LENGTH_LONG).show();
        }

        adate.setText("Marked On: " + getCurrentDate());

    }


    private void checkandget() {
        if (isInternetOn()) {
            data = parent.getAttendance();
            new AsyncPayment().execute();
            new AsynAttendenct().execute();

        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(Home.this);
            builder.setTitle("NO Internet Connection")
//                builder.setMessage("")
                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            checkandget();

                        }
                    })
                    .show();
        }

    }

    private void Saveinformation() {
        jsonObject = new JSONObject();

        jsonObject = parent.getjsonobj();

        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method
                .POST, URLs.GET_USER_INFO, jsonObject,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonResponse) {
                        try {
                            parent.savePreference("name", jsonResponse.getString("name").trim());
                            parent.savePreference("version", jsonResponse.getString("version"));
                            parent.savePreference("class", jsonResponse.getString("class"));
                            parent.savePreference("section", jsonResponse.getString("section"));
                            parent.savePreference("class_roll", jsonResponse.getString("class_roll"));
                            parent.savePreference("gender", jsonResponse.getString("gender"));
                            parent.savePreference("mobile", jsonResponse.getString("mobile"));
                            parent.savePreference("dob", jsonResponse.getString("dob"));
                            parent.savePreference("father_name", jsonResponse.getString("father_name"));
                            parent.savePreference("mother_name", jsonResponse.getString("mother_name"));
                            parent.savePreference("last_name", jsonResponse.getString("last_name"));
                            parent.savePreference("class_id", jsonResponse.getString("class_id"));
                            parent.savePreference("section_id", jsonResponse.getString("section_id"));
                            parent.savePreference("image", jsonResponse.getString("image"));

                            name.setText(parent.getPreference("name"));
                            classn.setText(parent.getPreference("class"));
                            classroll.setText(parent.getPreference("class_roll"));
                            String gender = jsonResponse.getString("gender");
                            response = jsonResponse.getString("image");
                            Log.e("image", jsonResponse.getString("image"));
                            dashname.setText(parent.getPreference("name"));
                            classname.setText(parent.getPreference("class"));
                            stdid.setText(parent.getPreference("class_roll"));


                            if (!response.equalsIgnoreCase("http://bgb.bgbschoolraj.edu.bd/smsadmin/uploads/std_photo/")) {

                              /*  Glide.with(Home.this)
                                        .load(response)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .placeholder(R.drawable.female)
                                        .transform(new CenterCrop(), new Rotate(90))
                                        .into((ImageView) navview.findViewById(R.id.imageView));*/

                                Glide.with(Home.this).load(response).bitmapTransform(new
                                        RoundedCornersTransformation(Home.this, 15,
                                        1)).into((ImageView)
                                        navview.findViewById(R.id.imageView));

                                Glide.with(Home.this).load(response).bitmapTransform(new
                                        RoundedCornersTransformation(Home.this, 15,
                                        1)).into((ImageView)
                                        findViewById(R.id.pp));

                            } else {
                                if (gender.equalsIgnoreCase("female")) {

                                    Glide.with(Home.this).load(R.drawable.female).bitmapTransform(new
                                            RoundedCornersTransformation(Home.this, 15,
                                            1)).into((ImageView) navview.findViewById(R.id
                                            .imageView));

                                    Glide.with(Home.this).load(response).bitmapTransform(new
                                            RoundedCornersTransformation(Home.this, 15,
                                            1)).into((ImageView)
                                            findViewById(R.id.pp));

                                } else {

                                    Glide.with(Home.this)
                                            .load(R.drawable.download).bitmapTransform(new RoundedCornersTransformation(Home.this, 15,
                                            1))
                                            .into((ImageView) navview.findViewById(R.id
                                            .imageView));
                                    Glide.with(Home.this).load(response).bitmapTransform(new
                                            RoundedCornersTransformation(Home.this, 15,
                                            1)).into((ImageView)
                                            findViewById(R.id.pp));

                                }
                                Toast.makeText(Home.this, "No Picture Found", Toast.LENGTH_LONG)
                                        .show();
                            }
                            Log.e("Responsen", jsonResponse.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.toString());
            }
        });
        requestQueue.add(objectRequest);
        requestQueue.getCache().clear();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Logout:
                Intent i = new Intent(Home.this, Login.class);
                startActivity(i);
                finishAffinity();
                return true;
            case R.id.Changepass:
                Intent j = new Intent(Home.this, ChangePass.class);
                startActivity(j);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public final boolean isInternetOn() {
        ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

            return true;
        } else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED || connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

            return false;
        }
        return false;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dash) {
            Toast.makeText(Home.this, "this is Dashboard", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.profile) {
            Intent i = new Intent(Home.this, Profile.class);
            startActivity(i);
        } else if (id == R.id.attendance) {
            Intent i = new Intent(Home.this, attendance.class);
            i.putExtra("totalp", data[2]);
            i.putExtra("totala", data[3]);
            i.putExtra("totalw", data[1]);
            i.putExtra("today", data[0]);
            startActivity(i);
        } else if (id == R.id.payment) {
            Intent i = new Intent(Home.this, Payment_list.class);
            startActivity(i);
        } else if (id == R.id.classroutine) {
            Intent i = new Intent(Home.this, Routine.class);
            startActivity(i);
        } else if (id == R.id.sendteachemessage) {
            Intent i = new Intent(Home.this, SendMessage.class);
            startActivity(i);
        } else if (id == R.id.teachemessagelist) {
            Intent i = new Intent(Home.this, com.arena.kidsstudent.kotlin.view.MessageList.class);
            startActivity(i);
        } else if (id == R.id.onlineclassurl) {
            Intent i = new Intent(Home.this, OnlineClassUrl.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }


    public void setfont(Integer[] font) {
        for (int i = 0; i < font.length; i++) {
            Button b = findViewById(font[i]);

            b.setOnClickListener(this);
            b.setTypeface(getSemiBold());
        }

    }


    @Override
    public void onClick(View view) {

        int id = view.getId();

        if (id == R.id.dash) {
            Toast.makeText(Home.this, "this is Dashboard", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.dp) {
            Intent i = new Intent(Home.this, Profile.class);
            startActivity(i);
        } else if (id == R.id.dashpayment) {
            Intent i = new Intent(Home.this, Payment_list.class);
            startActivity(i);
        } else if (id == R.id.dpay) {
            Intent i = new Intent(Home.this, SendMessage.class);
            startActivity(i);
        } else if (id == R.id.dashnotice) {
            AlertDialog.Builder builder = new AlertDialog.Builder(Home.this);
            View view1 = getLayoutInflater().inflate(R.layout.alart_dialog, null);
            Button General = view1.findViewById(R.id.general);
            Button Personal = view1.findViewById(R.id.personal);
            TextView ps = view1.findViewById(R.id.ps);
            ps.setTypeface(getSemiBold());
            Personal.setTypeface(getMedium());
            General.setTypeface(getMedium());
            builder.setView(view1);
            final AlertDialog dialog = builder.create();
            dialog.show();

            General.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Home.this, Notice.class);
                    intent.putExtra("noticeFor", "general");
                    startActivity(intent);
                    dialog.dismiss();
                }
            });
            Personal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Home.this, Notice.class);
                    intent.putExtra("noticeFor", "personal");
                    startActivity(intent);
                    dialog.dismiss();

                }
            });
        } else if (id == R.id.dashattendance) {
            Intent i = new Intent(Home.this, attendance.class);
            startActivity(i);

        } else if (id == R.id.dc) {
            Intent i = new Intent(Home.this, MessageList.class);
            startActivity(i);
        } else if (id == R.id.dno) {
            Intent i = new Intent(Home.this, Note.class);
            startActivity(i);
        } else if (id == R.id.dro) {
            Intent i = new Intent(Home.this, Routine.class);
            startActivity(i);
        } else if (id == R.id.call) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + "+0721761454"));
            startActivity(intent);
        }

    }

    public void setfonttext(Integer[] font) {
        for (int i = 0; i < font.length; i++) {

            if (i == 1) {
                dashname = findViewById(font[i]);
                dashname.setTypeface(getMedium());
            } else if (i == 2) {
                classname = findViewById(font[i]);
                classname.setTypeface(getMedium());
            } else if (i == 3) {
                stdid = findViewById(font[i]);
                stdid.setTypeface(getMedium());
            } else if (i == 5) {
                astatus = findViewById(font[i]);
                astatus.setTypeface(getMedium());
            } else if (i == 6) {
                adate = findViewById(font[i]);
                adate.setTypeface(getMedium());
            } else if (i == 8) {
                paydash = findViewById(font[i]);
                paydash.setTypeface(getMedium());
            } else if (i == 10) {
                noticedash = findViewById(font[i]);
                noticedash.setTypeface(getMedium());
            } else {
                TextView b = findViewById(font[i]);
                b.setTypeface(getSemiBold());
            }
        }
    }


    class AsyncPayment extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Home.this);

            pDialog.setMessage("Loading Please wait....");
            pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            /*pDialog.show();*/
            requestQueue = Volley.newRequestQueue(Home.this);
            jsonObject1 = new JSONObject();
            jsonObject1 = parent.getjsonobj();
        }

        @Override
        protected String doInBackground(String... args) {
            JsonObjectRequest jsonObjectRequestp = new JsonObjectRequest(Request.Method.POST, URLs
                    .PAMENT, jsonObject1, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("responce", response.toString());
                    try {
                        int duemonth = Integer.parseInt(response.getString("total_due_payment"));
                        if (duemonth > 0) {
                            parent.savePreference("payment", "You Have " + duemonth + " Month Due");
                            paydash.setText(parent.getPreference("payment"));
                            pDialog.dismiss();
                        } else {
                            parent.savePreference("payment", "You Have No Due");
                            paydash.setText(parent.getPreference("payment"));
                            pDialog.dismiss();
                        }

                        pDialog.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    pDialog.dismiss();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();

                }
            });
            requestQueue.add(jsonObjectRequestp);
            requestQueue.getCache().clear();
            return null;
        }


        @Override
        protected void onPostExecute(String s) {

            if ((pDialog != null) && pDialog.isShowing()) {
                pDialog.dismiss();
            }


        }
    }


    class AsynAttendenct extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Home.this);

            pDialog.setMessage("Loading Please wait.");
            pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
            requestQueue = Volley.newRequestQueue(Home.this);
            jsonObject1 = new JSONObject();
            jsonObject1 = parent.getjsonobj();
        }

        @Override
        protected String doInBackground(String... args) {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLs.ATTENDANCE, jsonObject1, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("responce", response.toString());
                    try {
                        todayflag = Integer.parseInt(response.getString("today_attendance"));
                        working_days = Integer.parseInt(response.getString("working_days"));
                        total_attendance = Integer.parseInt(response.getString("total_attendance"));
                        absent = working_days - total_attendance;
                        parent.savePreference("today", todayflag + "");
                        parent.savePreference("working_days", working_days + "");
                        parent.savePreference("total_attendance", total_attendance + "");
                        parent.savePreference("absent", absent + "");
                        int absentt = Integer.parseInt(parent.getPreference("today"));
                        Log.e("absent", absentt + "");
                        if (absentt == 0) {
                            astatus.setText("Absent");
                            astatus.setTextColor(Color.parseColor("#FE4E51"));
                            Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.redcircle);
                            img.setBounds(0, 0, 40, 40);
                            astatus.setCompoundDrawables(img, null, null, null);
                            pDialog.dismiss();
                        }
                        pDialog.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();

                }
            });
            requestQueue.add(jsonObjectRequest);
            requestQueue.getCache().clear();
            return null;
        }


        @Override
        protected void onPostExecute(String s) {

            if ((pDialog != null) && pDialog.isShowing()) {
                pDialog.dismiss();
            }


        }
    }
}
