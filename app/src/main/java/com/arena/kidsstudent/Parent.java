package com.arena.kidsstudent;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.arena.kidsstudent.utils.URLs;

import org.json.JSONException;
import org.json.JSONObject;

public class Parent  extends AppCompatActivity {

  static   Context context;
    int working_days,total_attendance,absent, todayflag ;
    RequestQueue requestQueue;
    ProgressDialog pDialog;
    JSONObject jsonObject1;

    public Parent(Context context) {
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    public void savePreference(String key, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreference(String key)
    {
        String value="";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        value = prefs.getString(key, "pref_not_saved");

        return value;

    }

    public JSONObject getjsonobj (){

      JSONObject  jsonObject = new JSONObject();

        try {
            jsonObject.put(Constants.USER_ID,getPreference(Constants.USER_ID));

            Log.e("sheard",jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public JSONObject getjsonobj (String string1,String string2){

        JSONObject  jsonObject = new JSONObject();

        try {
            jsonObject.put(string1,string2);

            Log.e("sheard",jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public int [] getAttendance() {
         new AsynAttendenct().execute();
        int data[]= {todayflag,working_days,total_attendance,absent};
         return data;
    }

    public void getpayment(){
        new AsyncPayment().execute();

    }

    class AsynAttendenct extends AsyncTask<String, String , String>
    {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);

            pDialog.setMessage("Loading Please wait.");
            pDialog .requestWindowFeature (Window.FEATURE_NO_TITLE);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
            requestQueue = Volley.newRequestQueue(context);
            jsonObject1 = new JSONObject();
            jsonObject1 = getjsonobj();
        }

        @Override
        protected String doInBackground(String... args)
        {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLs.ATTENDANCE, jsonObject1, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("responce", response.toString());
                    try {
                        todayflag = Integer.parseInt(response.getString("today_attendance"));
                        working_days = Integer.parseInt(response.getString("working_days"));
                        total_attendance = Integer.parseInt(response.getString("total_attendance"));
                        absent = working_days - total_attendance;
                        savePreference("today",todayflag+"");
                        savePreference("working_days",working_days+"");
                        savePreference("total_attendance",total_attendance+"");
                        savePreference("absent",absent+"");
                        pDialog.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();

                }
            });
            requestQueue.add(jsonObjectRequest);
            return  null;
        }



        @Override
        protected void onPostExecute(String s) {

            if ((pDialog != null) && pDialog.isShowing()) {
                pDialog.dismiss();
            }


        }
    }
    class AsyncPayment extends AsyncTask<String, String , String>
    {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);

            pDialog.setMessage("Loading Please wait.");
            pDialog .requestWindowFeature (Window.FEATURE_NO_TITLE);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
            requestQueue = Volley.newRequestQueue(context);
            jsonObject1 = new JSONObject();
            jsonObject1 = getjsonobj();
        }

        @Override
        protected String doInBackground(String... args)
        {
            JsonObjectRequest jsonObjectRequestp = new JsonObjectRequest(Request.Method.POST, URLs
                    .PAMENT, jsonObject1, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("responce", response.toString());
                    try { int duemonth = Integer.parseInt(response.getString("total_due_payment"));
                        if (duemonth>0){
                            savePreference("payment","You Have"+duemonth+"Due");
                        }else {
                            savePreference("payment","You Have No Due");
                        }

                        pDialog.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();

                }
            });
            requestQueue.add(jsonObjectRequestp);
            return  null;
        }



        @Override
        protected void onPostExecute(String s) {

            if ((pDialog != null) && pDialog.isShowing()) {
                pDialog.dismiss();
            }


        }
    }


    @Override
    public void onPause() {
        super.onPause();

        if ((pDialog != null) && pDialog.isShowing())
            pDialog.dismiss();
        pDialog = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        if ((pDialog != null) && pDialog.isShowing()) pDialog.dismiss();
        pDialog = null;
    }


    protected void applyFontToMenuItem(MenuItem mi) {

        Typeface font = Typeface.createFromAsset(context.getAssets(), "Sofia Pro SemiBold.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }
}
