package com.arena.kidsstudent;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.arena.kidsstudent.kotlin.view.OnlineRegistration;
import com.arena.kidsstudent.utils.URLs;

import org.json.JSONException;
import org.json.JSONObject;

public class Login extends Fonts {

    Button btnLogin, regbtn;
    Toolbar toolbar;
    EditText et_username, et_password;
    RequestQueue requestQueue;
    String Username, Password;
    String status;
    String user_id;
    Parent parent;
    JSONObject jsonObject;
    boolean internet;
    ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initiateViews();
        btnLogin.setTypeface(getSemiBold());
        et_username.setTypeface(getRegular());
        btnLogin.setTypeface(getSemiBold());
        parent = new Parent(Login.this);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                internet = isInternetOn();
                if (internet) {
                    if (et_password.length() > 0 && et_username.length() > 0) {
                        new LoginCheck().execute();
                    } else {
                        Toast.makeText(Login.this, "Input Username & Password", Toast
                                .LENGTH_LONG).show();
                    }

               /* Username = "RAJCPSC201805172";
                  Password = "123456";*/
                } else {

                    Toast.makeText(Login.this, "No Internet Connection", Toast.LENGTH_LONG).show();
                }
            }

        });

        regbtn.setOnClickListener(view -> {
            Intent i = new Intent(Login.this, OnlineRegistration.class);
            startActivity(i);
            finish();
        });
    }


    private void initiateViews() {
        TextView textView = findViewById(R.id.studentl);
        textView.setTypeface(getSemiBold());
        requestQueue = Volley.newRequestQueue(Login.this);
        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);

        btnLogin = findViewById(R.id.btn_login);
        regbtn = findViewById(R.id.btn_reg);
        Log.e("Username", Username + Password);
    }

    public final boolean isInternetOn() {
        ConnectivityManager connec = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {

            return true;
        } else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED || connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

            return false;
        }
        return false;
    }

    @Override
    public void onBackPressed() {

        moveTaskToBack(true);
    }

    class LoginCheck extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Login.this);

            pDialog.setMessage("Checking....");
            pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {

            Username = et_username.getText().toString().trim();
            Password = et_password.getText().toString().trim();


            if (Username.length() > 0) {

                if (Password.length() > 0) {


                    final JSONObject paramObject = new JSONObject();
                    try {
                        paramObject.put("username", Username);
                        paramObject.put("password", Password);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.e("json", paramObject.toString());
                    JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, URLs.LOGIN, paramObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonResponse) {
                            try {
                                status = jsonResponse.getString("status");
                                user_id = jsonResponse.getString("user_id");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.e("Response", user_id);
                            if (status.equals("success")) {


                                parent.savePreference(Constants.USER_ID, user_id);
                                parent.savePreference(Constants.USERNAME, Username);
                                Log.e("usernamee", Username);

                                parent.savePreference(Constants.PASSWORD, Password);
                                Intent i = new Intent(Login.this, Home.class);

                                startActivity(i);

                                finish();
                                pDialog.dismiss();

                            } else {
                                Toast.makeText(Login.this, "Wrong Username or Password ", Toast.LENGTH_LONG).show();
                                pDialog.dismiss();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Error", error.toString());
                            Toast.makeText(Login.this, "Try Again ", Toast
                                    .LENGTH_LONG).show();
                            pDialog.dismiss();
                        }
                    });
                    requestQueue.add(objectRequest);
                    requestQueue.getCache().clear();

                    objectRequest.setRetryPolicy(new RetryPolicy() {
                        @Override
                        public int getCurrentTimeout() {
                            return 50000;
                        }

                        @Override
                        public int getCurrentRetryCount() {
                            return 50000;
                        }

                        @Override
                        public void retry(VolleyError error) throws VolleyError {

                        }
                    });
                } else
                    et_password.setError("Password Can't be empty");

            } else

                et_username.setError("Username Can't be empty");


            return null;
        }


    }


}
