package com.arena.kidsstudent;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.arena.kidsstudent.utils.URLs;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePass extends AppCompatActivity {

    Toolbar toolbar;
    EditText et_oldpass,et_newpass,et_conpass;
    Button btn_cp;
    ProgressDialog pDialog;
    JSONObject jsonObject;
    RequestQueue requestQueue;
    Parent p;
    String oldpass,newpass,connewpass,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
        initiateViews();
        requestQueue = Volley.newRequestQueue(ChangePass.this);

             p = new Parent(ChangePass.this);


        btn_cp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                oldpass=et_oldpass.getText().toString();
                newpass=et_newpass.getText().toString();
                connewpass=et_conpass.getText().toString();


                jsonObject = new JSONObject();

                try {
                    jsonObject.put(Constants.USER_ID,p.getPreference(Constants.USER_ID));
                    jsonObject.put("user_name",p.getPreference(Constants.USERNAME));
                    jsonObject.put("new_password",newpass);
                    Log.e("sheard",jsonObject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                password= p.getPreference(Constants.PASSWORD);

                if (!oldpass.equals(password) ) {

                    et_oldpass.setError("Not match with Current Password");
                }else{

                    if (newpass.length() < 6) {
                        et_newpass.setError("minimum 6 character password");


                    }else {

                        if (!newpass.equals(connewpass)){

                            et_conpass.setError("Confirm Password Not match");

                        }else {

                            new AsynChangePass().execute();


                        }

                    }

                }





            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Logout:
                Intent i = new Intent(ChangePass.this,Login.class);
                startActivity(i);
                finish();
                return true;
            case R.id.Changepass:
                Intent j = new Intent(ChangePass.this,ChangePass.class);
                startActivity(j);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initiateViews() {
        /*toolbar = findViewById(R.id.toolbar);*/
        et_oldpass = findViewById(R.id.et_oldpass);
        et_newpass = findViewById(R.id.et_newpass);
        et_conpass = findViewById(R.id.et_conpass);
        btn_cp = findViewById(R.id.btn_cp);

       /* setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle(Constants.APPNAME);
        toolbar.setTitleTextColor(Color.parseColor("#000000"));*/


    }

    class AsynChangePass extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ChangePass.this);
            pDialog.setMessage("Loading Please wait.");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args)
        {           Log.e("obj",jsonObject.toString());
            try{
                JsonObjectRequest objectRequest = new JsonObjectRequest (Request.Method
                        .POST, URLs.CHANGE_PASSWORD,jsonObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject jsonResponse) {

                                try {
                                String reponce  =  jsonResponse.getString("response");

                                Toast.makeText(ChangePass.this,reponce,Toast.LENGTH_LONG).show();
                                    p.savePreference(Constants.PASSWORD,connewpass);
                                et_conpass.setText("");
                                et_newpass.setText("");
                                et_oldpass.setText("");

                                 pDialog.dismiss();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error", error.toString());
                        pDialog.dismiss();
                    }
                });
                requestQueue.add(objectRequest);
                requestQueue.getCache().clear();
            }
            catch(Exception e) { }
            return null;
        }
    }
}
