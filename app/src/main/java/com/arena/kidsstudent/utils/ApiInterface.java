package com.arena.kidsstudent.utils;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {

    @POST("check_login")
    Call<ResponseBody> doLogin(@Body RequestBody jsonObject);

    @POST("myprofile")
    Call<ResponseBody> doProfile(@Body RequestBody jsonObject);

    @POST("class_routine")
    Call<ResponseBody> doClass(@Body RequestBody jsonObject);

    @POST("section")
    Call<ResponseBody> doSection(@Body RequestBody jsonObject);

    @POST("get_class_routine")
    Call<ResponseBody> doClassRoutine(@Body RequestBody jsonObject);

    @POST("book_list")
    Call<ResponseBody> doBookList(@Body RequestBody jsonObject);

    @POST("get_book_list")
    Call<ResponseBody> doGetBookList(@Body RequestBody jsonObject);

    @POST("book_download")
    Call<ResponseBody> doGetDownload(@Body RequestBody jsonObject);

    @POST("syllabus_list")
    Call<ResponseBody> doGetSyllabusList(@Body RequestBody jsonObject);

    @POST("get_class_wise_syllabus")
    Call<ResponseBody> doGetDownloadClasswiseSyllabus(@Body RequestBody jsonObject);

    @POST("syllabus_download")
    Call<ResponseBody> doGetDownloadSyllabus(@Body RequestBody jsonObject);

    @POST("general_notice")
    Call<ResponseBody> doGetGeneralNotices(@Body RequestBody jsonObject);

    @POST("syllabus_view")
    Call<ResponseBody> doGetSyllabusView(@Body RequestBody jsonObject);

    @POST("get_notebook")
    Call<ResponseBody> doGetNoteBook(@Body RequestBody jsonObject);

    @POST("get_student")
    Call<ResponseBody> doGetStudentList(@Body RequestBody jsonObject);

    @POST("student_profile")
    Call<ResponseBody> doGetStudentProfile(@Body RequestBody jsonObject);

    @POST("note_download")
    Call<ResponseBody> doNoteDownload(@Body RequestBody jsonObject);

    @POST("note_view")
    Call<ResponseBody> doNoteView(@Body RequestBody jsonObject);


    @POST("academic_calendar")
    Call<ResponseBody> doAcademicCalendar(@Body RequestBody jsonObject);

    @POST("academic_calendar_download")
    Call<ResponseBody> doAcademicCalendarDownload(@Body RequestBody jsonObject);

    @POST("change_password")
    Call<ResponseBody> doChangePassword(@Body RequestBody jsonObject);

    @POST("result")
    Call<ResponseBody> doResultList(@Body RequestBody jsonObject);


    @POST("student_result")
    Call<ResponseBody> doStudentResult(@Body RequestBody jsonObject);

    @POST("comment")
    Call<ResponseBody> doComment(@Body RequestBody jsonObject);

    @POST("post_comment")
    Call<ResponseBody> doPostComment(@Body RequestBody jsonObject);

    @POST("student_first_term_result")
    Call<ResponseBody> doFirstTermResult(@Body RequestBody jsonObject);

    @POST("generate_grid_board")
    Call<ResponseBody> doGenerateGrid(@Body RequestBody jsonObject);

    @POST("insert_grid_board")
    Call<ResponseBody> doInsertGrid(@Body RequestBody jsonObject);

    @POST("insert_result")
    Call<ResponseBody> doInsertResult(@Body RequestBody jsonObject);

    @POST("generate_edit_grid_board")
    Call<ResponseBody> doGenerateGridEdit(@Body RequestBody jsonObject);

    @POST("edit_grid_board")
    Call<ResponseBody> doGenerateEditGrid(@Body RequestBody jsonObject);

    @POST("update_result")
    Call<ResponseBody> doUpdate(@Body RequestBody jsonObject);

    @POST("student_annual_result")
    Call<ResponseBody> doGenerateAnnualReport(@Body RequestBody jsonObject);

    @POST("student_pretest_result")
    Call<ResponseBody> doGeneratePretestResult(@Body RequestBody jsonObject);

    @POST("teacher_list")
    Call<ResponseBody> doTeacherResult(@Body RequestBody jsonObject);

    @POST("teacher_mobile_numer")
    Call<ResponseBody> doTeacherMobileno(@Body RequestBody jsonObject);

    @POST("library_book_list")
    Call<ResponseBody> doLibraryBookList(@Body RequestBody jsonObject);

    @POST("library_book_search")
    Call<ResponseBody> doLibraryBookSearch(@Body RequestBody jsonObject);

    @POST("all_library_book")
    Call<ResponseBody> doLibraryBookPagination(@Body RequestBody jsonObject);
}
