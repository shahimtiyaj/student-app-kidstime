package com.arena.kidsstudent.utils;
  
public class URLs {

	public static String DOMAIN = "http://portal.kidstimebd.com/index.php/apps_api/";
	//public static String DOMAIN = "http://192.168.10.108:8081/kidstime/index.php/apps_api/";

	public static String LOGIN = DOMAIN +"check_login/";
	public static String GET_USER_INFO = DOMAIN +"get_user_info/";
	public static String GENERAL_NOTICE = DOMAIN +"get_general_notice/";
	public static String PERSONAL_NOTICE = DOMAIN +"get_personal_notice/";
	public static String PAYEMENT_LIST = DOMAIN +"get_payment_list/";
	public static String PAYEMENT_DETAILS = DOMAIN +"get_payment_details/";
	public static String BOOK_LIST = DOMAIN +"get_book_list/";
	public static String CHANGE_PASSWORD = DOMAIN +"set_change_password/";
	public static String ATTENDANCE = DOMAIN +"get_student_attendance/";
	public static final String GETNOTE = DOMAIN +"get_notebook_list/";
	public static final String GETEXAMLIST = DOMAIN +"get_exam_list";
	public static final String GETEXAMRESULT = DOMAIN + "get_exam_result/" ;
	public static final String GETROUTINE = DOMAIN + "get_class_routines/" ;
	public static final String CONTUCT = DOMAIN + "get_all_contact_list/" ;
	public static final String SYLLUBUS = DOMAIN + "get_syllebus_info/" ;
	public static final String PAMENT = DOMAIN + "get_due_payment/" ;
	//New-----------------------------------------------------------------
	public static final String GET_SUBJECT = DOMAIN + "get_subject_list/" ;

}
  