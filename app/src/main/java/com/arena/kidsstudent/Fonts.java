package com.arena.kidsstudent;

import android.graphics.Typeface;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Fonts extends AppCompatActivity {

    Typeface Light,Medium,Regular,SemiBold;


    public  Typeface getLight() {
        return Light=Typeface.createFromAsset(getAssets(), "Sofia Pro Light.ttf");
    }

    public Typeface getMedium() {
        return Medium=Typeface.createFromAsset(getAssets(), "Sofia Pro Medium.ttf");
    }

    public Typeface getRegular() {
        return Regular=Typeface.createFromAsset(getAssets(), "Sofia Pro Regular.ttf");
    }

    public Typeface getSemiBold() {
        return SemiBold=Typeface.createFromAsset(getAssets(), "Sofia Pro SemiBold.ttf");
    }

    public String getCurrentDate (){
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        Date date = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        return sdf.format(date);

    }
}
