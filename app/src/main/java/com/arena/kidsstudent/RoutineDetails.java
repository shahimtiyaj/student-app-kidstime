package com.arena.kidsstudent;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.arena.kidsstudent.Model.RoutineM;
import com.arena.kidsstudent.Model.TimeM;

import java.util.ArrayList;
import java.util.HashMap;

public class RoutineDetails extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ArrayList<TimeM> arrayList;
    ArrayList<RoutineM> routineMS;
    HashMap<String,String> map;
    ArrayList< HashMap<String,String>> List;
    ListView lv;
    ListAdapter adapter;
    TextView info;
   Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routine_details);
        info = findViewById(R.id.info);
        arrayList = new ArrayList<TimeM>();
        routineMS = new ArrayList<RoutineM>();
        List = new ArrayList<HashMap<String,String>>();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitleTextColor(Color.parseColor("#000000"));
        lv = findViewById(R.id.listview);
        lv.setOnItemClickListener(this);
        arrayList.clear();
        List.clear();
        Intent i = getIntent();
        arrayList = (ArrayList<TimeM>) i.getSerializableExtra("time");
        routineMS = (ArrayList<RoutineM>) i.getSerializableExtra("routine");
        String title = i.getStringExtra("title");
        setTitle(title);
        int t = routineMS.size();

        Log.e("t",t+"");

        for (int j = 0 ; j < arrayList.size();j++ ){

            map = new HashMap<String, String>();
            map.put("id",arrayList.get(j).getId());
            map.put("title",arrayList.get(j).getTitle());

            List.add(map);



        }

        adapter = new SimpleAdapter(RoutineDetails.this, List, R.layout.daycl,
                new String[] {"title"},

                new int[] {
                        R.id.day});

        lv.setAdapter(adapter);


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.Logout:
                Intent i = new Intent(RoutineDetails.this,Login.class);
                startActivity(i);
                finish();
                return true;
            case R.id.Changepass:
                Intent j = new Intent(RoutineDetails.this,ChangePass.class);
                startActivity(j);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub
        String teacher;
        HashMap<String, String> map = List.get(arg2);
        String id = map.get("id");
        if (routineMS.isEmpty()){
            info.setText("No Class");
        }

                if (Integer.parseInt(id)>routineMS.size()){

                    info.setText("No class");

                }
        for (int i = 0 ; i < routineMS.size(); i++){

            if (id.equalsIgnoreCase(routineMS.get(i).getClass_time_id())){

                if (routineMS.get(i).getTeacher_name().equalsIgnoreCase("null")){
                    teacher= "";


                info.setText(routineMS.get(i).getSubject()+"\n"+teacher);
                }else if(routineMS.get(i).getSubject().equalsIgnoreCase(""))
                {

                }else
                    info.setText(routineMS.get(i).getSubject()+"\n"+routineMS.get(i).getTeacher_name());


            }

        }




    }
}
