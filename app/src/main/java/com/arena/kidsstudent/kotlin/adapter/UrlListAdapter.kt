package com.arena.kidsstudent.kotlin.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.arena.kidsstudent.R
import com.arena.kidsstudent.kotlin.model.OnlineClassUrl
import com.arena.kidsstudent.kotlin.view.MessageDetails
import com.arena.kidsstudent.kotlin.view.UrlDetails

class UrlListAdapter(private val mContext: Context, private val urlList: List<OnlineClassUrl>) :
    RecyclerView.Adapter<UrlListAdapter.HRViewHolder>() {
    private var filteredresultUrlList: List<OnlineClassUrl>? = null

    inner class HRViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tSubject: TextView
        var tBatch: TextView
        var tTeacher: TextView
        var action: TextView

        init {
            tSubject = view.findViewById<View>(R.id.tSubject) as TextView
            tBatch = view.findViewById<View>(R.id.tBatch) as TextView
            tTeacher = view.findViewById<View>(R.id.tTeacher) as TextView
            action = view.findViewById<View>(R.id.view) as TextView
        }
    }

    init {
        this.filteredresultUrlList = urlList
    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HRViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.online_class_url_row, parent, false)
        // Return a new holder instance
        return HRViewHolder(itemView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: HRViewHolder, position: Int) {
        try {
            // Get the item model based on position
            val url = filteredresultUrlList?.get(position)

                 holder.tSubject.text = url?.getClassName()
                 holder.tBatch.text = url?.getSection()
                 holder.tTeacher.text = url?.getUrl()?.getTname().toString()

            holder.action.setOnClickListener {
                val intent = Intent(mContext, UrlDetails::class.java)
                intent.putExtra("comments",url?.getUrl()?.getComments());
                intent.putExtra("url",url?.getUrl()?.getUrl());
                mContext.startActivity(intent)
            }

        } catch (e: Exception) {
            Log.e("EXCEPTION", e.toString())
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredresultUrlList?.size!!
    }


}