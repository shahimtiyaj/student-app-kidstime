package com.arena.kidsstudent.kotlin.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PaymentSubjectPost {

    @SerializedName("student_id")
    @Expose
    private var studentId: String? = null

    @SerializedName("subjectList")
    @Expose
    private var subjectList: List<Subject?>? = null

    fun getStudentId(): String? {
        return studentId
    }

    fun setStudentId(studentId: String?) {
        this.studentId = studentId
    }

    fun getSubjectList(): List<Subject?>? {
        return subjectList
    }

    fun setSubjectList(subjectList: List<Subject?>?) {
        this.subjectList = subjectList
    }


    class Subject {

        @SerializedName("id")
        @Expose
        private var id: String? = null

        @SerializedName("ammount")
        @Expose
        private var ammount: String? = null

        fun getId(): String? {
            return id
        }

        fun setId(id: String?) {
            this.id = id
        }

        fun getAmmount(): String? {
            return ammount
        }

        fun setAmmount(ammount: String?) {
            this.ammount = ammount
        }

    }
}