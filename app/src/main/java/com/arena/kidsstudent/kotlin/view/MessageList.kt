package com.arena.kidsstudent.kotlin.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arena.kidsstudent.*
import com.arena.kidsstudent.kotlin.adapter.MessageListAdapter
import com.arena.kidsstudent.kotlin.model.MessageData
import com.arena.kidsstudent.kotlin.viewmodel.DataViewModel
import com.arena.kidsstudent.kotlin.viewmodel.DataViewModelFactory

class MessageList : AppCompatActivity() {
    var parent: Parent? = null

    private lateinit var dataViewModel: DataViewModel
    private var messageList = ArrayList<MessageData>()

    private var recyclerView: RecyclerView? = null
    private lateinit var layoutManager: RecyclerView.LayoutManager
    var toolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.message_list)
        parent = Parent(this)


        dataViewModel = ViewModelProvider(
            this,
            DataViewModelFactory(this.application)
        )[DataViewModel::class.java]
        recyclerView = findViewById<View>(R.id.message_list_id) as RecyclerView
        dataViewModel.getMessageList(parent?.getPreference("user_id")!!)

        initViews()
        initObservables()

    }


    private fun initViews() {

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        title = "Message List"
        toolbar?.setTitleTextColor(Color.parseColor("#ffffff"))

        toolbar?.setNavigationOnClickListener {
            val i = Intent(this, Home::class.java)
            startActivity(i)
            finish()
        }


    }

    private fun initObservables() {

        layoutManager = LinearLayoutManager(applicationContext)

        dataViewModel.messageList.observe(this, androidx.lifecycle.Observer { messageList ->
            this.messageList = messageList as ArrayList<MessageData>
            setAdapter(messageList)
        })

    }

    private fun setAdapter(messageList: List<MessageData>) {
        recyclerView?.layoutManager = LinearLayoutManager(this)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.adapter = MessageListAdapter(this, messageList)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.optionmenu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.Logout -> {
                val i = Intent(this@MessageList, Login::class.java)
                startActivity(i)
                finishAffinity()
                true
            }
            R.id.Changepass -> {
                val j = Intent(this@MessageList, ChangePass::class.java)
                startActivity(j)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onBackPressed() {
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.dash) {
            val i = Intent(this, Home::class.java)
            startActivity(i)
            finish()
        } else if (id == R.id.profile) {
            val i = Intent(this, Profile::class.java)
            startActivity(i)
            finish()
        } else if (id == R.id.payment) {
            val i = Intent(this, Payment_list::class.java)
            startActivity(i)
            finish()
        } else if (id == R.id.attendance) {
            parent?.attendance
            val i = Intent(this, attendance::class.java)
            startActivity(i)
            finish()
        } else if (id == R.id.classroutine) {
            val i = Intent(this, Routine::class.java)
            startActivity(i)
            finish()
        }
        val drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }
}