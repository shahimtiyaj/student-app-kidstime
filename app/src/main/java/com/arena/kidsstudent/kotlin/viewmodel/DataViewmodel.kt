package com.arena.kidsstudent.kotlin.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.arena.kidsstudent.kotlin.Repository.DataRepository
import com.arena.kidsstudent.kotlin.model.PaymentSubject
import com.arena.kidsstudent.kotlin.view.OnlineRegistration
import com.google.gson.JsonArray
import com.google.gson.JsonObject

class DataViewModel(application: Application) : AndroidViewModel(application) {

    private val dataRepository = DataRepository(application)
    val subjectList = dataRepository.subjectList
    val batchList = dataRepository.batchList
    val teacherList = dataRepository.teacherList
    val classUrlList = dataRepository.classUrlList
    val messageList = dataRepository.messageList
    val PaymentSubjectList = dataRepository.paymentSubjectList

    var apiSuccess = dataRepository.apiSuccess
    var apiError = dataRepository.apiError
    var student_no = dataRepository.student_no
    var paymentAmount = dataRepository.paymentAmount
    var paymentId = dataRepository.paymentId

    var isStudentName = MutableLiveData<Boolean>()
    var isFatherName = MutableLiveData<Boolean>()
    var isMotherName = MutableLiveData<Boolean>()
    var isAddress = MutableLiveData<Boolean>()
    var isMobile = MutableLiveData<Boolean>()
    var isEmail = MutableLiveData<Boolean>()
    var isSubjectSelect = MutableLiveData<Boolean>()
    var isBatch = MutableLiveData<Boolean>()
    var isTeacher = MutableLiveData<Boolean>()
    var isMessage = MutableLiveData<Boolean>()

    private var apiStatus = dataRepository.apiSuccess

    fun getSuccessStatus(): MutableLiveData<String> {
        if (apiStatus == null) {
            apiStatus = MutableLiveData()
        }
        return apiStatus
    }

    fun postTransactionInfo(
        tranId: String,
        amount: String,
        paymentId: String,
        cardType: String,
        status: String
    ) {

        val jsonObj = JsonObject()
        jsonObj.addProperty("tranId", tranId)
        jsonObj.addProperty("amount", amount)
        jsonObj.addProperty("paymentId", paymentId)
        jsonObj.addProperty("cardType", cardType)
        jsonObj.addProperty("status", status)

        dataRepository.postTransactionInfo(jsonObj)
    }

    fun postPaymentSubData(paymentSubjectList: ArrayList<PaymentSubject.Subject>) {

        val jobject = JsonObject()
        val jsonArray = JsonArray()

        jobject.addProperty("student_id", OnlineRegistration.student_id)

        paymentSubjectList.forEach { payment ->
            if (payment.getSelected()) {
                val jsonObj = JsonObject()
                jsonObj.addProperty("sub_id", payment.getId())
                jsonObj.addProperty("amount", payment.getAmmount())
                jsonArray.add(jsonObj)
                jobject.add("admissionInfo", jsonArray)
            }
        }

        dataRepository.postPaymentSubData(jobject)
    }

    fun getPaymentSubjectList() {
        dataRepository.getPaymentSubject()
    }

    fun getSubjectList(userId: String) {
        val jsonbody = JsonObject()
        jsonbody.addProperty("user_id", userId)
        dataRepository.getSubjectData(jsonbody)
    }

    fun getBatchList(subId: String) {

        val jsonObj = JsonObject()
        jsonObj.addProperty("class_id", subId)

        dataRepository.getBatchData(jsonObj)
    }

    fun getTeacherList(classId: String, batchId: String) {

        val jsonObj = JsonObject()
        jsonObj.addProperty("class_id", classId)
        jsonObj.addProperty("section_id", batchId)

        dataRepository.getTeacherData(jsonObj)
    }

    fun postStudentMessage(
        user_id: String,
        subId: String,
        batchId: String,
        teacherId: String,
        message: String
    ) {

        val jsonObj = JsonObject()
        jsonObj.addProperty("user_id", user_id)
        jsonObj.addProperty("subject_id", subId)
        jsonObj.addProperty("batch_id", batchId)
        jsonObj.addProperty("teacher_id", teacherId)
        jsonObj.addProperty("message", message)

        when {
            subId.isNullOrEmpty() || subId == "0" -> {
                isSubjectSelect.value = true
            }

            batchId.isNullOrEmpty() || batchId == "0" -> {

                isBatch.value = true
            }

            teacherId.isNullOrEmpty() || teacherId == "0" -> {

                isTeacher.value = true
            }

            message.isNullOrEmpty() -> {

                isMessage.value = true
            }

            else -> {
                dataRepository.postStudentMessage(jsonObj)
            }

        }


    }

    fun getMessageList(userId: String) {
        val jsonObj = JsonObject()
        jsonObj.addProperty("user_id", userId)
        dataRepository.getOnlineMessageData(jsonObj)
    }

    fun getClassUrlList(userId: String) {
        val jsonObj = JsonObject()
        jsonObj.addProperty("user_id", userId)
        dataRepository.getOnlineClassUrl(jsonObj)
    }

    fun submitRegistration(
        stName: String,
        stFatherName: String,
        stMotherName: String,
        address: String,
        mobile: String,
        email: String,
        subjectChoose: String
    ) {

        val jsonObj = JsonObject()
        jsonObj.addProperty("st_name", stName)
        jsonObj.addProperty("st_father_name", stFatherName)
        jsonObj.addProperty("st_mother_name", stMotherName)
        jsonObj.addProperty("address", address)
        jsonObj.addProperty("mobile", mobile)
        jsonObj.addProperty("email", email)
        jsonObj.addProperty("subjectChoose", subjectChoose)

        when {
            stName.isNullOrEmpty() -> {
                isStudentName.value = true
            }

            stFatherName.isNullOrEmpty() -> {

                isFatherName.value = true
            }

            stMotherName.isNullOrEmpty() -> {

                isMotherName.value = true
            }

            address.isNullOrEmpty() -> {

                isAddress.value = true
            }

            mobile.isNullOrEmpty() -> {

                isMobile.value = true
            }

            email.isNullOrEmpty() -> {

                isEmail.value = true
            }

            else -> {
                dataRepository.postSignUpMessage(jsonObj)
            }
        }
    }
}

class DataViewModelFactory(var application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DataViewModel::class.java)) {
            return DataViewModel(application) as T
        }

        throw IllegalStateException("Unknown ViewModel class")
    }
}
