package com.arena.kidsstudent.kotlin.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.arena.kidsstudent.R
import com.arena.kidsstudent.kotlin.model.MessageData
import com.arena.kidsstudent.kotlin.model.PaymentSubject
import com.arena.kidsstudent.kotlin.model.PaymentSubjectPost

class PaymentSubWiseAdapter(private val mContext: Context,
                        private val paymentSubjectList: List<PaymentSubject.Subject>,
                        private val listener: OnSaveButtonClick
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var filteredPaymentSubject: List<PaymentSubject.Subject>? = null

    companion object {
        val markInputRqHelperList = ArrayList<PaymentSubjectPost.Subject>()
    }

    interface OnSaveButtonClick {
        fun onSave(markInputRqHelperList:ArrayList<PaymentSubjectPost.Subject>)
    }

    init {
        this.filteredPaymentSubject = paymentSubjectList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == paymentSubjectList.size) {
            // Inflate the custom layout
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.payment_sub_button, parent, false)
            // Return a new holder instance
            ButtonViewHolder(itemView)
        } else {
            // Inflate the custom layout
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.payment__subject_row, parent, false)
            // Return a new holder instance
            PaymentSubjectViewHolder(itemView)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == paymentSubjectList.size) {

            (holder as ButtonViewHolder).saveButton.setOnClickListener {
                listener.onSave(markInputRqHelperList)
            }
        } else {
            // Get the item model based on position
            val paymentSubject = filteredPaymentSubject?.get(position)

            // Set item views based on our views and data model
            (holder as PaymentSubjectViewHolder).txtSub.text=paymentSubject?.getTitle()
            holder.txtAmt.text = paymentSubject?.getAmmount()

            filteredPaymentSubject?.get(position)?.getSelected()?.let { holder.checkBox.isChecked = it }
            holder.checkBox.tag = position

            holder.checkBox.setOnClickListener {
                val pos = holder.checkBox.tag as Int

                if (filteredPaymentSubject?.get(pos)?.getSelected()!!) {
                    filteredPaymentSubject?.get(pos)?.setSelected(false)

                } else {
                    filteredPaymentSubject?.get(pos)?.setSelected(true)
                }
            }
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return (filteredPaymentSubject?.size!! + 1)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    class PaymentSubjectViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var txtSub: TextView
        var txtAmt: TextView
        var checkBox: CheckBox

        init {
            txtSub = view.findViewById<View>(R.id.txtSubject) as TextView
            txtAmt = view.findViewById<View>(R.id.txt_amt) as TextView
            checkBox = view.findViewById<View>(R.id.checkbox) as CheckBox
        }
    }

    class ButtonViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        // var absentCheckBox: CheckBox = view.findViewById(R.id.absent_sms_check)
        var saveButton: Button = view.findViewById(R.id.button_save)
    }
}