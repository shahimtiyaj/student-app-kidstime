package com.arena.kidsstudent.kotlin.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.arena.kidsstudent.Home
import com.arena.kidsstudent.R


class MessageDetails : AppCompatActivity() {
    var toolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message_list_details)
        initViews()
    }

    private fun initViews() {
        val ownMessage: TextView = findViewById(R.id.ownMessage)
        val detailsMessage: TextView = findViewById(R.id.teacherMessage)

        val intent = intent
        val message = intent.getStringExtra("message")
        val tMessage = intent.getStringExtra("tmessage")
        ownMessage.text = message
        detailsMessage.text = tMessage

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        title = "Message Details"
        toolbar?.setTitleTextColor(Color.parseColor("#ffffff"))

        toolbar?.setNavigationOnClickListener {
            val i = Intent(this, MessageList::class.java)
            startActivity(i)
            finish()
        }

    }

}