package com.arena.kidsstudent.kotlin.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.arena.kidsstudent.Home
import com.arena.kidsstudent.R

class UrlDetails : AppCompatActivity() {
    var toolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_online_class_url_details)
        initViews()
    }

    private fun initViews() {
        val comments: TextView = findViewById(R.id.tComments)
        val url: TextView = findViewById(R.id.tUrl)

        val intent = intent
        val message = intent.getStringExtra("comments")
        val tMessage = intent.getStringExtra("url")
        comments.text = message
        url.text = tMessage

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        title = "Online Class Url Details"
        toolbar?.setTitleTextColor(Color.parseColor("#ffffff"))

        toolbar?.setNavigationOnClickListener {
            val i = Intent(this, OnlineClassUrl::class.java)
            startActivity(i)
            finish()
        }
    }

}