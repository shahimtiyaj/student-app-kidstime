package com.arena.kidsstudent.kotlin.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import com.arena.kidsstudent.Home
import com.arena.kidsstudent.Login
import com.arena.kidsstudent.Parent
import com.arena.kidsstudent.R
import com.arena.kidsstudent.kotlin.model.BatchData
import com.arena.kidsstudent.kotlin.model.SubjectData
import com.arena.kidsstudent.kotlin.model.TeacherData
import com.arena.kidsstudent.kotlin.viewmodel.DataViewModel
import com.arena.kidsstudent.kotlin.viewmodel.DataViewModelFactory
import com.arena.kidsstudent.utils.MyUtils
import es.dmoral.toasty.Toasty


class SendMessage : AppCompatActivity() {
    var parent: Parent? = null
    var toolbar: Toolbar? = null
    var etTitle: EditText? = null
    var etDescription: EditText? = null
    var btn_send: Button? = null
    var btn_reset: Button? = null
    var title: String? = null

    var description: String? = null
    var teacherName: String? = null
    var subject: Spinner? = null
    var batch: Spinner? = null
    var teacher: Spinner? = null

    var subjectId: String? = "0"
    var batchId: String? = "0"
    var teacherId: String? = "0"
    private lateinit var subjectDataList: List<SubjectData.Subject>
    private lateinit var batchDataList: List<BatchData.Batch>
    private lateinit var teacherDataList: List<TeacherData.Teacher>

    private lateinit var dataViewModel: DataViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sendmessage)


        dataViewModel = ViewModelProvider(this, DataViewModelFactory(this.application))[DataViewModel::class.java]

        parent = Parent(this)

        dataViewModel.getSubjectList(parent?.getPreference("user_id")!!)

        subject = findViewById<View>(R.id.spSubject) as Spinner
        batch = findViewById<View>(R.id.spBatch) as Spinner
        teacher = findViewById<View>(R.id.spTeacher) as Spinner

        etDescription = findViewById<View>(R.id.et_des) as EditText
        btn_send = findViewById<View>(R.id.btn_send) as Button
        btn_reset = findViewById<View>(R.id.btn_reset) as Button

        btn_send!!.setOnClickListener { view: View? ->
            description = etDescription!!.text.toString()
            dataViewModel.postStudentMessage(parent?.getPreference("user_id")!!, subjectId!!, batchId!!, teacherId!!, description!!)
        }

        btn_reset!!.setOnClickListener { view: View? ->
            etDescription!!.text.clear()
        }

        initObservables()

        initViews()

    }

    private fun initViews(){

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        title = "Send Message"
        toolbar?.setTitleTextColor(Color.parseColor("#ffffff"))

        toolbar?.setNavigationOnClickListener {
            val i = Intent(this, Home::class.java)
            startActivity(i)
            finish()
        }


        subject?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position == 0) {
                        subjectId = "0"
                    } else {

                        getSubjectId(parent?.getItemAtPosition(position).toString())

                    }
                }
            }

        batch?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {}
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position == 0) {
                        batchId = "0"
                    } else {
                        getBatchId(parent?.getItemAtPosition(position).toString())

                    }
                }
            }

        teacher?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position != 0) {
                        teacherName = parent!!.getItemAtPosition(position).toString()

                        if (position == 0) {
                            teacherId = "0"
                        } else {
                            getTeacherId(parent?.getItemAtPosition(position).toString())

                        }

                    }
                }
            }
    }


    private fun initObservables() {

        dataViewModel.subjectList.observe(this, androidx.lifecycle.Observer {
            subjectDataList=it

            setSubjectSpinner(it)

        })


        dataViewModel.batchList.observe(this, androidx.lifecycle.Observer {
            batchDataList=it

            setBatchSpinner(it)

        })

        dataViewModel.teacherList.observe(this, androidx.lifecycle.Observer {
            teacherDataList=it

            setTeacherSpinner(it)

        })
        dataViewModel.apiSuccess.observe(this, androidx.lifecycle.Observer {

            if (!it.isNullOrEmpty()){
                Toasty.success(this!!, it, Toasty.LENGTH_LONG).show()

                val i = Intent(this, MessageList::class.java)
                startActivity(i)
                finish()
            }

        })

        dataViewModel.apiError.observe(this, androidx.lifecycle.Observer {

         if (!it.isNullOrEmpty()){
             Toasty.error(this!!, it, Toasty.LENGTH_LONG).show()
         }

        })

        dataViewModel.isSubjectSelect.observe(this, androidx.lifecycle.Observer {
            if (it) {
                MyUtils.showErrorToasty(this, "Subject Select can't empty!")
                dataViewModel.isSubjectSelect.value = false

            }
        })

        dataViewModel.isBatch.observe(this, androidx.lifecycle.Observer {
            if (it) {
                MyUtils.showErrorToasty(this, "Batch Select can't empty!")
                dataViewModel.isBatch.value = false

            }
        })

        dataViewModel.isTeacher.observe(this, androidx.lifecycle.Observer {
            if (it) {
                MyUtils.showErrorToasty(this, "Teacher Select can't empty!")
                dataViewModel.isTeacher.value = false

            }
        })

        dataViewModel.isMessage.observe(this, androidx.lifecycle.Observer {
            if (it) {
                etDescription?.error = "Message Description can't left empty"
                dataViewModel.isEmail.value = false
                MyUtils.requestFocus(etDescription!!, this)
            }
        })

    }


    private fun setSubjectSpinner(subjectDataList: List<SubjectData.Subject>) {
        val subjectTempList = ArrayList<String>()
        subjectTempList.add("Select Subject")

        subjectDataList.forEach { subject ->
            subjectTempList.add(subject.getTitle().toString())
        }

        val sectionAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            subjectTempList
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        subject?.adapter = sectionAdapter
    }

    private fun setBatchSpinner(batchDataList: List<BatchData.Batch>) {
        val batchTempList = ArrayList<String>()
        batchTempList.add("Select Batch")

        batchDataList.forEach { batch ->
            batchTempList.add(batch.getTitle().toString())
        }

        val sectionAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            batchTempList
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        batch?.adapter = sectionAdapter
    }

    private fun setTeacherSpinner(batchDataList: List<TeacherData.Teacher>) {
        val batchTempList = ArrayList<String>()
        batchTempList.add("Select Teacher")

        batchDataList.forEach { teacher ->
            batchTempList.add(teacher.getName().toString())
        }

        val sectionAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            batchTempList
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        teacher?.adapter = sectionAdapter
    }

    private fun getSubjectId(subName: String) {
        subjectDataList.forEach { subject ->
            if (subject.getTitle().equals(subName, true)) {
                subjectId = subject.getId().toString()
                dataViewModel.getBatchList(subjectId!!)
            }
        }
    }

    private fun getBatchId(batchName: String) {
        batchDataList.forEach { batch ->
            if (batch.getTitle().equals(batchName, true)) {
                batchId = batch.getId().toString()
                dataViewModel.getTeacherList(subjectId!!, batchId!!)
            }
        }
    }

    private fun getTeacherId(teacherName: String) {
        teacherDataList.forEach { teacher ->
            if (teacher.getName().equals(teacherName, true)) {
                teacherId = teacher.getId().toString()
            }
        }
    }

}
