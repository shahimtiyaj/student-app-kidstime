package com.arena.kidsstudent.kotlin.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class SubjectData {
    @SerializedName("subjectList")
    @Expose
    private var subjectList: List<Subject>? = null

    fun getSubjectList(): List<Subject?>? {
        return subjectList
    }

    fun setSubjectList(subjectList: List<Subject>?) {
        this.subjectList = subjectList
    }

    class Subject {
        @SerializedName("id")
        @Expose
        private var id: String? = null

        @SerializedName("title")
        @Expose
        private var title: String? = null

        @SerializedName("sid")
        @Expose
        private var sid: String? = null

        @SerializedName("sname")
        @Expose
        private var sname: String? = null

        fun getId(): String? {
            return id
        }

        fun setId(id: String?) {
            this.id = id
        }

        fun getTitle(): String? {
            return title
        }

        fun setTitle(title: String?) {
            this.title = title
        }

        fun getSid(): String? {
            return sid
        }

        fun setSid(sid: String?) {
            this.sid = sid
        }

        fun getSname(): String? {
            return sname
        }

        fun setSname(sname: String?) {
            this.sname = sname
        }
    }
}