package com.arena.kidsstudent.kotlin.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class OnlineClassUrl {

    @SerializedName("class")
    @Expose
    private var className: String? = null

    @SerializedName("section")
    @Expose
    private var section: String? = null

    @SerializedName("url")
    @Expose
    private var url: Url? = null

    fun getClassName(): String? {
        return className
    }

    fun setClassName(_className_: String) {
        className = _className_
    }

    fun getSection(): String? {
        return section
    }

    fun setSection(section: String?) {
        this.section = section
    }

    fun getUrl(): Url? {
        return url
    }

    fun setUrl(url: Url?) {
        this.url = url
    }

    class Url {
        @SerializedName("id")
        @Expose
        private var id: String? = null

        @SerializedName("class_id")
        @Expose
        private var classId: String? = null

        @SerializedName("section_id")
        @Expose
        private var sectionId: String? = null

        @SerializedName("teacher_id")
        @Expose
        private var teacherId: String? = null

        @SerializedName("comments")
        @Expose
        private var comments: String? = null

        @SerializedName("url")
        @Expose
        private var url: String? = null

        @SerializedName("create_at")
        @Expose
        private var createAt: String? = null

        @SerializedName("tname")
        @Expose
        private var tname: Any? = null

        fun getId(): String? {
            return id
        }

        fun setId(id: String?) {
            this.id = id
        }

        fun getClassId(): String? {
            return classId
        }

        fun setClassId(classId: String?) {
            this.classId = classId
        }

        fun getSectionId(): String? {
            return sectionId
        }

        fun setSectionId(sectionId: String?) {
            this.sectionId = sectionId
        }

        fun getTeacherId(): String? {
            return teacherId
        }

        fun setTeacherId(teacherId: String?) {
            this.teacherId = teacherId
        }

        fun getComments(): String? {
            return comments
        }

        fun setComments(comments: String?) {
            this.comments = comments
        }

        fun getUrl(): String? {
            return url
        }

        fun setUrl(url: String?) {
            this.url = url
        }

        fun getCreateAt(): String? {
            return createAt
        }

        fun setCreateAt(createAt: String?) {
            this.createAt = createAt
        }

        fun getTname(): Any? {
            return tname
        }

        fun setTname(tname: Any?) {
            this.tname = tname
        }
    }

}