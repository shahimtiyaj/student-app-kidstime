package com.arena.kidsstudent.kotlin.adapter

import android.content.Context
import android.content.Intent
import android.system.Os.link
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.arena.kidsstudent.R
import com.arena.kidsstudent.kotlin.model.MessageData
import com.arena.kidsstudent.kotlin.view.MessageDetails


class MessageListAdapter(
    private val mContext: Context,
    private val messageList: List<MessageData>
) :
    RecyclerView.Adapter<MessageListAdapter.HRViewHolder>() {
    private var filteredresultmessageListList: List<MessageData>? = null

    inner class HRViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tName: TextView
        var mobile: TextView
        var subject: TextView
        var batch: TextView
        var action: TextView

        init {
            tName = view.findViewById<View>(R.id.tName) as TextView
            mobile = view.findViewById<View>(R.id.mobile) as TextView
            subject = view.findViewById<View>(R.id.subject) as TextView
            batch = view.findViewById<View>(R.id.batch) as TextView
            action = view.findViewById<View>(R.id.view) as TextView
        }
    }

    init {
        this.filteredresultmessageListList = messageList
    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HRViewHolder {
        // Inflate the custom layout
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.messagelist_row, parent, false)
        // Return a new holder instance
        return HRViewHolder(itemView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(holder: HRViewHolder, position: Int) {
        try {
            // Get the item model based on position
            val message = filteredresultmessageListList?.get(position)

            holder.tName.text = message?.getName()
            holder.mobile.text = message?.getMobileNo()
            holder.subject.text = message?.getCname()
            holder.batch.text = message?.getSecname()

            holder.action.setOnClickListener {
                val intent = Intent(mContext, MessageDetails::class.java)
                intent.putExtra("message",message?.getStMessage());
                intent.putExtra("tmessage",message?.gettMessage());
                mContext.startActivity(intent)
            }


        } catch (e: Exception) {
            Log.e("EXCEPTION", e.toString())
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return filteredresultmessageListList?.size!!
    }


}


/*package com.arena.kidsstudent.kotlin.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.arena.kidsstudent.R
import java.util.*

class MessageListAdapter(
    private val mContext: Context,
    private val itemsList: ArrayList<HashMap<String, String>>?
) :
    RecyclerView.Adapter<MessageListAdapter.SingleItemRowHolder>() {
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        i: Int
    ): SingleItemRowHolder {
        val v: View =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.messagelist_row, null)
        return SingleItemRowHolder(v)
    }

    override fun onBindViewHolder(holder: SingleItemRowHolder, i: Int) {

        holder.tName.text = itemsList?.get(i)!!["product_name"]
        holder.mobile.text = itemsList[i]["vat"]
        holder.subject.text = price
        holder.batch.text = itemsList[i]["quantity"]
    }

    override fun getItemCount(): Int {
        return itemsList?.size ?: 0
    }

    inner class SingleItemRowHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tName: TextView
        var mobile: TextView
        var subject: TextView
        var batch: TextView

        init {
            tName = view.findViewById<View>(R.id.tName) as TextView
            mobile = view.findViewById<View>(R.id.mobile) as TextView
            subject = view.findViewById<View>(R.id.subject) as TextView
            batch = view.findViewById<View>(R.id.batch) as TextView
        }
    }

}*/
