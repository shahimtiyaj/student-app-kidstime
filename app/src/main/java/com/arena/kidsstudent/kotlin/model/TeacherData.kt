package com.arena.kidsstudent.kotlin.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class TeacherData {
    @SerializedName("teacherlist")
    @Expose
    private var teacherlist: List<Teacher>? = null

    fun getTeacherlist(): List<Teacher?>? {
        return teacherlist
    }

    fun setTeacherlist(teacherlist: List<Teacher>?) {
        this.teacherlist = teacherlist
    }

    class Teacher {
        @SerializedName("id")
        @Expose
        private var id: String? = null

        @SerializedName("name")
        @Expose
        private var name: String? = null

        @SerializedName("teacher_number")
        @Expose
        private var teacherNumber: String? = null

        @SerializedName("personal_number")
        @Expose
        private var personalNumber: String? = null

        @SerializedName("designation_id")
        @Expose
        private var designationId: String? = null

        @SerializedName("username")
        @Expose
        private var username: String? = null

        @SerializedName("passwd")
        @Expose
        private var passwd: String? = null

        @SerializedName("dob")
        @Expose
        private var dob: String? = null

        @SerializedName("gender")
        @Expose
        private var gender: String? = null

        @SerializedName("blood_group_id")
        @Expose
        private var bloodGroupId: String? = null

        @SerializedName("religion_id")
        @Expose
        private var religionId: String? = null

        @SerializedName("address")
        @Expose
        private var address: String? = null

        @SerializedName("mobile_no")
        @Expose
        private var mobileNo: String? = null

        @SerializedName("email")
        @Expose
        private var email: String? = null

        @SerializedName("status")
        @Expose
        private var status: String? = null

        @SerializedName("photo")
        @Expose
        private var photo: String? = null

        @SerializedName("holder_sign")
        @Expose
        private var holderSign: Any? = null

        @SerializedName("barcode")
        @Expose
        private var barcode: Any? = null

        @SerializedName("relevant_subject_id")
        @Expose
        private var relevantSubjectId: String? = null

        @SerializedName("is_class_teacher")
        @Expose
        private var isClassTeacher: String? = null

        @SerializedName("class_id")
        @Expose
        private var classId: String? = null

        @SerializedName("section_id")
        @Expose
        private var sectionId: String? = null

        @SerializedName("edulabel")
        @Expose
        private var edulabel: String? = null

        @SerializedName("order")
        @Expose
        private var order: String? = null

        @SerializedName("created_at")
        @Expose
        private var createdAt: String? = null

        @SerializedName("updated_at")
        @Expose
        private var updatedAt: String? = null

        @SerializedName("created_by")
        @Expose
        private var createdBy: String? = null

        @SerializedName("updated_by")
        @Expose
        private var updatedBy: Any? = null


        fun getId(): String? {
            return id
        }

        fun setId(id: String?) {
            this.id = id
        }

        fun getName(): String? {
            return name
        }

        fun setName(name: String?) {
            this.name = name
        }

        fun getTeacherNumber(): String? {
            return teacherNumber
        }

        fun setTeacherNumber(teacherNumber: String?) {
            this.teacherNumber = teacherNumber
        }

        fun getPersonalNumber(): String? {
            return personalNumber
        }

        fun setPersonalNumber(personalNumber: String?) {
            this.personalNumber = personalNumber
        }

        fun getDesignationId(): String? {
            return designationId
        }

        fun setDesignationId(designationId: String?) {
            this.designationId = designationId
        }

        fun getUsername(): String? {
            return username
        }

        fun setUsername(username: String?) {
            this.username = username
        }

        fun getPasswd(): String? {
            return passwd
        }

        fun setPasswd(passwd: String?) {
            this.passwd = passwd
        }

        fun getDob(): String? {
            return dob
        }

        fun setDob(dob: String?) {
            this.dob = dob
        }

        fun getGender(): String? {
            return gender
        }

        fun setGender(gender: String?) {
            this.gender = gender
        }

        fun getBloodGroupId(): String? {
            return bloodGroupId
        }

        fun setBloodGroupId(bloodGroupId: String?) {
            this.bloodGroupId = bloodGroupId
        }

        fun getReligionId(): String? {
            return religionId
        }

        fun setReligionId(religionId: String?) {
            this.religionId = religionId
        }

        fun getAddress(): String? {
            return address
        }

        fun setAddress(address: String?) {
            this.address = address
        }

        fun getMobileNo(): String? {
            return mobileNo
        }

        fun setMobileNo(mobileNo: String?) {
            this.mobileNo = mobileNo
        }

        fun getEmail(): String? {
            return email
        }

        fun setEmail(email: String?) {
            this.email = email
        }

        fun getStatus(): String? {
            return status
        }

        fun setStatus(status: String?) {
            this.status = status
        }

        fun getPhoto(): String? {
            return photo
        }

        fun setPhoto(photo: String?) {
            this.photo = photo
        }

        fun getHolderSign(): Any? {
            return holderSign
        }

        fun setHolderSign(holderSign: Any?) {
            this.holderSign = holderSign
        }

        fun getBarcode(): Any? {
            return barcode
        }

        fun setBarcode(barcode: Any?) {
            this.barcode = barcode
        }

        fun getRelevantSubjectId(): String? {
            return relevantSubjectId
        }

        fun setRelevantSubjectId(relevantSubjectId: String?) {
            this.relevantSubjectId = relevantSubjectId
        }

        fun getIsClassTeacher(): String? {
            return isClassTeacher
        }

        fun setIsClassTeacher(isClassTeacher: String?) {
            this.isClassTeacher = isClassTeacher
        }

        fun getClassId(): String? {
            return classId
        }

        fun setClassId(classId: String?) {
            this.classId = classId
        }

        fun getSectionId(): String? {
            return sectionId
        }

        fun setSectionId(sectionId: String?) {
            this.sectionId = sectionId
        }

        fun getEdulabel(): String? {
            return edulabel
        }

        fun setEdulabel(edulabel: String?) {
            this.edulabel = edulabel
        }

        fun getOrder(): String? {
            return order
        }

        fun setOrder(order: String?) {
            this.order = order
        }

        fun getCreatedAt(): String? {
            return createdAt
        }

        fun setCreatedAt(createdAt: String?) {
            this.createdAt = createdAt
        }

        fun getUpdatedAt(): String? {
            return updatedAt
        }

        fun setUpdatedAt(updatedAt: String?) {
            this.updatedAt = updatedAt
        }

        fun getCreatedBy(): String? {
            return createdBy
        }

        fun setCreatedBy(createdBy: String?) {
            this.createdBy = createdBy
        }

        fun getUpdatedBy(): Any? {
            return updatedBy
        }

        fun setUpdatedBy(updatedBy: Any?) {
            this.updatedBy = updatedBy
        }
    }
}