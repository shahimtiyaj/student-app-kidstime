package com.arena.kidsstudent.kotlin.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arena.kidsstudent.Home
import com.arena.kidsstudent.Parent
import com.arena.kidsstudent.R
import com.arena.kidsstudent.kotlin.adapter.UrlListAdapter
import com.arena.kidsstudent.kotlin.model.OnlineClassUrl
import com.arena.kidsstudent.kotlin.viewmodel.DataViewModel
import com.arena.kidsstudent.kotlin.viewmodel.DataViewModelFactory

class OnlineClassUrl : AppCompatActivity() {
    private lateinit var dataViewModel: DataViewModel
    private var onlineClassUrlList = ArrayList<OnlineClassUrl>()

    private var recyclerView: RecyclerView? = null
    private lateinit var layoutManager: RecyclerView.LayoutManager
    var parent: Parent? = null
    var toolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_online_class_url)
        dataViewModel = ViewModelProvider(
            this,
            DataViewModelFactory(this.application)
        )[DataViewModel::class.java]
        recyclerView = findViewById<View>(R.id.onlineClassUrlId) as RecyclerView

        parent = Parent(this)
        dataViewModel.getClassUrlList(parent?.getPreference("user_id")!!)

        initViews()
        initObservables()

    }

    private fun initViews() {

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        title = "Online Class Url"
        toolbar?.setTitleTextColor(Color.parseColor("#ffffff"))

        toolbar?.setNavigationOnClickListener {
            val i = Intent(this, Home::class.java)
            startActivity(i)
            finish()
        }

    }

    private fun initObservables() {

        layoutManager = LinearLayoutManager(this)

        dataViewModel.classUrlList.observe(this, androidx.lifecycle.Observer {

            setAdapter(it as List<OnlineClassUrl>)

        })
    }

    private fun setAdapter(urlList: List<OnlineClassUrl>) {
        recyclerView?.layoutManager = LinearLayoutManager(this)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.adapter = UrlListAdapter(this, urlList)
    }
}