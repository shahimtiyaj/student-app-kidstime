package com.arena.kidsstudent.kotlin.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PaymentAmount {

    @SerializedName("payment_id")
    @Expose
    private var paymentId: Int? = null

    @SerializedName("amount")
    @Expose
    private var amount: Int? = null

    fun getPaymentId(): Int? {
        return paymentId
    }

    fun setPaymentId(paymentId: Int?) {
        this.paymentId = paymentId
    }

    fun getAmount(): Int? {
        return amount
    }

    fun setAmount(amount: Int?) {
        this.amount = amount
    }
}