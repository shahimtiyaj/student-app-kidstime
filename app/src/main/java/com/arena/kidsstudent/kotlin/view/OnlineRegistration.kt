package com.arena.kidsstudent.kotlin.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import com.arena.kidsstudent.Login
import com.arena.kidsstudent.Parent
import com.arena.kidsstudent.R
import com.arena.kidsstudent.kotlin.model.SubjectData
import com.arena.kidsstudent.kotlin.viewmodel.DataViewModel
import com.arena.kidsstudent.kotlin.viewmodel.DataViewModelFactory
import com.arena.kidsstudent.utils.MyUtils.Companion.requestFocus
import es.dmoral.toasty.Toasty

class OnlineRegistration : AppCompatActivity() {

    var user_id: String? = null

    var backImg: ImageView? = null
    var parent: Parent? = null
    var toolbar: Toolbar? = null

    var studentName: EditText? = null
    var studentFatherName: EditText? = null
    var studentMotherName: EditText? = null
    var location: EditText? = null
    var mobileNo: EditText? = null
    var Email: EditText? = null
    var SubjectChoose: EditText? = null
    var btn_submit: Button? = null
    var studentname: String? = null
    var studentfatherName: String? = null
    var studentmotherName: String? = null
    var Location: String? = null
    var mobileno: String? = null
    var email: String? = null
    var subjectChoose: String? = "0"
    private lateinit var dataViewModel: DataViewModel
    var subjectId: String? = "0"
    private lateinit var subjectDataList: List<SubjectData.Subject>

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_online_registration)
        dataViewModel = ViewModelProvider(
            this,
            DataViewModelFactory(this.application)
        )[DataViewModel::class.java]

        parent = Parent(this)

        studentName = findViewById<View>(R.id.et_studentname) as EditText
        studentFatherName = findViewById<View>(R.id.et_fathername) as EditText
        studentMotherName = findViewById<View>(R.id.et_mothername) as EditText
        location = findViewById<View>(R.id.et_location) as EditText
        mobileNo = findViewById<View>(R.id.et_mobileno) as EditText
        Email = findViewById<View>(R.id.et_email) as EditText

        btn_submit = findViewById<View>(R.id.btn_submit) as Button

        dataViewModel.getSubjectList(parent?.getPreference("user_id")!!)

        btn_submit!!.setOnClickListener { view: View? ->
            studentname = studentName!!.text.toString()
            studentfatherName = studentFatherName!!.text.toString()
            studentmotherName = studentMotherName!!.text.toString()
            Location = location!!.text.toString()
            mobileno = mobileNo!!.text.toString()
            email = Email!!.text.toString()
            dataViewModel.submitRegistration(
                studentname!!,
                studentfatherName!!,
                studentmotherName!!,
                Location!!,
                mobileno!!,
                email!!,
                ""
            )
        }

        initViews()

        initObservables()

    }


    private fun initViews() {

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        title = "Online Registration"
        toolbar?.setTitleTextColor(Color.parseColor("#ffffff"))

        toolbar?.setNavigationOnClickListener { onBackPressed() }

      /*  SubjectSelect?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position == 0) {
                        subjectId = "0"
                    } else {

                        getSubjectId(parent?.getItemAtPosition(position).toString())

                    }
                }
            }*/
    }

    private fun initObservables() {

   /*     dataViewModel.subjectList.observe(this, androidx.lifecycle.Observer {
            subjectDataList = it

            setSubjectSpinner(it)

        })*/


        dataViewModel.apiSuccess.observe(this, androidx.lifecycle.Observer {

            if (!it.isNullOrEmpty()) {
                Toasty.success(this, it, Toasty.LENGTH_LONG).show()
                val i = Intent(this, PaymentWithSubWise::class.java)
                startActivity(i)
                finish()
            }

        })

        dataViewModel.apiError.observe(this, androidx.lifecycle.Observer {

            if (!it.isNullOrEmpty()) {
                Toasty.error(this, it, Toasty.LENGTH_LONG).show()
            }
        })

        dataViewModel.student_no.observe(this, androidx.lifecycle.Observer {

            if (!it.isNullOrEmpty()) {
                student_id=it
                //Toasty.error(this, it, Toasty.LENGTH_LONG).show()

            }
        })


        dataViewModel.isStudentName.observe(this, androidx.lifecycle.Observer {
            if (it) {
                studentName?.error = "Student name can't left empty"
                dataViewModel.isStudentName.value = false
                requestFocus(studentName!!, this)
            }
        })

        dataViewModel.isFatherName.observe(this, androidx.lifecycle.Observer {
            if (it) {
                studentFatherName?.error = "Father name can't left empty"
                dataViewModel.isFatherName.value = false
                requestFocus(studentFatherName!!, this)
            }
        })

        dataViewModel.isMotherName.observe(this, androidx.lifecycle.Observer {
            if (it) {
                studentMotherName?.error = "Mother name can't left empty"
                dataViewModel.isMotherName.value = false
                requestFocus(studentMotherName!!, this)
            }
        })

        dataViewModel.isMobile.observe(this, androidx.lifecycle.Observer {
            if (it) {
                mobileNo?.error = "Mobile name can't left empty"
                dataViewModel.isMobile.value = false
                requestFocus(mobileNo!!, this)
            }
        })


        dataViewModel.isAddress.observe(this, androidx.lifecycle.Observer {
            if (it) {
                location?.error = "Address name can't left empty"
                dataViewModel.isAddress.value = false
                requestFocus(location!!, this)
            }
        })

        dataViewModel.isEmail.observe(this, androidx.lifecycle.Observer {
            if (it) {
                Email?.error = "Address name can't left empty"
                dataViewModel.isEmail.value = false
                requestFocus(Email!!, this)
            }
        })

       /* dataViewModel.isSubjectSelect.observe(this, androidx.lifecycle.Observer {
            if (it) {
                showErrorToast("Subject Select can't empty!")
                dataViewModel.isSubjectSelect.value = false

            }
        })*/


    }


/*    private fun setSubjectSpinner(subjectDataList: List<SubjectData.Subject>) {
        val subjectTempList = ArrayList<String>()
        subjectTempList.add("Select Subject")

        subjectDataList.forEach { subject ->
            subjectTempList.add(subject.getTitle().toString())
        }

        val sectionAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            subjectTempList
        )
        sectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        SubjectSelect?.adapter = sectionAdapter
    }

    private fun getSubjectId(subName: String) {
        subjectDataList.forEach { subject ->
            if (subject.getTitle().equals(subName, true)) {
                subjectId = subject.getId().toString()
            }
        }
    }*/

    private fun showErrorToast(message: String) {
        Toasty.error(this, message, Toasty.LENGTH_LONG).show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val i = Intent(this, Login::class.java)
        startActivity(i)
        finish()
    }

    companion object{
        var student_id: String="";
    }
}