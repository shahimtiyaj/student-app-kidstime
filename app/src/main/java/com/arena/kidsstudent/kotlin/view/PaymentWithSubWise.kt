package com.arena.kidsstudent.kotlin.view

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arena.kidsstudent.Parent
import com.arena.kidsstudent.R
import com.arena.kidsstudent.kotlin.adapter.PaymentSubWiseAdapter
import com.arena.kidsstudent.kotlin.model.PaymentSubject
import com.arena.kidsstudent.kotlin.model.PaymentSubjectPost
import com.arena.kidsstudent.kotlin.sslgateway.SSLPayment
import com.arena.kidsstudent.kotlin.viewmodel.DataViewModel
import com.arena.kidsstudent.kotlin.viewmodel.DataViewModelFactory
import com.example.awesomedialog.*
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCAdditionalInitializer
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCommerzInitialization
import es.dmoral.toasty.Toasty

class PaymentWithSubWise : AppCompatActivity(), PaymentSubWiseAdapter.OnSaveButtonClick{

    private lateinit var dataViewModel: DataViewModel

    private var paymentSubjectList = ArrayList<PaymentSubject.Subject>()

    private var recyclerView: RecyclerView? = null
    private lateinit var layoutManager: RecyclerView.LayoutManager
    var parent: Parent? = null
    var toolbar: Toolbar? = null
    var studentId: TextView? = null

    private var sslCommerzInitialization: SSLCommerzInitialization? = null
    var additionalInitializer: SSLCAdditionalInitializer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_list)
        dataViewModel = ViewModelProvider(
            this,
            DataViewModelFactory(this.application)
        )[DataViewModel::class.java]

        recyclerView = findViewById<View>(R.id.paymentSubjectList) as RecyclerView

        parent = Parent(this)

        dataViewModel.getPaymentSubjectList()

        initViews()

        initObservables()
    }

    private fun initViews() {
        studentId = findViewById(R.id.txtStudentId)
        val password: TextView = findViewById(R.id.txtPassword)

        studentId?.text = "Username: "+OnlineRegistration.student_id
        password.text = "Password: "+ "123456"

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        title = "Payment"
        toolbar?.setTitleTextColor(Color.parseColor("#ffffff"))

        toolbar?.setNavigationOnClickListener {
            val i = Intent(this, OnlineRegistration::class.java)
            startActivity(i)
            finish()
        }
    }


    private fun initObservables() {

        layoutManager = LinearLayoutManager(this)

        dataViewModel.apiSuccess.observe(this, androidx.lifecycle.Observer {

            if (!it.isNullOrEmpty()){
                dataViewModel.paymentAmount.observe(this, androidx.lifecycle.Observer { paymentAmount->

                    val i = Intent(this, SSLPayment::class.java)
                    i.putExtra("paymentAmount",paymentAmount)

                    startActivity(i)
                    finish()
                })

            }

        })

        dataViewModel.apiError.observe(this, androidx.lifecycle.Observer {

            if (!it.isNullOrEmpty()){
                Toasty.error(this!!, it, Toasty.LENGTH_LONG).show()
            }

        })


        dataViewModel.paymentId.observe(this, androidx.lifecycle.Observer {
            paymentId=it
        })


        dataViewModel.PaymentSubjectList.observe(this, androidx.lifecycle.Observer {
            this.paymentSubjectList = it as ArrayList<PaymentSubject.Subject>
            setAdapter(it as List<PaymentSubject.Subject>)
        })
    }

    private fun setAdapter(subList: List<PaymentSubject.Subject>) {
        recyclerView?.layoutManager = LinearLayoutManager(this)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.adapter = PaymentSubWiseAdapter(this, subList, this)
    }

    override fun onSave(markInputRqHelperList:ArrayList<PaymentSubjectPost.Subject>) {

           dataViewModel.postPaymentSubData(paymentSubjectList)
    }

    companion object{
        var paymentId: String?=""
    }

}