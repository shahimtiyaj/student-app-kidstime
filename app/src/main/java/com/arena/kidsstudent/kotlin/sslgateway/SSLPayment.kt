package com.arena.kidsstudent.kotlin.sslgateway


import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import com.arena.kidsstudent.Login
import com.arena.kidsstudent.Parent
import com.arena.kidsstudent.R
import com.arena.kidsstudent.kotlin.view.MessageList
import com.arena.kidsstudent.kotlin.view.PaymentWithSubWise
import com.arena.kidsstudent.kotlin.viewmodel.DataViewModel
import com.arena.kidsstudent.kotlin.viewmodel.DataViewModelFactory
import com.example.awesomedialog.*
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCAdditionalInitializer
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCommerzInitialization
import com.sslwireless.sslcommerzlibrary.model.response.SSLCTransactionInfoModel
import com.sslwireless.sslcommerzlibrary.model.util.SSLCCurrencyType
import com.sslwireless.sslcommerzlibrary.model.util.SSLCSdkType
import com.sslwireless.sslcommerzlibrary.view.singleton.IntegrateSSLCommerz
import com.sslwireless.sslcommerzlibrary.viewmodel.listener.SSLCTransactionResponseListener
import es.dmoral.toasty.Toasty

class SSLPayment : AppCompatActivity() , SSLCTransactionResponseListener {

    lateinit var eTAmount: EditText
    lateinit var payButton: Button
    var toolbar: Toolbar? = null

    private var sslCommerzInitialization: SSLCommerzInitialization? = null
    var additionalInitializer: SSLCAdditionalInitializer? = null
    var parent: Parent? = null

    private lateinit var dataViewModel: DataViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_ssl)

        parent = Parent(this)

        dataViewModel = ViewModelProvider(this, DataViewModelFactory(this.application))[DataViewModel::class.java]


        supportActionBar?.hide()
        eTAmount=findViewById(R.id.etAmountId)
        payButton=findViewById(R.id.payNowButtonId)

        payButton.setOnClickListener {
            val amount=eTAmount.text.toString().trim()
            if (amount.isNotEmpty()){
                sslSetUp(amount)
            }else{
                eTAmount.error="Error"
            }
        }

        initViews()
        initObservables()
    }

    private fun initViews() {

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        title = "Payment"
        toolbar?.setTitleTextColor(Color.parseColor("#ffffff"))

        toolbar?.setNavigationOnClickListener {
            val i = Intent(this, PaymentWithSubWise::class.java)
            startActivity(i)
            finish()
        }

    }

    private fun initObservables() {
        val intent = intent
        val paymentAmount = intent.getStringExtra("paymentAmount")
        eTAmount.setText(paymentAmount)
        eTAmount.isFocusable = false

        dataViewModel.apiSuccess.observe(this, androidx.lifecycle.Observer {

            if (!it.isNullOrEmpty()){
                Toasty.success(this!!, it, Toasty.LENGTH_LONG).show()
                val i = Intent(this, Login::class.java)
                startActivity(i)
                finish()
            }

        })

        dataViewModel.apiError.observe(this, androidx.lifecycle.Observer {

            if (!it.isNullOrEmpty()){
                Toasty.error(this!!, it, Toasty.LENGTH_LONG).show()
            }

        })
    }



    private fun sslSetUp(amount:String) {

        val currentTimestamp = System.currentTimeMillis()


  /*      //ssl_commarz
        sslCommerzInitialization = SSLCommerzInitialization(
            "xyz60cedb0687de1",
            "xyz60cedb0687de1@ssl",
            amount.toDouble(), SSLCCurrencyType.BDT,
            "MYID$currentTimestamp",
            "education",
            SSLCSdkType.TESTBOX
        )*/


        Log.d("Amount:" , amount)
        //ssl_commarz
        sslCommerzInitialization = SSLCommerzInitialization(
            "kidstimebdlive",
            "5E89ACF79D3AD88255",
             amount.toDouble(), SSLCCurrencyType.BDT,
            "MYID$currentTimestamp",
            "education",
            SSLCSdkType.LIVE
        )

        additionalInitializer = SSLCAdditionalInitializer()
        additionalInitializer!!.valueA = "Value Option 1"
        additionalInitializer!!.valueB = "Value Option 1"
        additionalInitializer!!.valueC = "Value Option 1"
        additionalInitializer!!.valueD = "Value Option 1"


        IntegrateSSLCommerz
            .getInstance(this)
            .addSSLCommerzInitialization(sslCommerzInitialization)
            .addAdditionalInitializer(additionalInitializer)
            .buildApiCall(this)

    }

    override fun transactionFail(p0: String?) {

        AwesomeDialog
            .build(this)
            .position(AwesomeDialog.POSITIONS.CENTER)
            .title("$p0")
            .icon(R.mipmap.ic_launcher)
            .onPositive("Continue") {

            }
    }

    override fun merchantValidationError(p0: String?) {
        AwesomeDialog
            .build(this)
            .position(AwesomeDialog.POSITIONS.CENTER)
            .title("$p0")
            .icon(R.mipmap.ic_launcher)
            .onPositive("Continue") {

            }

    }

    override fun transactionSuccess(successInfo: SSLCTransactionInfoModel?) {

        if (successInfo != null) {

            Log.d("BODYDATA", successInfo.tranId)
            Log.d("BODYDATA", successInfo.status)

            AwesomeDialog
                .build(this)
                .position(AwesomeDialog.POSITIONS.CENTER)
                .title("Id:${successInfo.tranId} \nAmount: ${successInfo.amount} \nPayment Status:${successInfo.apiConnect}")
                .icon(R.mipmap.ic_launcher)
                .onPositive("Continue") {

                    dataViewModel.postTransactionInfo(successInfo.tranId, successInfo.amount,
                        PaymentWithSubWise.paymentId!!, successInfo.cardType, successInfo.status)

                }

        }


    }
}