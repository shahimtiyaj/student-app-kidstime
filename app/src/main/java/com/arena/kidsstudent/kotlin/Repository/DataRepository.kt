package com.arena.kidsstudent.kotlin.Repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.arena.kidsstudent.kotlin.ApiClient
import com.arena.kidsstudent.kotlin.ApiInterface
import com.arena.kidsstudent.kotlin.model.*
import com.google.gson.Gson
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataRepository(val application: Application) {

    var subjectList = MutableLiveData<List<SubjectData.Subject>>()
    var batchList = MutableLiveData<List<BatchData.Batch>>()
    var teacherList = MutableLiveData<List<TeacherData.Teacher>>()
    var classUrlList = MutableLiveData<List<*>>()
    var messageList = MutableLiveData<List<*>>()
    var paymentSubjectList = MutableLiveData<List<*>>()

    var apiSuccess = MutableLiveData<String>()
    var apiError = MutableLiveData<String>()
    var student_no = MutableLiveData<String>()
    var paymentAmount = MutableLiveData<String>()
    var paymentId = MutableLiveData<String>()

    fun getSubjectData(subJson: JsonObject) {
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getSubjectListData(subJson)

        Log.e("TestMyJson", subJson.toString())
        val emptyList = ArrayList<String>()
        //calling the api ---------------------------------------------
        call?.enqueue(object : Callback<SubjectData> {
            override fun onResponse(call: Call<SubjectData>, response: Response<SubjectData>) {
                if (response.isSuccessful) {

                    try {
                        if (response.isSuccessful) {
                            subjectList.value =
                                response.body()?.getSubjectList() as List<SubjectData.Subject>?
                        }


                        Log.e("all_json_data", response.body().toString())

                    } catch (e: Exception) {

                        e.printStackTrace()
                    }

                } else {
                    Log.d("onResponse", "Subject data fail:" + response.body())
                }
            }

            override fun onFailure(call: Call<SubjectData>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }

    fun getBatchData(subId: JsonObject) {
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getBatchData(subId)

        Log.e("TestMyJson", subId.toString())
        val emptyList = ArrayList<String>()
        //calling the api ---------------------------------------------
        call?.enqueue(object : Callback<BatchData> {
            override fun onResponse(call: Call<BatchData>, response: Response<BatchData>) {
                if (response.isSuccessful) {

                    try {
                        if (response.isSuccessful) {
                            batchList.value = response.body()?.getBatch() as List<BatchData.Batch>?
                        }

                        Log.e("all_json_data", response.body().toString())

                    } catch (e: Exception) {

                        e.printStackTrace()
                    }

                } else {
                    Log.d("onResponse", "Teacher data fail:" + response.body())
                }
            }

            override fun onFailure(call: Call<BatchData>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }

    fun getTeacherData(subJson: JsonObject) {
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getTeacherListData(subJson)

        Log.e("TestMyJson", subJson.toString())
        val emptyList = ArrayList<String>()
        //calling the api ---------------------------------------------
        call?.enqueue(object : Callback<TeacherData> {
            override fun onResponse(call: Call<TeacherData>, response: Response<TeacherData>) {
                if (response.isSuccessful) {

                    try {
                        if (response.isSuccessful) {
                            teacherList.value =
                                response.body()?.getTeacherlist() as List<TeacherData.Teacher>?
                        }

                        Log.e("all_json_data", response.body().toString())

                    } catch (e: Exception) {

                        e.printStackTrace()
                    }

                } else {
                    Log.d("onResponse", "Teacher data fail:" + response.body())
                }
            }

            override fun onFailure(call: Call<TeacherData>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }

    fun postStudentMessage(postData: JsonObject) {
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.postMessageData(postData)

        Log.e("TestMyJson", postData.toString())
        val emptyList = ArrayList<String>()
        //calling the api ---------------------------------------------
        call?.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.isSuccessful) {

                    apiSuccess.value = "Message sent successfully!"

                } else {
                    apiError.value = "Message sent to fail!!"

                    Log.d("onResponse", "Fail data :" + response.body())
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }


    fun getOnlineMessageData(postData: JsonObject) {
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getMessageData(postData)

        Log.e("TestMyJson", postData.toString())
        val emptyList = ArrayList<String>()
        //calling the api ---------------------------------------------
        call?.enqueue(object : Callback<List<MessageData>> {
            override fun onResponse(
                call: Call<List<MessageData>>,
                response: Response<List<MessageData>>
            ) {
                if (response.isSuccessful) {

                    try {
                        if (response.isSuccessful) {
                            messageList.value = response.body()
                        }

                        Log.e("all_json_data", response.body().toString())

                    } catch (e: Exception) {

                        e.printStackTrace()
                    }

                } else {
                    Log.d("onResponse", "Teacher data fail:" + response.body())
                }
            }

            override fun onFailure(call: Call<List<MessageData>>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }

    fun getOnlineClassUrl(postData: JsonObject) {
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getUrlListData(postData)

        Log.e("TestMyJson", postData.toString())
        val emptyList = ArrayList<String>()
        //calling the api ---------------------------------------------
        call?.enqueue(object : Callback<List<OnlineClassUrl>> {
            override fun onResponse(
                call: Call<List<OnlineClassUrl>>,
                response: Response<List<OnlineClassUrl>>
            ) {
                if (response.isSuccessful) {

                    try {
                        if (response.isSuccessful) {
                            classUrlList.value = response.body()
                        }

                        Log.e("all_json_data", response.body().toString())

                    } catch (e: Exception) {

                        e.printStackTrace()
                    }

                } else {
                    Log.d("onResponse", "Teacher data fail:" + response.body())
                }
            }

            override fun onFailure(call: Call<List<OnlineClassUrl>>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }


    fun postSignUpMessage(postData: JsonObject) {
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.postRegistrationData(postData)

        Log.e("TestMyJson", postData.toString())
        val emptyList = ArrayList<String>()
        //calling the api ---------------------------------------------
        call?.enqueue(object : Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                if (response.isSuccessful) {

                    val jsonObject = JSONObject(Gson().toJson(response.body()))
                    student_no.value = jsonObject.getString("student_no")
                    Log.d("onResponse", jsonObject.getString("student_no"))

                    apiSuccess.value = "Registration successfully completed!"


                } else {
                    apiError.value = "Registration to fail!!"

                    Log.d("onResponse", "Fail data :" + response.body())
                }
            }

            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }


    fun getPaymentSubject() {
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getPaymentListData()

        val emptyList = ArrayList<String>()
        //calling the api ---------------------------------------------
        call?.enqueue(object : Callback<PaymentSubject> {
            override fun onResponse(
                call: Call<PaymentSubject>,
                response: Response<PaymentSubject>
            ) {
                if (response.isSuccessful) {

                    try {
                        if (response.isSuccessful) {
                            paymentSubjectList.value =
                                response.body()?.getSubjectList() as List<PaymentSubject.Subject>?
                        }

                        Log.e("all_payment_sub_data", response.body().toString())

                    } catch (e: Exception) {

                        e.printStackTrace()
                    }

                } else {
                    Log.d("onResponse", "payment subject data fail:" + response.body())
                }
            }

            override fun onFailure(call: Call<PaymentSubject>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }


    fun postPaymentSubData(paymentSubjectPost: JsonObject) {

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)

        val call = apiService.postPaymentSubData(paymentSubjectPost)

        call.enqueue(object : Callback<PaymentAmount> {
            override fun onFailure(call: Call<PaymentAmount>, t: Throwable) {
                apiError.value = "Couldn't get  Student Ids list to Save!"
            }

            override fun onResponse(
                call: Call<PaymentAmount>,
                response: Response<PaymentAmount>
            ) {
                try {
                    if (response.isSuccessful) {
                        apiSuccess.value = "Student Id Successfully Saved !"
                        paymentAmount.postValue(response.body()?.getAmount()?.toString())
                        paymentId.postValue(response.body()?.getPaymentId()?.toString())

                    } else {
                        apiError.value = "Couldn't Save  Data! Please try again."
                    }
                } catch (e: Exception) {
                    apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun postTransactionInfo(paymentSubjectPost: JsonObject) {

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)

        val call = apiService.postPaymentAmount(paymentSubjectPost)

        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                apiError.value = "Couldn't get  Student Ids list to Save!"
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                try {
                    if (response.isSuccessful) {

                        apiSuccess.value = "Payment successfully completed!"

                    } else {
                        apiError.value = "Couldn't paid! Please try again."
                    }
                } catch (e: Exception) {
                    apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }


}