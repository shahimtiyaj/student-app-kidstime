package com.arena.kidsstudent.kotlin.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Jsonbody {
    @SerializedName("user_id")
    @Expose
    private var userId: String? = null

    fun getUserId(): String? {
        return userId
    }

    fun setUserId(userId: String?) {
        this.userId = userId
    }
}