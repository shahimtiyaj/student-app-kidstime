package com.arena.kidsstudent.kotlin


import com.arena.kidsstudent.kotlin.model.*
import com.google.gson.JsonObject
import com.google.gson.JsonArray
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface ApiInterface {

    @Headers("Content-Type: application/json")
    @POST("get_product_combination_v2.json")
    fun getProductCombinationV2Data(@Body jsonObject: JsonObject): Call<String>

    @Headers("Content-Type: application/json")
    @POST("get_special_group.json")
    fun getSpecialGroupData(@Body jsonObject: JsonObject): Call<String>

    @Headers("Content-Type: application/json")
    @POST("get_product_price_v2.json")
    fun getPriceV2Data(@Body jsonObject: JsonObject): Call<String>

    @Headers("Content-Type: application/json")
    @POST("get_combination_list.json")
    fun getProductCombinationListData(@Body jsonObject: JsonObject?): Call<String?>

    //-----------------------------------------------------------------------------

    @Headers("Content-Type: application/json")
    @POST("get_subject_list")
    fun getSubjectListData(@Body jsonObject: JsonObject): Call<SubjectData>


    @Headers("Content-Type: application/json")
    @POST("get_subject_wise_base")
    fun getBatchData(@Body jsonObject: JsonObject): Call<BatchData>


    @Headers("Content-Type: application/json")
    @POST("get_subject_batch_wise_teacher")
    fun getTeacherListData(@Body jsonObject: JsonObject): Call<TeacherData>

    @Headers("Content-Type: application/json")
    @POST("get_online_class_url_list")
    fun getUrlListData(@Body jsonObject: JsonObject): Call<List<OnlineClassUrl>>

    @Headers("Content-Type: application/json")
    @POST("get_teacher_message_list")
    fun getMessageData(@Body jsonObject: JsonObject): Call<List<MessageData>>


    @Headers("Content-Type: application/json")
    @POST("send_teacher_message")
    fun postMessageData(@Body jsonObject: JsonObject): Call<JsonObject>


    @Headers("Content-Type: application/json")
    @POST("send_signup")
    fun postRegistrationData(@Body jsonObject: JsonObject): Call<JsonObject>


    @Headers("Content-Type: application/json")
    @POST("get_subject_admission_fee")
    fun getPaymentListData(): Call<PaymentSubject>

    @Headers("Content-Type: application/json")
    @POST("admission_submit")
    fun postPaymentSubData(@Body paymentSubjectPost: JsonObject): Call<PaymentAmount>

    @Headers("Content-Type: application/json")
    @POST("admission_payment_confirmation")
    fun postPaymentAmount(@Body paymentSubjectPost: JsonObject): Call<JsonObject>

}


