package com.arena.kidsstudent.kotlin.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PaymentSubject {
    @SerializedName("subjectList")
    @Expose
    private var subjectList: List<Subject?>? = null

    fun getSubjectList(): List<Subject?>? {
        return subjectList
    }

    fun setSubjectList(subjectList: List<Subject?>?) {
        this.subjectList = subjectList
    }



    class Subject {
        @SerializedName("id")
        @Expose
        private var id: String? = null

        @SerializedName("title")
        @Expose
        private var title: String? = null

        @SerializedName("ammount")
        @Expose
        private var ammount: String? = null

        fun getId(): String? {
            return id
        }

        fun setId(id: String?) {
            this.id = id
        }

        fun getTitle(): String? {
            return title
        }

        fun setTitle(title: String?) {
            this.title = title
        }

        fun getAmmount(): String? {
            return ammount
        }

        fun setAmmount(ammount: String?) {
            this.ammount = ammount
        }

        @SerializedName("")
        @Expose
        private var isSelected: Boolean = false

        fun getSelected(): Boolean {
            return isSelected
        }

        fun setSelected(selected: Boolean) {
            isSelected = selected
        }


    }
}