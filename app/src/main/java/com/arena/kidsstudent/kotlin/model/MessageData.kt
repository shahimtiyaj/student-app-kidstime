package com.arena.kidsstudent.kotlin.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class MessageData {

    @SerializedName("id")
    @Expose
    private var id: String? = null

    @SerializedName("student_id")
    @Expose
    private var studentId: String? = null

    @SerializedName("class_id")
    @Expose
    private var classId: String? = null

    @SerializedName("section_id")
    @Expose
    private var sectionId: String? = null

    @SerializedName("teacher_id")
    @Expose
    private var teacherId: String? = null

    @SerializedName("st_message")
    @Expose
    private var stMessage: String? = null

    @SerializedName("t_message")
    @Expose
    private var tMessage: String? = null

    @SerializedName("create_at")
    @Expose
    private var createAt: String? = null

    @SerializedName("name")
    @Expose
    private var name: String? = null

    @SerializedName("mobile_no")
    @Expose
    private var mobileNo: String? = null

    @SerializedName("cname")
    @Expose
    private var cname: String? = null

    @SerializedName("secname")
    @Expose
    private var secname: String? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String?) {
        this.id = id
    }

    fun getStudentId(): String? {
        return studentId
    }

    fun setStudentId(studentId: String?) {
        this.studentId = studentId
    }

    fun getClassId(): String? {
        return classId
    }

    fun setClassId(classId: String?) {
        this.classId = classId
    }

    fun getSectionId(): String? {
        return sectionId
    }

    fun setSectionId(sectionId: String?) {
        this.sectionId = sectionId
    }

    fun getTeacherId(): String? {
        return teacherId
    }

    fun setTeacherId(teacherId: String?) {
        this.teacherId = teacherId
    }

    fun getStMessage(): String? {
        return stMessage
    }

    fun setStMessage(stMessage: String?) {
        this.stMessage = stMessage
    }

    fun gettMessage(): String? {
        return tMessage
    }

    fun settMessage(tMessage: String?) {
        this.tMessage = tMessage
    }

    fun getCreateAt(): String? {
        return createAt
    }

    fun setCreateAt(createAt: String?) {
        this.createAt = createAt
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun getMobileNo(): String? {
        return mobileNo
    }

    fun setMobileNo(mobileNo: String?) {
        this.mobileNo = mobileNo
    }

    fun getCname(): String? {
        return cname
    }

    fun setCname(cname: String?) {
        this.cname = cname
    }

    fun getSecname(): String? {
        return secname
    }

    fun setSecname(secname: String?) {
        this.secname = secname
    }
}