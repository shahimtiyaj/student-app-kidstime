package com.arena.kidsstudent.kotlin.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class BatchData {
    @SerializedName("batch")
    @Expose
    private var batch: List<Batch>? = null

    fun getBatch(): List<Batch?>? {
        return batch
    }

    fun setBatch(batch: List<Batch>?) {
        this.batch = batch
    }

    class Batch {

        @SerializedName("id")
        @Expose
        private var id: String? = null

        @SerializedName("version_id")
        @Expose
        private var versionId: String? = null

        @SerializedName("class_id")
        @Expose
        private var classId: String? = null

        @SerializedName("title")
        @Expose
        private var title: String? = null

        @SerializedName("description")
        @Expose
        private var description: String? = null

        @SerializedName("status")
        @Expose
        private var status: String? = null

        @SerializedName("room_number")
        @Expose
        private var roomNumber: String? = null

        @SerializedName("created_at")
        @Expose
        private var createdAt: String? = null

        @SerializedName("created_by")
        @Expose
        private var createdBy: String? = null

        @SerializedName("updated_at")
        @Expose
        private var updatedAt: String? = null

        fun getId(): String? {
            return id
        }

        fun setId(id: String?) {
            this.id = id
        }

        fun getVersionId(): String? {
            return versionId
        }

        fun setVersionId(versionId: String?) {
            this.versionId = versionId
        }

        fun getClassId(): String? {
            return classId
        }

        fun setClassId(classId: String?) {
            this.classId = classId
        }

        fun getTitle(): String? {
            return title
        }

        fun setTitle(title: String?) {
            this.title = title
        }

        fun getDescription(): String? {
            return description
        }

        fun setDescription(description: String?) {
            this.description = description
        }

        fun getStatus(): String? {
            return status
        }

        fun setStatus(status: String?) {
            this.status = status
        }

        fun getRoomNumber(): String? {
            return roomNumber
        }

        fun setRoomNumber(roomNumber: String?) {
            this.roomNumber = roomNumber
        }

        fun getCreatedAt(): String? {
            return createdAt
        }

        fun setCreatedAt(createdAt: String?) {
            this.createdAt = createdAt
        }

        fun getCreatedBy(): String? {
            return createdBy
        }

        fun setCreatedBy(createdBy: String?) {
            this.createdBy = createdBy
        }

        fun getUpdatedAt(): String? {
            return updatedAt
        }

        fun setUpdatedAt(updatedAt: String?) {
            this.updatedAt = updatedAt
        }

    }
}