package com.arena.kidsstudent;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.arena.kidsstudent.ADapter.PaymentDeatislCa;
import com.arena.kidsstudent.utils.URLs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class PaymentDetailsActivity extends Fonts {
    private static final int SMS_REQUEST_CODE =110 ;
    private static final int PHONE_REQUEST_CODE =120 ;
    ListView lv;
    PaymentDeatislCa adapter;
    RequestQueue requestQueue;
    JSONObject jsonObject;
    String Payment_id,status,month,name,lastname,mobile;
    JSONArray  jsonArray;
    ArrayList<HashMap<String,String>> List;
    HashMap<String,String> map;
    Button paysms;
    Parent p;
    Toolbar toolbar;
    LinearLayout paylay;
    String fine;
    String tamount;
    AlertDialog dialog;
    EditText pin;
    int check=0;
    Float absentfine=0f;
    TextView ptitle,pamout;
    TelephonyManager manager;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);
        toolbar = findViewById(R.id.toolbar);
        paylay = findViewById(R.id.paylay);
        ptitle = findViewById(R.id.ptitle);
        pamout = findViewById(R.id.pamount);
        ptitle.setTypeface(getSemiBold());
        pamout.setTypeface(getSemiBold());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Details");
        SimUtil util = new SimUtil();
        manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {

        }
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        List = new ArrayList<HashMap<String, String>>();
        paysms = findViewById(R.id.paysms);
        lv = (ListView)findViewById(R.id.listpay);
        lv.setBackgroundResource(R.drawable.coustomshape);
        Intent i = getIntent();
        Payment_id = i.getStringExtra("paymentId");
        status = i.getStringExtra("status");
        month = i.getStringExtra("monthn");
        status = i.getStringExtra("status");
        fine = i.getStringExtra("fine");
        try{
            tamount = i.getStringExtra("tamount");
        }catch (Exception e){

        }

        Log.e("month",Payment_id+" Payment_id"+status+" status"+month+" month"+fine+" fine");
        final EditText input = new EditText(PaymentDetailsActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(150,
                LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(30, 20, 30, 0);
        input.setLayoutParams(lp);
        input.setHint("****");
        input.setMaxEms(4);
        input.setMaxLines(4);
        input.setShowSoftInputOnFocus(true);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        requestQueue = Volley.newRequestQueue(PaymentDetailsActivity.this);
        Log.e("payment",status);
        p = new Parent(PaymentDetailsActivity.this);
        name = p.getPreference("name");
        Log.e("lastname",name);
        lastname = name.substring(name.lastIndexOf(" ")+1);
        Log.e("lastnamte",lastname);
        mobile = p.getPreference("mobile");



        jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.USER_ID,p.getPreference(Constants.USER_ID));
            jsonObject.put("payment_id",Payment_id);
            if (String.valueOf(Integer.parseInt(month)-1).length()==1){

                String value = "0"+String.valueOf(Integer.parseInt(month)-1);
                jsonObject.put("month_id",value);
            }else {
                jsonObject.put("month_id",Integer.parseInt(month)-1);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        if(status.equalsIgnoreCase("Unpaid")){


            paysms.setVisibility(View.VISIBLE);

        }
    /*    LinearLayout.LayoutParams param1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f
        );
        param1.setMargins(20,20,20,150);
        paylay.setLayoutParams(param1);*/
        if(status.equalsIgnoreCase("paid")){
            paysms.setVisibility(View.GONE);

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    1.0f
            );
            param.setMargins(20,20,20,0);
            paylay.setLayoutParams(param);

        }


        paysms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if (check_perrmission(Manifest.permission.READ_PHONE_STATE)){

                    checksms();

                }else {
                    ActivityCompat.requestPermissions(PaymentDetailsActivity
                            .this,new String[]
                            {Manifest.permission.READ_PHONE_STATE},PHONE_REQUEST_CODE);
                }


            }







        });



        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.POST, URLs
                .PAYEMENT_DETAILS, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {



                try {
                    jsonArray = response.getJSONArray("head_wise_amount");
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                Log.e("responcee",response.toString());

                for (int i = 0 ; i <jsonArray.length();i++){

                    map = new HashMap<String, String>();


                    try {

                        jsonArray.getJSONObject(i)
                                .getString( "head_title");
                        Log.e("AbsentFine","....."+absentfine+""+jsonArray.getJSONObject(i)
                                .getString( "head_title"));

                        if (!status.equalsIgnoreCase("paid")&&jsonArray.getJSONObject(i)
                                .getString( "head_title").equalsIgnoreCase("Monthly Late Fee")){

                            map.put("head_title", jsonArray.getJSONObject(i).getString("head_title"));
                            Log.e("head_title","....."+ jsonArray.getJSONObject(i).getString("head_title"));
                            map.put("amount", fine+"");
                        }/*else if (jsonArray.getJSONObject(i)
                                .getString( "head_title").trim().equalsIgnoreCase("Absent Fine")){

                          *//*  absentfine = Float.parseFloat(response.getString
                                    ("absent_count_day"))*20f;*//*

                            map.put("head_title", jsonArray.getJSONObject(i).getString("head_title"));
                            Log.e("head_title_ABSENT", "....."+jsonArray.getJSONObject(i)
                                    .getString("head_title"));
                            map.put("amount", "(20x+"+Float.parseFloat(response.getString
                                    ("absent_count_day"))+")="+absentfine);
                        }*/

                        else {

                            map.put("head_title", jsonArray.getJSONObject(i).getString("head_title"));
                            Log.e("head_title","....."+ jsonArray.getJSONObject(i).getString
                                    ("head_title"));
                            map.put("amount", jsonArray.getJSONObject(i).getString("amount"));
                        }

                           /* Toast.makeText(PaymentDetailsActivity.this, "Massage Sent " +
                                            "Successfully" ,
                                    Toast.LENGTH_LONG).show();*/
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    List.add(map);


                }
                if (!status.equalsIgnoreCase("paid")){
                    try {
                        map = new HashMap<String, String>();
                        map.put("head_title", response.getString( "head_title"));
                        map.put("amount",Float.parseFloat(tamount)+absentfine+"");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    try {
                        map = new HashMap<String, String>();
                        map.put("head_title", response.getString( "head_title"));
                        map.put("amount",response.getString( "amount"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                List.add(map);
                Log.e("list",List.toString());
                adapter = new PaymentDeatislCa(PaymentDetailsActivity.this, List);
                lv.setAdapter(adapter);









            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("payment",error.toString());

            }
        });
        requestQueue.add(jsonArrayRequest);
        requestQueue.getCache().clear();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.Logout:
                Intent i = new Intent(PaymentDetailsActivity.this,Login.class);
                startActivity(i);
                finish();
                return true;
            case R.id.Changepass:
                Intent j = new Intent(PaymentDetailsActivity  .this,ChangePass.class);
                startActivity(j);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sendSms() {
        if (check_perrmission(Manifest.permission.SEND_SMS)){

            SmsManager smsManager = SmsManager.getDefault();
            if (mobile.length()>11){
                smsManager.sendTextMessage("03590016201",null,"TrustMM RCPSC " +p.getPreference
                        (Constants
                        .USERNAME)+" "+ month+" "+pin.getText().toString().trim()+" " + lastname+" " +
                        ""+mobile,null,null);
                Toast.makeText(PaymentDetailsActivity
                                .this, "Massage Sent " +
                                "Successfully" ,
                        Toast.LENGTH_LONG).show();

            }else if (mobile.length()==11) {
                smsManager.sendTextMessage("03590016201",null,"TrustMM RCPSC " +p
                        .getPreference(Constants
                        .USERNAME)+" "+ month+" "+pin.getText().toString().trim()+" " + lastname+" " +
                        "88"+mobile,null,null);
                Toast.makeText(PaymentDetailsActivity
                                .this, "Massage Sent " +
                                "Successfully" ,
                        Toast.LENGTH_LONG).show();


            }



        }else {
            ActivityCompat.requestPermissions(PaymentDetailsActivity.this,new String[] {Manifest
                    .permission
                    .SEND_SMS},SMS_REQUEST_CODE);
        }

    }

    private boolean check_perrmission(String permission){
        int checkPermission = ContextCompat.checkSelfPermission(this,permission);

        return checkPermission == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("info",grantResults.length+","+grantResults[0]);

        switch (requestCode){

            case SMS_REQUEST_CODE:

                if (grantResults.length>0 && (grantResults[0]==PackageManager.PERMISSION_GRANTED)){
                    sendSms();
                }else {
                    ActivityCompat.requestPermissions(PaymentDetailsActivity.this,new String[] {Manifest
                            .permission
                            .SEND_SMS},SMS_REQUEST_CODE);
                    Toast.makeText(PaymentDetailsActivity.this,"Please Give Sms permission",
                            Toast.LENGTH_LONG).show();
                    dialog.dismiss();

                }
                break;

            case PHONE_REQUEST_CODE:
                if (grantResults.length>0 && (grantResults[0]==PackageManager.PERMISSION_GRANTED)){
                    checksms();

                }else {
                    Toast.makeText(PaymentDetailsActivity.this,"Please Give Sms permission",
                            Toast.LENGTH_LONG).show();


                }

                break;
        }

    }

    public void checksms(){
        View view1 = getLayoutInflater().inflate(R.layout.edittext, null);

        pin = view1.findViewById(R.id.pin);
        final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentDetailsActivity.this);
        builder.setTitle("Enter t-Cash Pin");

        builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {





                        if (pin.getText().toString().length()==4){




                          /*  if (manager.getPhoneCount()>1){*/



                                Uri uri = Uri.parse("smsto:03590016201");
                                Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                               String text =  "TrustMM RCPSC " +p.getPreference(Constants
                                        .USERNAME)+" "+ month+" "+pin.getText().toString().trim()+" " + lastname+" " +
                                        ""+mobile;

                            it.putExtra("sms_body",text);

                           /* ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                            ClipData clip = ClipData.newPlainText("label", text);
                            clipboard.setPrimaryClip(clip);

                            Toast.makeText(PaymentDetailsActivity.this,"text copy paste in message and send it to 03590016201",Toast.LENGTH_LONG).show();*/

                               startActivity(it);
                          /*  }else
                            {
                                sendSms();
                            }*/



                        }else {
                            Toast.makeText(PaymentDetailsActivity.this, "Pin need to " +
                                            "be 4" +
                                            " " +
                                            "digit",
                                    Toast.LENGTH_LONG).show();
                        }











            }
        });
        dialog = builder.create();
        dialog.setView(view1);


        dialog.show();

    }


    /*public void senddoublesms(int sim){

        if (check_perrmission(Manifest.permission.SEND_SMS)){


            if (mobile.length()>11){
                SmsManager.getSmsManagerForSubscriptionId(sim).sendTextMessage("03590016201" , null ,
                        "TrustMM RCPSC " +p.getPreference(Constants
                                .USERNAME)+" "+ month+" "+pin.getText().toString().trim()+" " + lastname+" " +
                                ""+mobile,null, null);


            }else if (mobile.length()==11) {
                SmsManager.getSmsManagerForSubscriptionId(sim).sendTextMessage("03590016201" , null ,
                        "TrustMM RCPSC " +p.getPreference(Constants
                                .USERNAME)+" "+ month+" "+pin.getText().toString().trim()+" " + lastname+" " +
                                "88"+mobile,null, null);


            }

            Toast.makeText(PaymentDetailsActivity
                            .this, "Massage Sent " +
                            "Successfully" ,
                    Toast.LENGTH_LONG).show();



        }else {
            ActivityCompat.requestPermissions(PaymentDetailsActivity.this,new String[] {Manifest
                    .permission
                    .SEND_SMS},SMS_REQUEST_CODE);
        }


    }*/
}
