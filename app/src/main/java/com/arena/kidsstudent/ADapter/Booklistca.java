package com.arena.kidsstudent.ADapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arena.kidsstudent.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Tanvi on 26-Mar-18.
 */

public class Booklistca extends BaseAdapter {


    Context context;
    ArrayList<HashMap<String,String>> arrayList;

    public Booklistca(Context context, ArrayList<HashMap<String,String>> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View cl = inflater.inflate(R.layout.bookrow,null);
        TextView Subject_name = (TextView) cl.findViewById(R.id.NoticeTitle);
        TextView total = (TextView) cl.findViewById(R.id.Description);
        LinearLayout rel = (LinearLayout) cl.findViewById(R.id.rel);
        View view =  cl.findViewById(R.id.view);

        if (i%2==0){

/*
            rel.setBackgroundColor(Color.parseColor("#F0F5F9"));
            view.setBackgroundResource(R.drawable.blueline);*/

        }
        total.setTypeface(Typeface.createFromAsset(context.getAssets(),"Sofia Pro Regular.ttf"));
        Subject_name.setTypeface(Typeface.createFromAsset(context.getAssets(),"Sofia Pro Regular.ttf"));

        /*if (arrayList.size()==i+1){
            total.setTypeface(Typeface.createFromAsset(context.getAssets(),"Sofia Pro SemiBold.ttf"));
            Subject_name.setTypeface(Typeface.createFromAsset(context.getAssets(),"Sofia Pro SemiBold.ttf"));
            total.setTextColor(Color.parseColor("#ffffff"));
            Subject_name.setTextColor(Color.parseColor("#ffffff"));
            total.setBackgroundColor(Color.parseColor("#028FE7"));
            Subject_name.setBackgroundColor(Color.parseColor("#028FE7"));
        }*/


        Subject_name.setText(arrayList.get(i).get("title"));
        total.setText(arrayList.get(i).get("writer_name"));

        Animation animation = AnimationUtils.loadAnimation(context,R.anim.fade_in);

        cl.startAnimation(animation);



        return cl;
    }
}
