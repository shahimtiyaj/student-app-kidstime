package com.arena.kidsstudent.ADapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arena.kidsstudent.Model.PhonebookM;

import com.arena.kidsstudent.R;

import java.util.ArrayList;

public class PhonebookCA extends BaseAdapter{

    Context context;
    ArrayList<PhonebookM> arrayList;

    public PhonebookCA(Context context, ArrayList<PhonebookM> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View cl = inflater.inflate(R.layout.phone_book_row,null);
        TextView name = (TextView) cl.findViewById(R.id.name);
        TextView designation = (TextView) cl.findViewById(R.id.designation);
        TextView number = (TextView) cl.findViewById(R.id.number);
        LinearLayout linearLayout = cl.findViewById(R.id.lin);

        if (i%2==0){


            linearLayout.setBackgroundColor(Color.parseColor("#E2EFD9"));



        }



        name.setText(arrayList.get(i).getName());
        designation.setText(arrayList.get(i).getDesignation());
        number.setText(arrayList.get(i).getNumber());





        Animation animation = AnimationUtils.loadAnimation(context,R.anim.fade_in);

        cl.startAnimation(animation);



        return cl;
    }
}
