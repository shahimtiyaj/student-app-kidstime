package com.arena.kidsstudent.ADapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.arena.kidsstudent.Model.RoutineM;
import com.arena.kidsstudent.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RoutinCA extends BaseAdapter{

    Context context;
    ArrayList<RoutineM> arrayList;

    public RoutinCA(Context context, ArrayList<RoutineM> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup parent) {
    Typeface font = Typeface.createFromAsset(context.getAssets(),"Sofia Pro SemiBold.ttf");
    Typeface font1 = Typeface.createFromAsset(context.getAssets(),"Sofia Pro Regular.ttf");
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cl = inflater.inflate(R.layout.tdaycl,null);
        TextView sub = (TextView) cl.findViewById(R.id.sub);
        TextView teacher = (TextView) cl.findViewById(R.id.teacher);
        TextView time = (TextView) cl.findViewById(R.id.time);
        TextView timet = (TextView) cl.findViewById(R.id.timet);
        CircleImageView image = cl.findViewById(R.id.image);
        View view =  cl.findViewById(R.id.view);
        TextView am = cl.findViewById(R.id.am);
        TextView pm = cl.findViewById(R.id.pm);
        sub.setTypeface(font);
        teacher.setTypeface(font1);
        time.setTypeface(font);
        timet.setTypeface(font);
        am.setTypeface(font);
        pm.setTypeface(font);





        List<String> times = Arrays.asList(arrayList.get(i).getClass_time_id()
                .split("-"));
        sub.setText(arrayList.get(i).getSubject());
        teacher.setText(arrayList.get(i).getTeacher_name());
        time.setText(times.get(0).substring(0,times.get(0).length()-2));
        timet.setText(times.get(1).substring(0,times.get(1).length()-2));
        am.setText(times.get(0).substring(times.get(0).length()-2,times.get(0).length()));
        pm.setText(times.get(1).substring(times.get(1).length()-2,times.get(1).length()));



      if(i%2==1){


        }else {
          image.setImageResource(R.drawable.greenbook);
          view.setBackgroundResource(R.drawable.blueline);

      }



        Animation animation = AnimationUtils.loadAnimation(context,R.anim.fade_in);

        cl.startAnimation(animation);



        return cl;
    }

}
