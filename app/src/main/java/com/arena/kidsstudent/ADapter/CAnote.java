package com.arena.kidsstudent.ADapter;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.arena.kidsstudent.Model.NoteModel;
import com.arena.kidsstudent.R;

import java.util.ArrayList;


/**
 * Created by Tanvi on 26-Mar-18.
 */

public class CAnote extends BaseAdapter {


    Context context;
    ArrayList<NoteModel> arrayList;

    public CAnote(Context context, ArrayList<NoteModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View cl = inflater.inflate(R.layout.note_row,null);
        TextView sirial = (TextView) cl.findViewById(R.id.Sirialn);
        TextView name = (TextView) cl.findViewById(R.id.notename);
        TextView subject = (TextView) cl.findViewById(R.id.subject);
        TextView discription = (TextView) cl.findViewById(R.id.discription);
        Button download = (Button) cl.findViewById(R.id.download);





        name.setText(arrayList.get(i).getNote_name());
        subject.setText(arrayList.get(i).getSubject());
        discription.setText(arrayList.get(i).getDescription());
        sirial.setText(i+1+"");

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DownloadManager mManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                DownloadManager.Request mRqRequest = new DownloadManager.Request(
                        Uri.parse("http://bgb.bgbschoolraj.edu.bd/index.php/book_list/note_download/"+arrayList.get(i).getLink()));
                mRqRequest.setDescription("This is Test File");
//  mRqRequest.setDestinationUri(Uri.parse("give your local path"));
                long idDownLoad = mManager.enqueue(mRqRequest);
            }
        });



        Animation animation = AnimationUtils.loadAnimation(context,R.anim.fade_in);

        cl.startAnimation(animation);



        return cl;
    }
}
