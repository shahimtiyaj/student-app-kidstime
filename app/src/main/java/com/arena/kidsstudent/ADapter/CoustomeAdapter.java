package com.arena.kidsstudent.ADapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.arena.kidsstudent.Model.ResultModel;
import com.arena.kidsstudent.R;

import java.util.ArrayList;


/**
 * Created by Tanvi on 26-Mar-18.
 */

public class CoustomeAdapter extends BaseAdapter {


    Context context;
    ArrayList<ResultModel> arrayList;

    public CoustomeAdapter(Context context, ArrayList<ResultModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        Typeface font = Typeface.createFromAsset(context.getAssets(),"Sofia Pro SemiBold.ttf");
        Typeface font1 = Typeface.createFromAsset(context.getAssets(),"Sofia Pro Regular.ttf");
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View cl = inflater.inflate(R.layout.clforresult,null);
        TextView Subject_name = (TextView) cl.findViewById(R.id.Subject_name);
        TextView total = (TextView) cl.findViewById(R.id.total);
        TextView gradpoint = (TextView) cl.findViewById(R.id.gradpoint);
        TextView greadletter = (TextView) cl.findViewById(R.id.greadletter);

        if (i%2==0){

            Subject_name.setBackgroundColor(Color.parseColor("#b3ffaf"));
            total.setBackgroundColor(Color.parseColor("#E2EFD9"));
            gradpoint.setBackgroundColor(Color.parseColor("#E2EFD9"));
            greadletter.setBackgroundColor(Color.parseColor("#E2EFD9"));


        }

            Subject_name.setTypeface(font);
        total.setTypeface(font1);
        gradpoint.setTypeface(font1);
        greadletter.setTypeface(font);

        Subject_name.setText(arrayList.get(i).getSubject_name());
        total.setText(arrayList.get(i).getHalf_yearly_grand_total());
        gradpoint.setText(arrayList.get(i).getHalf_yearly_gp());
        greadletter.setText(arrayList.get(i).getHalf_yearly_lg());




        Animation animation = AnimationUtils.loadAnimation(context,R.anim.fade_in);

        cl.startAnimation(animation);



        return cl;
    }
}
