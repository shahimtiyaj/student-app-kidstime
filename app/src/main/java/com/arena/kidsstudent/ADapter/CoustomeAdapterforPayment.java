package com.arena.kidsstudent.ADapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.arena.kidsstudent.Model.Payment;
import com.arena.kidsstudent.PaymentDetailsActivity;
import com.arena.kidsstudent.R;

import java.util.ArrayList;

/**
 * Created by Tanvi on 26-Mar-18.
 */

public class CoustomeAdapterforPayment extends BaseAdapter {


    Context context;
    ArrayList<Payment> arrayList;

    public CoustomeAdapterforPayment(Context context, ArrayList<Payment> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

       Typeface typeface =Typeface.createFromAsset(context.getAssets(), "Sofia Pro Regular.ttf");
        View cl = inflater.inflate(R.layout.payment_row12,null);
        TextView sl = (TextView) cl.findViewById(R.id.SL);
        TextView PaymentTitle = (TextView) cl.findViewById(R.id.PaymentTitle);
        TextView TransactionID = (TextView) cl.findViewById(R.id.TransactionID);
        TextView Amount = (TextView) cl.findViewById(R.id.Amount);
        TextView month = (TextView) cl.findViewById(R.id.month);
        TextView year = (TextView) cl.findViewById(R.id.year);
        TextView ExpireDate = (TextView) cl.findViewById(R.id.ExpireDate);
        TextView Status = (TextView) cl.findViewById(R.id.Status);

        Button action = cl.findViewById(R.id.Action);

        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Payment map = arrayList.get(i);
                Log.d("k", "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU"+map.getPayment_id());
                Intent idd = new Intent(context, PaymentDetailsActivity.class);
                idd.putExtra("paymentId", map.getPayment_id());
                idd.putExtra("status", map.getPay_status());
                idd.putExtra("fine", map.getFine());
                idd.putExtra("tamount", map.getAmount());
                idd.putExtra("monthn", map.getMonthdigit());
                Log.e("month",map.getAmount()+"");
                context.startActivity(idd);
            }
        });




        if (i%2==0){

            sl.setBackgroundColor(Color.parseColor("#F0F5F9"));
            PaymentTitle.setBackgroundColor(Color.parseColor("#F0F5F9"));
            TransactionID.setBackgroundColor(Color.parseColor("#F0F5F9"));
            Amount.setBackgroundColor(Color.parseColor("#F0F5F9"));
            month.setBackgroundColor(Color.parseColor("#F0F5F9"));
            year.setBackgroundColor(Color.parseColor("#F0F5F9"));
            ExpireDate.setBackgroundColor(Color.parseColor("#F0F5F9"));
            Status.setBackgroundColor(Color.parseColor("#F0F5F9"));


        }



        sl.setText(i+1+"");
        PaymentTitle.setText(arrayList.get(i).getPayment_title());
        TransactionID.setText(arrayList.get(i).getBank_transection_id());
        Amount.setText(arrayList.get(i).getAmount());
        month.setText(arrayList.get(i).getMonth());
        year.setText(arrayList.get(i).getYear());
        ExpireDate.setText(arrayList.get(i).getExpire_date());
        Status.setText(arrayList.get(i).getPay_status());

        sl.setTypeface(typeface);
        PaymentTitle.setTypeface(typeface);
        TransactionID.setTypeface(typeface);
        Amount.setTypeface(typeface);
        month.setTypeface(typeface);
        year.setTypeface(typeface);
        ExpireDate.setTypeface(typeface);
        Status.setTypeface(typeface);
        action.setTypeface(typeface);


        Animation animation = AnimationUtils.loadAnimation(context,R.anim.fade_in);

        cl.startAnimation(animation);



        return cl;
    }
}
