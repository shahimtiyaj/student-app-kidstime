package com.arena.kidsstudent.ADapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.arena.kidsstudent.Model.SearchBook;
import com.arena.kidsstudent.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ViewSearchLibraryBookListAdapter extends RecyclerView.Adapter<ViewSearchLibraryBookListAdapter.ViewStudentProfileHolder>{
    private List<SearchBook> libraryBookList = new ArrayList();
    Date date = null;
    String output = null;

    public ViewSearchLibraryBookListAdapter(List<SearchBook> list) {
        this.libraryBookList = list;
    }

    @NonNull
    @Override
    public ViewStudentProfileHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.custom_search_library_book_list,parent,false);
        return new ViewStudentProfileHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewStudentProfileHolder holder, int position) {
        SearchBook searchBook = libraryBookList.get(position);
        holder.srlno.setText(String.valueOf(holder.getAdapterPosition()+1));
        holder.bookname.setText(String.valueOf(searchBook.getBookanme()));
        holder.writer.setText(String.valueOf(searchBook.getWritername()));
        holder.status.setText(String.valueOf(searchBook.getStatus()));

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
        try {
            date = df.parse(searchBook.getDate());
            output = outputformat.format(date);
            Log.e("outputdatevalue",output);
            holder.date.setText(output);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
    @Override
    public int getItemCount() {
        return libraryBookList.size();
    }

    public static class ViewStudentProfileHolder extends RecyclerView.ViewHolder {
        TextView srlno,bookname,writer,status,date;
        LinearLayout linearLayout;

        public ViewStudentProfileHolder(@NonNull View itemView) {
            super(itemView);
            srlno = itemView.findViewById(R.id.srlno);
            bookname = itemView.findViewById(R.id.bookname);
            writer = itemView.findViewById(R.id.writer);
            status = itemView.findViewById(R.id.status);
            date = itemView.findViewById(R.id.date);
            linearLayout = itemView.findViewById(R.id.linearlayout);
        }
    }
}
