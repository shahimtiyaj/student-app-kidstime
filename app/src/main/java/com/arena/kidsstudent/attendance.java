package com.arena.kidsstudent;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.arena.kidsstudent.kotlin.view.OnlineClassUrl;
import com.arena.kidsstudent.kotlin.view.SendMessage;
import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class attendance extends Fonts implements NavigationView.OnNavigationItemSelectedListener {

    TextView TotalWorkingDay,TotalAttendance,TotalAbsent,name,status,present,namet,classn,classroll;
    RequestQueue requestQueue;
    ImageView emo;
    Parent p;
    JSONObject jsonObject;
    Toolbar toolbar;
    ProgressDialog pDialog;
    PieChart pieChart;
    View navview;
    Parent parent;
    int working_days,total_attendance,absent,today;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        parent = new Parent(attendance.this);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_home, null, false);
        TotalWorkingDay = findViewById(R.id.twdn);
        TotalAttendance = findViewById(R.id.tan);
        TotalAbsent = findViewById(R.id.totalpresent);
        name = findViewById(R.id.twd);
        status = findViewById(R.id.std);
        present = findViewById(R.id.presentt);
        emo = findViewById(R.id.emo);
        TextView h1= findViewById(R.id.h1);
        TextView h2= findViewById(R.id.h2);
        TextView h3= findViewById(R.id.h3);
        TextView h4= findViewById(R.id.h4);
        TextView h5= findViewById(R.id.h5);
        h1.setTypeface(getSemiBold());
        h2.setTypeface(getSemiBold());
        h3.setTypeface(getLight());
        h4.setTypeface(getLight());
        h5.setTypeface(getLight());
        status.setTypeface(getMedium());
        present.setTypeface(getSemiBold());
        name.setTypeface(getSemiBold());
        TotalAbsent.setTypeface(getSemiBold());
        TotalAttendance.setTypeface(getSemiBold());
        TotalWorkingDay.setTypeface(getSemiBold());
        requestQueue = Volley.newRequestQueue(attendance.this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Attendance");
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        p = new Parent(this);

        total_attendance =Integer.parseInt(p.getPreference("total_attendance"));
        absent =Integer.parseInt(p.getPreference("absent"));
        working_days = Integer.parseInt(p.getPreference("working_days"));
        today = Integer.parseInt(p.getPreference("today"));
        List<String> list = Arrays.asList(p.getPreference("name").split(" "));
        name.setText("Hello! "+list.get(list.size()-1));
        if (today==0){
            emo.setImageResource(R.drawable.absent);
            present.setText("Absent");
            present.setBackgroundResource(R.drawable.attendaceab);
            status.setText("You are Absent Today...");
        }
        Log.e("status",total_attendance+", "+absent+" ,"+working_days+","+today);
        TotalWorkingDay.setText(working_days+"");
        TotalAttendance.setText(total_attendance+"");
        TotalAbsent.setText(absent+"");
        pieChart = (PieChart) findViewById(R.id.piechart);
        pieChart.setUsePercentValues(true);
        pieChart.setEntryLabelTypeface(getRegular());
        pieChart.setCenterTextTypeface(getRegular());
        pieChart.setCenterText("Attendance   Status");
        pieChart.setCenterTextSize(15f);
        pieChart.animateXY(2000,2000);
        pieChart.getHoleRadius();
        List<PieEntry> yvalues = new ArrayList<PieEntry>();
        yvalues.add(new PieEntry(total_attendance, "Present"));
        yvalues.add(new PieEntry(absent, "Absent"));
        pieChart.getDescription().setEnabled(false);
        PieDataSet dataSet = new PieDataSet(yvalues,"");
        dataSet.setColors(ColorTemplate.createColors(new int[]{Color.parseColor("#3FBD2E"),
                Color.parseColor("#EE534F")}));
        PieData data = new PieData(dataSet);
        pieChart.setData(data);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(4).setChecked(true);
        NavigationMenuView navMenuView = (NavigationMenuView) navigationView.getChildAt(0);
       /* navMenuView.addItemDecoration(new DividerItemDecoration(attendance.this,
                DividerItemDecoration
                .VERTICAL));*/
        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size();i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    parent.applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            parent.applyFontToMenuItem(mi);
        }
        navview = navigationView.getHeaderView(0);

        name = navview.findViewById(R.id.name);
        classn = navview.findViewById(R.id.classn);
        classroll = navview.findViewById(R.id.classroll);
        name.setText(parent.getPreference("name"));
        classn.setText(parent.getPreference("class"));
        classroll.setText(parent.getPreference("class_roll"));
            String response = parent.getPreference("image");
            String gender = parent.getPreference("gender");
        if (!response.equalsIgnoreCase("http://bgb.bgbschoolraj.edu.bd/smsadmin/uploads/std_photo/")){
            Glide.with(attendance.this).load(response).bitmapTransform(new
                    RoundedCornersTransformation( attendance.this,15,
                    1)).into((ImageView)
                    navview.findViewById(R.id.imageView));

        }else {
            if (gender.equalsIgnoreCase("female")) {

                Glide.with(attendance.this).load(R.drawable.female).bitmapTransform(new
                        RoundedCornersTransformation( attendance.this,15,
                        1)).into((ImageView) navview.findViewById(R.id
                        .imageView));


            } else {

                Glide.with(attendance.this).load(R.drawable.download).bitmapTransform
                        (new
                                RoundedCornersTransformation( attendance.this,15,
                                1)).into((ImageView) navview.findViewById(R.id
                        .imageView));

            }
            Toast.makeText(attendance.this, "No Picture Found", Toast.LENGTH_LONG)
                    .show();

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.Logout:
                Intent i = new Intent(attendance.this,Login.class);
                startActivity(i);
                finishAffinity();
                return true;
            case R.id.Changepass:
                Intent j = new Intent(attendance.this,ChangePass.class);
                startActivity(j);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dash) {
            Intent i = new Intent(attendance.this,Home.class);
            startActivity(i);
            finish();
        } else if (id == R.id.profile) {
            Intent i = new Intent(attendance.this,Profile.class);
            startActivity(i);
            finish();
        } else if (id == R.id.payment) {
            Intent i = new Intent(attendance.this,Payment_list.class);
            startActivity(i);
            finish();
        }   else if (id == R.id.attendance) {
            p.getAttendance();
            Intent i = new Intent(attendance.this,attendance.class);
            startActivity(i);
            finish();

        }

        else if (id == R.id.classroutine) {
            Intent i = new Intent(attendance.this, Routine.class);
            startActivity(i);
        } else if (id == R.id.sendteachemessage) {
            Intent i = new Intent(attendance.this, SendMessage.class);
            startActivity(i);
        } else if (id == R.id.teachemessagelist) {
            Intent i = new Intent(attendance.this, com.arena.kidsstudent.kotlin.view.MessageList.class);
            startActivity(i);
        } else if (id == R.id.onlineclassurl) {
            Intent i = new Intent(attendance.this, OnlineClassUrl.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
