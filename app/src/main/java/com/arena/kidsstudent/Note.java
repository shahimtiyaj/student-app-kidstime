package com.arena.kidsstudent;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.arena.kidsstudent.ADapter.CAnote;
import com.arena.kidsstudent.Model.NoteModel;
import com.arena.kidsstudent.utils.URLs;
import com.bumptech.glide.Glide;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Note extends Fonts implements  NavigationView.OnNavigationItemSelectedListener{
    ArrayList<NoteModel> arrayList;
    CAnote adpter;
    RequestQueue requestQueue;
    Parent parent;
    JSONObject jsonObject;
    ListView listView;
    NoteModel note;
Toolbar toolbar;
    View navview;
    ImageView imageView;
    TextView name,classn,classroll;

    Context context = Note.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        requestQueue = Volley.newRequestQueue(Note.this);
        arrayList = new ArrayList<NoteModel>();
        listView = findViewById(R.id.notes);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Note");
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));

            parent = new Parent(Note.this);

            parent.getPreference("section_id");

        jsonObject = new JSONObject();

        try {
            jsonObject.put("section_id",parent.getPreference("section_id"));
            jsonObject.put("class_id",parent.getPreference("class_id"));

            Log.e("sheard",jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST, URLs.GETNOTE, jsonObject, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e("rsponce",response.toString());
                if (response.isNull(0)){
                    Toast.makeText(Note.this,"No Note Found",Toast.LENGTH_LONG).show();
                }
                for (int i = 0 ; i <response.length();i++ ){

                    try {
                        String id = response.getJSONObject(i).getString("id");
                        String title = response.getJSONObject(i).getString("title");
                        String description = response.getJSONObject(i).getString("description");
                        String file_name = response.getJSONObject(i).getString("file_name");
                        String subject = response.getJSONObject(i).getString("subject");
                        Log.e("s",subject);
                       note = new NoteModel(title,subject,description,file_name,id);
                       note.setSubject(subject);
                       note.setDescription(description);
                       note.setNote_name(title);
                       note.setLink(file_name);
                       note.setId(id);



                        arrayList.add(note);
                        adpter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }




                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonArrayRequest);
        requestQueue.getCache().clear();
        adpter = new CAnote(Note.this,arrayList);
        listView.setAdapter(adpter);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

     //   navigationView.setCheckedItem(R.id.note);

        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size()-1;i++) {
            MenuItem mi = m.getItem(i).setActionView(R.layout.menu_image);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    parent.applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            parent.applyFontToMenuItem(mi);
        }
        NavigationMenuView navMenuView = (NavigationMenuView) navigationView.getChildAt(0);
        /*navMenuView.addItemDecoration(new DividerItemDecoration(Note.this,
                DividerItemDecoration
                        .VERTICAL));*/
        navview = navigationView.getHeaderView(0);

        name = navview.findViewById(R.id.name);
        classn = navview.findViewById(R.id.classn);
        classroll = navview.findViewById(R.id.classroll);

        name.setText(parent.getPreference("name"));
        classn.setText(parent.getPreference("class"));
        classroll.setText(parent.getPreference("class_roll"));
        String response = parent.getPreference("image");
        String gender = parent.getPreference("gender");
        if (!response.equalsIgnoreCase("http://bgb.bgbschoolraj.edu.bd/smsadmin/uploads/std_photo/")){



            Glide.with(Note.this).load(response).bitmapTransform(new
                    RoundedCornersTransformation( Note.this,15,
                    1)).into((ImageView)
                    navview.findViewById(R.id.imageView));

        }else {
            if (gender.equalsIgnoreCase("female")) {

                Glide.with(Note.this).load(R.drawable.female).bitmapTransform(new
                        RoundedCornersTransformation( Note.this,15,
                        1)).into((ImageView) navview.findViewById(R.id
                        .imageView));


            } else {

                Glide.with(Note.this).load(R.drawable.download).bitmapTransform
                        (new
                                RoundedCornersTransformation( Note.this,15,
                                1)).into((ImageView) navview.findViewById(R.id
                        .imageView));

            }
            Toast.makeText(Note.this, "No Picture Found", Toast.LENGTH_LONG)
                    .show();

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.Logout:
                Intent i = new Intent(Note.this,Login.class);
                startActivity(i);
                finishAffinity();
                return true;
            case R.id.Changepass:
                Intent j = new Intent(Note.this,ChangePass.class);
                startActivity(j);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dash) {
            Intent i = new Intent(context,Home.class);
            startActivity(i);
            finish();
        } else if (id == R.id.profile) {
            Intent i = new Intent(context,Profile.class);
            startActivity(i);
            finish();
        } else if (id == R.id.payment) {
            Intent i = new Intent(context,Payment_list.class);
            startActivity(i);
            finish();
        }
            else if (id == R.id.attendance) {
            parent.getAttendance();
            Intent i = new Intent(context,attendance.class);
            startActivity(i);
            finish();

        }else if (id == R.id.classroutine) {
            Intent i = new Intent(context,Routine.class);
            startActivity(i);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
