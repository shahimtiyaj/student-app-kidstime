package com.arena.kidsstudent.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;


import com.arena.kidsstudent.LibraryBookSearch;
import com.arena.kidsstudent.repository.FilterLibraryBookListRepository;

import org.json.JSONObject;

import java.util.HashMap;

public class FilterLibraryBookListViewModel extends AndroidViewModel {

    private FilterLibraryBookListRepository filterLibraryBookListRepository;
    public FilterLibraryBookListViewModel(@NonNull Application application) {
        super(application);
    }
    public LiveData<HashMap<String, String>> getProfileStatus(){
        return filterLibraryBookListRepository.getBookListStatus();
    }

    public void initialize(LibraryBookSearch application, JSONObject reqJsonObj){
        Log.e("init--->", "initialize: "+reqJsonObj.toString() );
        filterLibraryBookListRepository = new FilterLibraryBookListRepository(application,reqJsonObj);
    }
}
