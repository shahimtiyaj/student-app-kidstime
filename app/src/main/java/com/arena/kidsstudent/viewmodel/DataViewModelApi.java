package com.arena.kidsstudent.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.arena.kidsstudent.repository.DataRepositoryApi;

import org.json.JSONObject;

import java.util.HashMap;

public class DataViewModelApi extends AndroidViewModel {

    private DataRepositoryApi dataRepositoryApi;
    public DataViewModelApi(@NonNull Application application) {
        super(application);
    }
    public LiveData<HashMap<String,String>> getStatus(){
        return dataRepositoryApi.getRegStatus();
    }

    public void initialize(Application application, JSONObject reqJsonObj){
        Log.e("init--->", "initialize: "+reqJsonObj.toString() );
        dataRepositoryApi = new DataRepositoryApi(application,reqJsonObj);

    }
}
