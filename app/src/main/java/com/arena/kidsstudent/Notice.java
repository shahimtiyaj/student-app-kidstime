package com.arena.kidsstudent;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.arena.kidsstudent.utils.URLs;
import com.bumptech.glide.Glide;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Notice extends Fonts implements NavigationView.OnNavigationItemSelectedListener{
   Toolbar toolbar;
    String noticefor,session;
    String Title,discription;
   private  JSONObject jsonObject;
   RequestQueue requestQueue;
    ArrayList<HashMap<String, String>> List ;
    ListAdapter adapter;
    ListView lv;
    ProgressDialog pDialog;
    View navview;
    ImageView imageView;
    TextView name,classn,classroll;
    Parent parent;
    Context context = Notice.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);
        initiateViews();

        parent = new Parent(Notice.this);

        checkandget();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(Notice.this,"hello",Toast.LENGTH_SHORT).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().getItem(2).setChecked(true);
        NavigationMenuView navMenuView = (NavigationMenuView) navigationView.getChildAt(0);
      /*  navMenuView.addItemDecoration(new DividerItemDecoration(Notice.this,
                DividerItemDecoration
                        .VERTICAL));*/
        navview = navigationView.getHeaderView(0);

        name = navview.findViewById(R.id.name);
        classn = navview.findViewById(R.id.classn);
        classroll = navview.findViewById(R.id.classroll);

        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size()-1;i++) {
            MenuItem mi = m.getItem(i).setActionView(R.layout.menu_image);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    parent.applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            parent.applyFontToMenuItem(mi);
        }
        name.setText(parent.getPreference("name"));
        classn.setText(parent.getPreference("class"));
        classroll.setText(parent.getPreference("class_roll"));
        String response = parent.getPreference("image");
        String gender = parent.getPreference("gender");
        if (!response.equalsIgnoreCase("http://bgb.bgbschoolraj.edu.bd/smsadmin/uploads/std_photo/")){


            Glide.with(Notice.this).load(response).bitmapTransform(new
                    RoundedCornersTransformation( Notice.this,15,
                    1)).into((ImageView)
                    navview.findViewById(R.id.imageView));

        }else {
            if (gender.equalsIgnoreCase("female")) {

                Glide.with(Notice.this).load(R.drawable.female).bitmapTransform(new
                        RoundedCornersTransformation( Notice.this,15,
                        1)).into((ImageView) navview.findViewById(R.id
                        .imageView));


            } else {

                Glide.with(Notice.this).load(R.drawable.download).bitmapTransform
                        (new
                                RoundedCornersTransformation( Notice.this,15,
                                1)).into((ImageView) navview.findViewById(R.id
                        .imageView));

            }
            Toast.makeText(Notice.this, "No Picture Found", Toast.LENGTH_LONG)
                    .show();

        }

    }

    private void checkandget() {

        if (isInternetOn()){
            if (noticefor.equals("general")){
                new AsynGeneralNotice().execute();
            }else if(noticefor.equals("personal")){
                new AsynNoticePersonal().execute();
            }
        }else{

            AlertDialog.Builder builder = new AlertDialog.Builder(Notice.this);
            builder.setTitle("NO Internet Connection")
//                builder.setMessage("")
                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            checkandget();

                        }
                    })
                    .show();
        }


    }


    private void initiateViews() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Notice");
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        Intent i = getIntent();
        List = new ArrayList<HashMap<String, String>>();
        noticefor = i.getStringExtra("noticeFor");
        lv = (ListView)findViewById(R.id.list);
        requestQueue = Volley.newRequestQueue(Notice.this);
        Log.e("noticefor",noticefor);
        jsonObject = new JSONObject();
        Parent p = new Parent(Notice.this);
        jsonObject=  p.getjsonobj();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.Logout:
                Intent i = new Intent(Notice.this,Login.class);
                startActivity(i);
                finishAffinity();
                return true;
            case R.id.Changepass:
                Intent j = new Intent(Notice.this,ChangePass.class);
                startActivity(j);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class AsynNoticePersonal extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Notice.this);
            pDialog.setMessage("Loading Please wait.");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args)
        {
            try{
                JsonArrayRequest objectRequest = new JsonArrayRequest (Request.Method
                        .POST, URLs.PERSONAL_NOTICE,jsonObject,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray jsonResponse) {
                                int len = jsonResponse.length();
                                Log.e("len",jsonResponse.toString());
                                for (int i = 0 ; i < len; i++){
                                    HashMap<String, String> map = new HashMap<String, String>();
                                    try {
                                        Title = jsonResponse.getJSONObject(i).getString
                                                ("notice_title");
                                        discription = jsonResponse.getJSONObject(i).getString
                                                ("full_notice");
                                        map.put("description",discription);
                                        map.put("notice_title",Title);
                                        Log.e("Data",discription);
                                        List.add(map);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                Log.e("list",List.toString());
                                adapter = new SimpleAdapter(Notice.this, List, R.layout.notice_row,
                                        new String[] { "notice_title","description"},
                                        new int[] {
                                                R.id.NoticeTitle,
                                                R.id.Description});
                                lv.setAdapter(adapter);
                                pDialog.dismiss();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();
                        Toast.makeText(Notice.this,"No Notice Found",
                                Toast.LENGTH_LONG).show();

                    }
                });
                requestQueue.add(objectRequest);
                requestQueue.getCache().clear();
            }
            catch(Exception e) { }
            return null;
        }
    }




    class AsynGeneralNotice extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Notice.this);

            pDialog.setMessage("Loading Please wait.");

            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args)
        {
            try{
                JsonArrayRequest objectRequest = new JsonArrayRequest (Request.Method
                        .POST, URLs.GENERAL_NOTICE,jsonObject,
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray jsonResponse) {
                                int len = jsonResponse.length();
                                Log.e("len",len+"");
                                for (int i = 0 ; i < len; i++){
                                    HashMap<String, String> map = new HashMap<String, String>();
                                        map.clear();
                                    Log.e("I",i+"");
                                    try {
                                        Title = jsonResponse.getJSONObject(i).getString
                                                ("notice_title");
                                        Log.e("I",Title);
                                        discription = jsonResponse.getJSONObject(i).getString
                                                ("full_notice");
                                    Log.e("Data",Title+discription);
                                    map.put("notice_title",Title);
                                    map.put("description",discription);
                                    List.add(map);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                adapter = new SimpleAdapter(Notice.this, List, R.layout.notice_row,
                                        new String[] { "notice_title","description"},

                                        new int[] {
                                                R.id.NoticeTitle,
                                                R.id.Description});

                                lv.setAdapter(adapter);
                                pDialog.dismiss();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pDialog.dismiss();
                        Toast.makeText(Notice.this,"No Notice Found",
                                Toast.LENGTH_LONG).show();


                        Log.e("Error", error.toString());
                    }
                });
                requestQueue.add(objectRequest);
            }
            catch(Exception e) { }
            return null;
        }
    }
    public final boolean isInternetOn() {
        ConnectivityManager connec =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED ) {

            return true;
        } else if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||  connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED  ) {

            return false;
        }
        return false;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dash) {
            Intent i = new Intent(context,Home.class);
            startActivity(i);
            finish();
        } else if (id == R.id.profile) {
            Intent i = new Intent(context,Profile.class);
            startActivity(i);
            finish();
        } else if (id == R.id.payment) {
            Intent i = new Intent(context,Payment_list.class);
            startActivity(i);
            finish();
        }   else if (id == R.id.attendance) {
            parent.getAttendance();
            Intent i = new Intent(context,attendance.class);
            startActivity(i);
            finish();

        }else if (id == R.id.classroutine) {
            Intent i = new Intent(context,Routine.class);
            startActivity(i);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
