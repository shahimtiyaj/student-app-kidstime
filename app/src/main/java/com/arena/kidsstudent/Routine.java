package com.arena.kidsstudent;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.arena.kidsstudent.ADapter.RoutinCA;
import com.arena.kidsstudent.Model.RoutineM;
import com.arena.kidsstudent.Model.TimeM;
import com.arena.kidsstudent.kotlin.view.OnlineClassUrl;
import com.arena.kidsstudent.kotlin.view.SendMessage;
import com.arena.kidsstudent.utils.URLs;
import com.bumptech.glide.Glide;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


public class Routine extends Fonts implements NavigationView.OnNavigationItemSelectedListener{
    Toolbar toolbar;
    ProgressDialog pDialog;
    JSONObject j;
    Button Selectd;
    String dayOfTheWeek;
    RoutinCA routinCA;
    Parent parent;
    static final int DIALOG_ID = 0;
    RequestQueue requestQueue;
    JSONArray days,time,routin;
    HashMap<String,String> map;
    TextView t1;
    ArrayList< HashMap<String,String>> List;
    ArrayList<RoutineM> list;
    ArrayList<TimeM> timeMArrayList;
    ArrayList<RoutineM> routineMS;
    ListView lv;
    GridView lv2;
    ListAdapter adapter;
    TimeM timeM;
    RoutineM routineM;
    int a,numberofday   ;
    int year_x;
    int month_x;
    int day_x;

    View navview;
    ImageView imageView;
    TextView name,classn,classroll;
    Context context = Routine.this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routine);
        requestQueue = Volley.newRequestQueue(Routine.this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Selectd = findViewById(R.id.Selectd);
        t1 = findViewById(R.id.t1);
        Selectd.setTypeface(getSemiBold());
        t1.setTypeface(getSemiBold());
        List = new ArrayList<HashMap<String,String>>();
        list = new ArrayList<RoutineM>();
        timeMArrayList = new ArrayList<TimeM>();
        routineMS = new ArrayList<RoutineM>();
        /*lv = (ListView)findViewById(R.id.listview);*/
        lv2 = findViewById(R.id.expand);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Routine");
        Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH);
        day_x = cal.get(Calendar.DAY_OF_MONTH);
       /* lv.setOnItemClickListener(this);*/
        toolbar.setTitleTextColor(Color.parseColor("#ffffff"));
        parent = new Parent(Routine.this);
        j = new JSONObject();
        try {
            j.put("section_id",parent.getPreference("section_id"));
            j.put("class_id",parent.getPreference("class_id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        dayOfTheWeek = sdf.format(d);

        Log.e("day",dayOfTheWeek);
        checkandget();
        Selectd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(DIALOG_ID);
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.setCheckedItem(R.id.classroutine);
        NavigationMenuView navMenuView = (NavigationMenuView) navigationView.getChildAt(0);
      /*  navMenuView.addItemDecoration(new DividerItemDecoration(Routine.this,
                DividerItemDecoration
                        .VERTICAL));*/
        navview = navigationView.getHeaderView(0);
        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size()-1;i++) {
            MenuItem mi = m.getItem(i).setActionView(R.layout.menu_image);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    parent.applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            parent.applyFontToMenuItem(mi);
        }
        name = navview.findViewById(R.id.name);
        classn = navview.findViewById(R.id.classn);
        classroll = navview.findViewById(R.id.classroll);

        name.setText(parent.getPreference("name"));
        classn.setText(parent.getPreference("class"));
        classroll.setText(parent.getPreference("class_roll"));
        String response = parent.getPreference("image");
        String gender = parent.getPreference("gender");
        if (!response.equalsIgnoreCase("http://bgb.bgbschoolraj.edu.bd/smsadmin/uploads/std_photo/")){



            Glide.with(Routine.this).load(response).bitmapTransform(new
                    RoundedCornersTransformation( Routine.this,15,
                    1)).into((ImageView)
                    navview.findViewById(R.id.imageView));

        }else {
            if (gender.equalsIgnoreCase("female")) {

                Glide.with(Routine.this).load(R.drawable.female).bitmapTransform(new
                        RoundedCornersTransformation( Routine.this,15,
                        1)).into((ImageView) navview.findViewById(R.id
                        .imageView));


            } else {

                Glide.with(Routine.this).load(R.drawable.download).bitmapTransform
                        (new
                                RoundedCornersTransformation( Routine.this,15,
                                1)).into((ImageView) navview.findViewById(R.id
                        .imageView));

            }
            Toast.makeText(Routine.this, "No Picture Found", Toast.LENGTH_LONG)
                    .show();

        }


    }

    public void setroutine(){
        if (dayOfTheWeek.equalsIgnoreCase("Saturday")){

            numberofday = 1;

            Toast.makeText(Routine.this, "No Class Today", Toast.LENGTH_SHORT).show();

        }else if (dayOfTheWeek.equalsIgnoreCase("Sunday")){
            numberofday = 2;
        }else if (dayOfTheWeek.equalsIgnoreCase("Monday")){
            numberofday = 3;
        }else if (dayOfTheWeek.equalsIgnoreCase("Tuesday")){
            numberofday = 4;
        }else if (dayOfTheWeek.equalsIgnoreCase("Wednesday")){
            numberofday = 5;
        }else if (dayOfTheWeek.equalsIgnoreCase("Thursday")){
            numberofday = 6;
        }else if (dayOfTheWeek.equalsIgnoreCase("Friday")){
            Toast.makeText(Routine.this, "No Class Today", Toast.LENGTH_SHORT).show();
            numberofday = 7;

        }

        if (routin==null){

            Toast.makeText(Routine.this, "No Class Today", Toast.LENGTH_SHORT).show();

        }
        Log.e("day",numberofday+"");

        for (int i = 0 ; i<time.length(); i++){

            for (int j = 0 ; j<routin.length(); j++){
                try {
                    String dayid = routin.getJSONObject(j).getString
                            ("class_day_id");
                    if (dayid.equalsIgnoreCase
                            (numberofday+"")){
                        Log.e("dayid",dayid);

                        String timeid = time.getJSONObject(i).getString("id");
                        String classtimeid = routin.getJSONObject(j).getString
                                ("class_time_id");
                        if (timeid.equalsIgnoreCase
                                (classtimeid)&&
                                dayid.equalsIgnoreCase
                                        (numberofday+"")){



                            Log.e("todayctid",routin.getJSONObject(j).getString
                                    ("class_time_id")+"");

                            Log.e("todayid",time.getJSONObject(i).getString
                                    ("id")+"");




                            map = new HashMap<String, String> ();

                            map.put("time",time.getJSONObject(i).getString("title"));
                            map.put("subject",routin.getJSONObject(j).getString
                                    ("subject"));

                            /*String time_title = time.getJSONObject(i).getString("title");*/
                            String time_title = time.getJSONObject(i).getString("title");
                            String hour =  time_title.substring(8,10);
                            Log.e("time"+i,time_title.substring(8,10));




                            if (routin.getJSONObject(j).getString
                                    ("teacher_name").equalsIgnoreCase("null")){
                                map.put("teacher","");

                                routineM  = new RoutineM(time.getJSONObject(i).getString
                                        ("title"),routin.getJSONObject(j).getString
                                        ("subject"),"");
                            }else{
                                routineM  = new RoutineM(time.getJSONObject(i).getString
                                        ("title"),routin.getJSONObject(j).getString
                                        ("subject"),routin.getJSONObject(j).getString
                                        ("teacher_name"));
                            }

                            list.add(routineM);

                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }

        routinCA = new RoutinCA(Routine.this,list);
               /* adapter = new SimpleAdapter(Routine.this, list, R.layout.tdaycl,
                        new String[] {"time","subject","teacher"},

                        new int[] {
                                R.id.time,
                                R.id.sub,
                                R.id.teacher});*/
        routinCA.notifyDataSetChanged();
        lv2.setAdapter(routinCA);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if(id == DIALOG_ID){
            DatePickerDialog datePickerDialog =  new DatePickerDialog(this,datePickerListener,year_x,month_x,day_x);
            //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis()-1000);
            return datePickerDialog;
        }
        else{
            return null;
        }

    }
    DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            year_x = year;
            // month_x = month + 1;
            month_x = month;
            day_x = day;

            //etDepositDate.setText(day_x+"-"+month_x+"-"+year_x);
            Selectd.setText(formatDate(year_x,month_x,day_x));
        }
    };

    private  String formatDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(year, month, day);
        Date date = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat sf = new SimpleDateFormat("EEEE");

        dayOfTheWeek = sf.format(date);
        list.clear();

        t1.setText(dayOfTheWeek+"'s Routine");

        Log.e("dac",dayOfTheWeek);

        setroutine();

        return sdf.format(date);
    }

    private void checkandget() {
        if (isInternetOn()){
            new AsynRoutine().execute();
        }else{

            AlertDialog.Builder builder = new AlertDialog.Builder(Routine.this);
            builder.setTitle("NO Internet Connection")
//                builder.setMessage("")
                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            checkandget();

                        }
                    })
                    .show();
        }


    }

    class AsynRoutine extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Routine.this);
            pDialog.setMessage("Loading Please wait.");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args)
        {JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLs.GETROUTINE,
                j, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("responce",response.toString());
                try {
                     days = response.getJSONArray("day_list");
                     time = response.getJSONArray("time_list");
                     routin = response.getJSONArray("routine_list");
                    Log.e("responce",time.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                    for (int i = 1 ; i < days.length()-1; i++){


                        map = new HashMap<String, String>();

                        try {
                            map.put("id",days.getJSONObject(i).getString("id"));

                        map.put("title",days.getJSONObject(i).getString("title"));
                        Log.e("title",days.getJSONObject(i).getString("title"));
                        List.add(map);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                setroutine();


               /* adapter = new SimpleAdapter(Routine.this, List, R.layout.daycl,
                        new String[] {"title"},

                        new int[] {
                                R.id.day});

                lv.setAdapter(adapter);*/


               /* SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
                Date d = new Date();
                dayOfTheWeek = sdf.format(d);

                Log.e("day",dayOfTheWeek);*/




                    pDialog.dismiss();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();

            }
        });
            requestQueue.add(jsonObjectRequest);
            requestQueue.getCache().clear();
            return null;
        }




    }

   /* @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub
        HashMap<String, String> map = List.get(arg2);

        String id =  map.get("id");
        String Title =  map.get("title");

        for (int i = 0 ; i < time.length();i++){
            try {
                timeM = new TimeM(time.getJSONObject(i).getString("id"),time.getJSONObject(i)
                        .getString("title"),time.getJSONObject(i).getString("serial"));
                timeMArrayList.add(timeM);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        for (int i = 0 ; i < routin.length();i++){
            try {
                if (id.equalsIgnoreCase(routin.getJSONObject(i).getString("class_day_id"))){



                routineM = new RoutineM(routin.getJSONObject(i).getString("class_day_id"),routin.getJSONObject(i)
                        .getString("class_time_id"),routin.getJSONObject(i).getString("subject"),routin
                        .getJSONObject(i).getString("teacher_name"));
                    routineMS.add(routineM);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Log.d("k", "UUU"+map.get("payment_id"));
        Intent idd = new Intent(Routine.this, RoutineDetails.class);
        idd.putExtra("paymentId", map.get("id"));
        idd.putExtra("time",timeMArrayList);
        idd.putExtra("routine",routineMS);
        idd.putExtra("title", Title);
        startActivity(idd);
        timeMArrayList.clear();
        routineMS.clear();

    }*/
    public void setTextViewText(int id, String text)
    {


        TextView tv = (TextView) findViewById(id);
        tv.setText(text);

        tv.setVisibility(View.VISIBLE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.Logout:
                Intent i = new Intent(Routine.this,Login.class);
                startActivity(i);
                finishAffinity();
                return true;
            case R.id.Changepass:
                Intent j = new Intent(Routine.this,ChangePass.class);
                startActivity(j);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public final boolean isInternetOn() {
        ConnectivityManager connec =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED ) {

            return true;
        } else if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||  connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED  ) {

            return false;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }





    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dash) {
            Intent i = new Intent(context,Home.class);
            startActivity(i);
            finish();
        } else if (id == R.id.profile) {
            Intent i = new Intent(context,Profile.class);
            startActivity(i);
            finish();
        } else if (id == R.id.payment) {
            Intent i = new Intent(context,Payment_list.class);
            startActivity(i);
            finish();
        } else if (id == R.id.attendance) {
            parent.getAttendance();
            Intent i = new Intent(context,attendance.class);
            startActivity(i);
            finish();

        }else if (id == R.id.classroutine) {
            Intent i = new Intent(context,Routine.class);
            startActivity(i);
            finish();
        }

        else if (id == R.id.sendteachemessage) {
            Intent i = new Intent(context, SendMessage.class);
            startActivity(i);
        } else if (id == R.id.teachemessagelist) {
            Intent i = new Intent(context, com.arena.kidsstudent.kotlin.view.MessageList.class);
            startActivity(i);
        } else if (id == R.id.onlineclassurl) {
            Intent i = new Intent(context, OnlineClassUrl.class);
            startActivity(i);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
