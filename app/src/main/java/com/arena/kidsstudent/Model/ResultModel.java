package com.arena.kidsstudent.Model;

public class ResultModel {
    private String half_yearly_gp;
    private String subject_name;
    private String half_yearly_lg;
    private String half_yearly_grand_total;


    public ResultModel(String half_yearly_gp, String subject_name, String half_yearly_lg, String half_yearly_grand_total) {
        this.half_yearly_gp = half_yearly_gp;
        this.subject_name = subject_name;
        this.half_yearly_lg = half_yearly_lg;
        this.half_yearly_grand_total = half_yearly_grand_total;
    }


    public String getHalf_yearly_gp() {
        return half_yearly_gp;
    }

    public void setHalf_yearly_gp(String half_yearly_gp) {
        this.half_yearly_gp = half_yearly_gp;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getHalf_yearly_lg() {
        return half_yearly_lg;
    }

    public void setHalf_yearly_lg(String half_yearly_lg) {
        this.half_yearly_lg = half_yearly_lg;
    }

    public String getHalf_yearly_grand_total() {
        return half_yearly_grand_total;
    }

    public void setHalf_yearly_grand_total(String half_yearly_grand_total) {
        this.half_yearly_grand_total = half_yearly_grand_total;
    }
}
