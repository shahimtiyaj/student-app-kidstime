package com.arena.kidsstudent.Model;

import java.io.Serializable;

public class TimeM implements Serializable{
    private String id;
    private String title;
    private String sirial;

    public TimeM(String id, String title, String sirial) {
        this.id = id;
        this.title = title;
        this.sirial = sirial;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSirial() {
        return sirial;
    }

    public void setSirial(String sirial) {
        this.sirial = sirial;
    }
}
