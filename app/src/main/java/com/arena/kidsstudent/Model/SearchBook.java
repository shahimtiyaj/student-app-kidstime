package com.arena.kidsstudent.Model;

public class SearchBook {
    String bookanme,writername,status,date;

    public SearchBook(String bookanme, String writername, String status, String date) {
        this.bookanme = bookanme;
        this.writername = writername;
        this.status = status;
        this.date = date;
    }

    public String getBookanme() {
        return bookanme;
    }

    public void setBookanme(String bookanme) {
        this.bookanme = bookanme;
    }

    public String getWritername() {
        return writername;
    }

    public void setWritername(String writername) {
        this.writername = writername;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
