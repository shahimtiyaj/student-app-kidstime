package com.arena.kidsstudent.Model;

public class NoteModel {
    private String Id,Note_name,Subject,Description,link;

    public NoteModel(String note_name, String subject, String description, String link,String Id) {
        Note_name = note_name;
        Subject = subject;
        Description = description;
        this.link = link;
        this.Id = Id;
    }

    public String getNote_name() {
        return Note_name;
    }

    public void setNote_name(String note_name) {
        Note_name = note_name;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }
}
