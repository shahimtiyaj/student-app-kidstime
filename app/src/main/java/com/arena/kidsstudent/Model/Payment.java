package com.arena.kidsstudent.Model;

public class Payment {

    String payment_id,payment_title,bank_transection_id,amount,monthn,month,year,pay_type,
            pay_status,expire_date, fine,monthdigit;


    public Payment(String payment_id, String payment_title, String bank_transection_id, String
            amount, String monthn, String month, String year, String pay_type, String pay_status,
                   String expire_date,String digit) {
        this.payment_id = payment_id;
        this.payment_title = payment_title;
        this.bank_transection_id = bank_transection_id;
        this.amount = amount;
        this.monthn = monthn;
        this.month = month;
        this.year = year;
        this.pay_type = pay_type;
        this.pay_status = pay_status;
        this.expire_date = expire_date;
        this.monthdigit = digit;
    }

    public String getFine() {
        return fine;
    }

    public void setFine(String fine) {
        this.fine = fine;
    }

    public Payment(String payment_id, String payment_title, String bank_transection_id, String
            amount, String monthn, String month, String year, String pay_type, String pay_status,
                   String expire_date, String fine,String digit) {
        this.payment_id = payment_id;
        this.payment_title = payment_title;
        this.bank_transection_id = bank_transection_id;
        this.amount = amount;
        this.monthn = monthn;
        this.month = month;
        this.year = year;
        this.pay_type = pay_type;
        this.pay_status = pay_status;
        this.expire_date = expire_date;
        this.fine = fine;
        this.monthdigit = digit;
    }

    public String getMonthdigit() {
        return monthdigit;
    }

    public void setMonthdigit(String monthdigit) {
        this.monthdigit = monthdigit;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getPayment_title() {
        return payment_title;
    }

    public void setPayment_title(String payment_title) {
        this.payment_title = payment_title;
    }

    public String getBank_transection_id() {
        return bank_transection_id;
    }

    public void setBank_transection_id(String bank_transection_id) {
        this.bank_transection_id = bank_transection_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMonthn() {
        return monthn;
    }

    public void setMonthn(String monthn) {
        this.monthn = monthn;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPay_type() {
        return pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public String getPay_status() {
        return pay_status;
    }

    public void setPay_status(String pay_status) {
        this.pay_status = pay_status;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }
}
