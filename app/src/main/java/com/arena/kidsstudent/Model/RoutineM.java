package com.arena.kidsstudent.Model;

import java.io.Serializable;

public class RoutineM implements Serializable{


    private String class_time;
    private String subject;
    private String teacher_name;

    public RoutineM(String class_time_id, String subject, String teacher_name) {

        this.class_time = class_time_id;
        this.subject = subject;
        this.teacher_name = teacher_name;
    }




    public String getClass_time_id() {
        return class_time;
    }

    public void setClass_time_id(String class_time_id) {
        this.class_time = class_time_id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTeacher_name() {
        return teacher_name;
    }

    public void setTeacher_name(String teacher_name) {
        this.teacher_name = teacher_name;
    }
}
