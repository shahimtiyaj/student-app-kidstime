package com.arena.kidsstudent.Model;

public class LibraryBooks {
    String bookname,writername,status,date;

    public LibraryBooks(String bookname, String writername, String status, String date) {
        this.bookname = bookname;
        this.writername = writername;
        this.status = status;
        this.date = date;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getWritername() {
        return writername;
    }

    public void setWritername(String writername) {
        this.writername = writername;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
