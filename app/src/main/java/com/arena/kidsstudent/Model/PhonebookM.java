package com.arena.kidsstudent.Model;

public class PhonebookM {
    private String name,Designation,number;


    public PhonebookM(String name, String designation, String number) {
        this.name = name;
        Designation = designation;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return Designation;
    }

    public void setDesignation(String designation) {
        Designation = designation;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
