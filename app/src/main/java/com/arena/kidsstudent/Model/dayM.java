package com.arena.kidsstudent.Model;

public class dayM {
    private String title;
    private String id;
    private String short_title;

    public dayM(String title, String id, String short_title) {
        this.title = title;
        this.id = id;
        this.short_title = short_title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShort_title() {
        return short_title;
    }

    public void setShort_title(String short_title) {
        this.short_title = short_title;
    }
}
