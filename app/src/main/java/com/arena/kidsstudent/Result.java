package com.arena.kidsstudent;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.arena.kidsstudent.ADapter.CoustomeAdapter;
import com.arena.kidsstudent.Model.ResultModel;
import com.arena.kidsstudent.utils.URLs;
import com.bumptech.glide.Glide;
import com.google.android.material.internal.NavigationMenuView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Result extends Fonts implements NavigationView.OnNavigationItemSelectedListener{

    Toolbar toolbar;
    Spinner spinner;
    CoustomeAdapter adapter;
    ListView listview;
    ArrayList<ResultModel> arrayList;
    ArrayAdapter<String> dataAdapter;
    RelativeLayout Rdetails;
    RequestQueue requestQueue;
    JSONObject j;
    Parent parent;
    List<String> id,Examlist;
    View navview;
    ImageView imageView;
    TextView name,classn,classroll;
    Context context = Result.this;
    int textViews[] = {R.id.Subject_name,R.id.total,R.id.gradpoint,R.id.greadletter};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        listview = (ListView) findViewById(R.id.listview);
        spinner = findViewById(R.id.spinner);
      id = new ArrayList<String>();
        requestQueue = Volley.newRequestQueue(Result.this);
         Examlist = new ArrayList<String>();
        arrayList = new ArrayList<ResultModel>();
        parent = new Parent(Result.this);
        adapter = new CoustomeAdapter(Result.this,arrayList);
        listview.setAdapter(adapter);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        settypeface(textViews);
        id.add("0");
        Examlist.add("Select Exam");
        spinner = findViewById(R.id.spinner);
             checkandget();










    dataAdapter = new ArrayAdapter<String>(this, R.layout
                .simple_spinner_item, Examlist);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                arrayList.clear();
                String item = Examlist.get(i).toString();
                String a = id.get(i);
                if (isInternetOn()) {

                    if (Integer.parseInt(a) != 0) {
                        j = new JSONObject();
                        try {
                            j.put("user_id", parent.getPreference("user_id"));
                            j.put("exam_id", a);
                            j.put("section_id", parent.getPreference("section_id"));
                            j.put("class_id", parent.getPreference("class_id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        JsonArrayRequest jsonArrayReques = new JsonArrayRequest(Request.Method.POST, URLs.GETEXAMRESULT, j, new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                Log.e("ttttt", response.toString());

                                if (response.length() > 0) {


                                    for (int i = 0; i < response.length(); i++) {

                                        try {

                                            String half_yearly_gp = response.getJSONObject(i).getString("half_yearly_gp");
                                            String subject_name = response.getJSONObject(i).getString("subject_name");
                                            String half_yearly_lg = response.getJSONObject(i).getString("half_yearly_lg");
                                            String half_yearly_grand_total = response.getJSONObject(i).getString("half_yearly_grand_total");

                                            final ResultModel re = new ResultModel(half_yearly_gp, subject_name, half_yearly_lg, half_yearly_grand_total);

                                            arrayList.add(re);
                                            adapter.notifyDataSetChanged();


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }


                                } else Toast.makeText(Result.this, " Result not  published yet!", Toast.LENGTH_LONG).show();

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                Log.e("eror", error.toString());
                                Toast.makeText(Result.this, " Result not yet published!", Toast.LENGTH_LONG).show();

                            }
                        });

                        requestQueue.add(jsonArrayReques);
                        adapter = new CoustomeAdapter(Result.this, arrayList);

                        listview.setAdapter(adapter);


                        /* Toast.makeText(Result.this, a+ " Selected: " + item, Toast.LENGTH_LONG).show();*/

                    }
                }else{
                    final AlertDialog.Builder builder = new AlertDialog.Builder(Result.this);
                    builder.setTitle("NO Internet Connection");
                    final AlertDialog dialog = builder.create();
//                builder.setMessage("")
                       builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                           @Override
                           public void onClick(DialogInterface dialogInterface, int i) {

                               dialog.dismiss();



                           }
                       })
                            .show();
                }







            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //navigationView.setCheckedItem(R.id.result);
        NavigationMenuView navMenuView = (NavigationMenuView) navigationView.getChildAt(0);
       /* navMenuView.addItemDecoration(new DividerItemDecoration(Result.this,
                DividerItemDecoration
                        .VERTICAL));*/
        navview = navigationView.getHeaderView(0);
        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size()-1;i++) {
            MenuItem mi = m.getItem(i).setActionView(R.layout.menu_image);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    parent.applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            parent.applyFontToMenuItem(mi);
        }
        name = navview.findViewById(R.id.name);
        classn = navview.findViewById(R.id.classn);
        classroll = navview.findViewById(R.id.classroll);

        name.setText(parent.getPreference("name"));
        classn.setText(parent.getPreference("class"));
        classroll.setText(parent.getPreference("class_roll"));
        String response = parent.getPreference("image");
        String gender = parent.getPreference("gender");
        if (!response.equalsIgnoreCase("http://bgb.bgbschoolraj.edu.bd/smsadmin/uploads/std_photo/")){



            Glide.with(Result.this).load(response).bitmapTransform(new
                    RoundedCornersTransformation( Result.this,15,
                    1)).into((ImageView)
                    navview.findViewById(R.id.imageView));

        }else {
            if (gender.equalsIgnoreCase("female")) {

                Glide.with(Result.this).load(R.drawable.female).bitmapTransform(new
                        RoundedCornersTransformation( Result.this,15,
                        1)).into((ImageView) navview.findViewById(R.id
                        .imageView));


            } else {

                Glide.with(Result.this).load(R.drawable.download).bitmapTransform
                        (new
                                RoundedCornersTransformation( Result.this,15,
                                1)).into((ImageView) navview.findViewById(R.id
                        .imageView));

            }
            Toast.makeText(Result.this, "No Picture Found", Toast.LENGTH_LONG)
                    .show();

        }



    }

    private void checkandget() {
        if (isInternetOn()){

            getsdata();
        }else{

            AlertDialog.Builder builder = new AlertDialog.Builder(Result.this);
            builder.setTitle("NO Internet Connection")
//                builder.setMessage("")
                    .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {

                            checkandget();

                        }
                    })
                    .show();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.optionmenu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.Logout:
                Intent i = new Intent(Result.this,Login.class);
                startActivity(i);
                finishAffinity();
                return true;
            case R.id.Changepass:
                Intent j = new Intent(Result.this,ChangePass.class);
                startActivity(j);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public final boolean isInternetOn() {
        ConnectivityManager connec =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED ) {

            return true;
        } else if ( connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||  connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED  ) {

            return false;
        }
        return false;
    }

    public void getsdata() {

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URLs
                .GETEXAMLIST, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i =  0 ; i<response.length();i++){
                    try {


                        id.add(response.getJSONObject(i).getString("id"));
                        Examlist.add(response.getJSONObject(i).getString("title"));
                        dataAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue.add(jsonArrayRequest);
        requestQueue.getCache().clear();


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.dash) {
            Intent i = new Intent(context,Home.class);
            startActivity(i);
            finish();
        } else if (id == R.id.profile) {
            Intent i = new Intent(context,Profile.class);
            startActivity(i);
            finish();
        } else if (id == R.id.payment) {
            Intent i = new Intent(context,Payment_list.class);
            startActivity(i);
            finish();
        }   else if (id == R.id.attendance) {
            parent.getAttendance();
            Intent i = new Intent(context,attendance.class);
            startActivity(i);
            finish();

        }else if (id == R.id.classroutine) {
            Intent i = new Intent(context,Routine.class);
            startActivity(i);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void settypeface(int id[]){
        for (int i=0 ; i<id.length;i++){
            TextView t = findViewById(id[i]);
            t.setTypeface(getSemiBold());
        }

    }
}
